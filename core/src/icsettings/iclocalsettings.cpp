#include <iclocalsettings.h>
#include <icobjectmacro.h>
#include "QCoreApplication"
#include <QFile>

void ICLocalSettings::parseSettingsFile(const QString& fname)
{
    // TODO: сделать детектирование колец (когда образуется цикл из включаемых файлов).
    QSettings* file = new QSettings(fname, QSettings::IniFormat);
    m_files.append(file);
    file->beginGroup("Include");
    QStringList includes = file->childKeys();
    for(const QString& path : includes)
        parseSettingsFile(file->value(path).toString());
    file->endGroup();
}

QString ICLocalSettings::makeFullGroup() const
{
    QString fullGroup;
    for(const QString& group : m_currentGroup)
    {
        fullGroup.append(group);
        fullGroup.append("/");
    }
    return fullGroup;
}

ICLocalSettings::ICLocalSettings()
{
    QSettings* file = new QSettings(QSettings::IniFormat, QSettings::UserScope, QCoreApplication::organizationName(), QCoreApplication::applicationName());
    parseSettingsFile(file->fileName());
    delete file;
}


ICLocalSettings::ICLocalSettings(const QString& organization, const QString& application)
{
    QSettings* file = new QSettings(QSettings::IniFormat, QSettings::UserScope, organization, application);
    parseSettingsFile(file->fileName());
    delete file;
}

ICLocalSettings::~ICLocalSettings()
{
    qDeleteAll(m_files);
}

QVariant ICLocalSettings::value(const QString& key, const QVariant& defaultValue) const
{
    QString fullKey = makeFullGroup();
    fullKey.append(key);
    struct KeyVals
    {
        QVariant val;
        bool writable;
    };
    QList<KeyVals> vals;
    bool readOnlyPresent = false;
    bool writablePresent = false;
    // прочитаем все параметры с таким ключем, которые есть в файлах
    for(const QSettings* file : m_files)
    {
        if(file->contains(fullKey))
        {
            bool isWritable = file->isWritable();
            vals.append({file->value(fullKey), isWritable});
            if(isWritable)
                writablePresent = true;
            else
                readOnlyPresent = true;
        }
    }
    if(vals.isEmpty())
        return defaultValue;
    if(readOnlyPresent && writablePresent) // вернем первое попавшееся read-only значение
        for(const KeyVals& kv : vals)
            if(!kv.writable)
                return kv.val;
    return vals[0].val; // просто первое попавшееся значение
}

void ICLocalSettings::setValue(const QString& key, const QVariant& value)
{
    QString fullKey = makeFullGroup().append(key);
    // составим список файлов, где есть этот ключ
    QList<QSettings*> files;
    for(QSettings* file : m_files)
        if(file->contains(fullKey))
        {
            if(file->isWritable())
                files.append(file);
            else
                return;
        }
    // мы нашли все вхождения таких ключей в файлах, доступные для записи
    QSettings* file = nullptr;
    if(files.isEmpty()) // если таких нет, то ищем первый попавшийся файл, куда можно писать
    {
        for(QSettings* f : m_files)
            if(f->isWritable())
            {
                file = f;
                break;
            }
    }
    else
        file = files[0];
    if(file == nullptr) // а писать-то и некуда
        return;
    file->setValue(fullKey, value);
    emit valueChanged(key, value);
}

void ICLocalSettings::beginGroup(const QString prefix)
{
    m_currentGroup.append(prefix);
}

void ICLocalSettings::endGroup()
{
    m_currentGroup.removeLast();
}

QStringList ICLocalSettings::childGroups() const
{
    QString group = makeFullGroup();
    if(!group.isEmpty())
        group.remove(group.length()-1, 1); // уберем последний слэш
    QStringList groups;
    for(QSettings* file : m_files)
    {
        if(!group.isEmpty())
            file->beginGroup(group);
        groups.append(file->childGroups());
        if(!group.isEmpty())
            file->endGroup();
    }
    return groups;
}

QStringList ICLocalSettings::childKeys() const
{
    QString group = makeFullGroup();
    if(!group.isEmpty())
        group.remove(group.length()-1, 1); // уберем последний слэш
    QStringList keys;
    for(QSettings* file : m_files)
    {
        if(!group.isEmpty())
            file->beginGroup(group);
        keys.append(file->childKeys());
        if(!group.isEmpty())
            file->endGroup();
    }
    return keys;
}

QStringList ICLocalSettings::allKeys() const
{
    QStringList keys;
    for(QSettings* file : m_files)
        keys.append(file->allKeys());
    return keys;
}

void ICLocalSettings::remove(const QString& key)
{
    QString fullKey = makeFullGroup().append(key);
    for(QSettings* file : m_files)
        if(file->isWritable())
            file->remove(fullKey);
}

void ICLocalSettings::clear()
{
    for(QSettings* file : m_files)
        file->clear();
}

QString ICLocalSettings::fileName() const
{
    return m_files[0]->fileName();
}

void ICLocalSettings::sync()
{
    for(QSettings* file : m_files)
        file->sync();
}

//void ICLocalSettings::beginReadArray(const QString &prefix)
//{
//    // сначала надо найти массив, который нужно прочитать в каком-нибудь файле
//    m_file->beginReadArray(prefix);
//}

//void ICLocalSettings::beginWriteArray(const QString &prefix)
//{
//    m_file->beginWriteArray(prefix);
//}

//void ICLocalSettings::endArray()
//{
//    m_file->endArray();
//}

//void ICLocalSettings::setArrayIndex(int index)
//{
//    m_file->setArrayIndex(index);
//}

IC_REGISTER_METATYPE(ICLocalSettings)
