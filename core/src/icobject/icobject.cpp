#include <icobject.h>
#include <iclocalsettings.h>
#include <QCoreApplication>
#include <QMetaClassInfo>
#include <QDebug>

#define HAVE_QT5 1

#define VERIFY_PREFIX  Q_ASSERT_X(!m_settingsHub.isNull(),"ICObject::setting()","Did you forget to set prefix? Call init(prefix) to solve this.")

ICObject::ICObject(QObject *parent)
    : ICErrorNotifier(parent)
{
}

ICObject::~ICObject()
{
    if(!m_settingsHub.isNull())
        m_settingsHub->removeWatcher(this);
}

QVariant ICObject::setting(const QString &name, const QVariant& defaultValue, bool locally) const
{
    VERIFY_PREFIX;
    return m_settingsHub->value(name,defaultValue,locally);
}

void ICObject::saveSetting(const QString &name, const QVariant &value, bool locally)
{
    VERIFY_PREFIX;
    m_settingsHub->setValue(name,value,locally);
}

ICRETCODE ICObject::init(const QString &pref)
{
    m_settingsPrefix = pref;
    //если ранее была назначена группа настроек, отписаться от ее мониторинга
    if(!m_settingsHub.isNull())
    {
        m_settingsHub->removeWatcher(this);
    }
    //инициализировать интерфейс доступа к настройкам
    m_settingsHub = ICObjectSettings::settingsGroup(pref);
    //для всех настроек, выполнить инициализацию и подписаться на наблюдение
    registerSettings(localSettings(),true);
    registerSettings(globalSettings(),false);
    return ICOK;
}

void ICObject::registerSettings(const QStringList &names, bool local)
{
//    const QMetaObject* mo = metaObject();
    for(auto name:names)
    {
        auto nm = name.toUtf8().constData();
//        if(mo->indexOfProperty(nm)==-1)
//            continue;
        //подписаться на изменения
        m_settingsHub->appendWatcher(name.toUtf8().constData(),this);
        //вычислить начальное значение настройки
        auto val = property(nm);
        if(local)
            val = m_settingsHub->value(name,val,local);
        else
        {
            auto curVal = m_settingsHub->queryValue(name);
            if(curVal.isValid())
               val = curVal;
        }
        //инициализировать настройку
        setProperty(name.toUtf8().constData(),val);
    }
}

#define INVALIDTYPE  0
#define USERTYPE     1
#define ORDINARYTYPE 2
#define LISTTYPE     3

void ICObject::recursivePack(const QVariant& v, QDataStream& str) const
{
    if(v.type()==QVariant::Invalid)
    {
        qDebug()<<(__FILE__+__LINE__+trUtf8(": Ошибка сериализации данных! Тип %1 не зарегистрирован!").arg(v.typeName()));
        return;
    }
    else if(v.type()==QVariant::List)
    {
        str<<LISTTYPE;
        QVariantList vl = v.value<QVariantList>();
        str<<vl.size();
        for(const QVariant& li : vl)
            recursivePack(li, str);
    }
    else if(v.type()==QVariant::UserType)
    {
        str<<USERTYPE;
        // считаем, что это тип, производный от ICObject
        ICObject* obj = v.value<ICObject*>();
        if(obj == nullptr)
        {
            qDebug()<<__FILE__+__LINE__+trUtf8(": Невозможно сериализовать объект типа %1").arg(v.typeName());
            return;
        }
        str<<QVariant(obj->pack());
    }
    else
    {
        str<<ORDINARYTYPE;
        str<<v;
    }
}

QByteArray ICObject::pack() const
{
    QByteArray arr;
    QDataStream str(&arr, QIODevice::WriteOnly);
    str<<QString(metaObject()->className());
    for(int i=0; i<metaObject()->propertyCount(); i++)
    {
        QString nm = metaObject()->property(i).name();
        if(nm == QString("objectName"))
            continue;
        if(isPropSerializable(nm))
        {
            str<<nm;
            auto v = metaObject()->property(i).read(this);
            recursivePack(v, str);
        }
    }
    QList<QByteArray> dnames = dynamicPropertyNames();
    for(const QByteArray& dn: dnames)
    {
        str<<QString(dn);
        recursivePack(property(dn.constData()), str);
    }
    return arr;
}

ICObject* ICObject::fromPacked(QByteArray& data)
{
    QDataStream str(&data, QIODevice::ReadOnly);
    QString className;
    str>>className;
    int id = QMetaType::type(className.toUtf8().constData());
#ifdef HAVE_QT5
    if(id==QMetaType::UnknownType)
#else
    if(id == 0)
#endif
    {
        qDebug()<<__FILE__+__LINE__+trUtf8(": Класс %1 не зарегистрирован!").arg(className);
        return nullptr;
    }
#ifdef HAVE_QT5
    ICObject* inst = (ICObject*)QMetaType::create(id);
#else
    ICObject* inst = (ICObject*)QMetaType::construct(id);
#endif
    if(inst->unpack(data))
        return inst;
    delete inst;
    return nullptr;
}

bool ICObject::unpack(QByteArray& data)
{
    QDataStream str(&data, QIODevice::ReadOnly);
    return unpack(str);
}

QVariant ICObject::recursiveUnpack(QDataStream& str)
{
    int ptype=INVALIDTYPE;
    str>>ptype;
    switch(ptype)
    {
    case USERTYPE:
    {
        QVariant data;
        str>>data;
        QByteArray arr = data.value<QByteArray>();
        ICObject* val = ICObject::fromPacked(arr);
        return QVariant::fromValue<ICObject*>(val);
    }
    case ORDINARYTYPE:
    {
        QVariant data;
        str>>data;
        return data;
    }
    case LISTTYPE:
    {
        int listSize=0;
        str>>listSize;
        QVariantList l;
        for(int i=0; i<listSize; i++)
        {
            QVariant v = recursiveUnpack(str);
            l.append(v);
        }
        return l;
    }
    default:
        reportError(__FILE__+__LINE__+trUtf8(": Внезапная ошибка распаковки объекта! Неопределен тип свойства."));
    }
    return QVariant();
}

bool ICObject::unpack(QDataStream& str)
{
    QString className;
    str>>className;
    if(className!=metaObject()->className())
    {
        reportError(__FILE__+__LINE__+trUtf8(": Имя упакованного класса не совпадает с именем класса-получателя данных!"));
        return false;
    }
    QStringList fnames;
    while(!str.atEnd())
    {
        QString pname;
        str>>pname;
        fnames<<pname;
        QVariant v = recursiveUnpack(str);
        setProperty(pname.toUtf8().constData(), v);
    }
    emit unpacked(fnames);
    return true;
}

bool ICObject::isPropSerializable(const QString &pname) const
{
    int cnt = metaObject()->propertyCount();
    bool ser = true;
    for(int i=0;i<cnt;i++)
    {
        auto p = metaObject()->property(i);
        if(QString(p.name())==pname)
        {
            ser = p.isStored(this);
            break;
        }
    }
    return ser;
}
