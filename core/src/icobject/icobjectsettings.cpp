#include <icobjectsettings.h>
#include <iclocalsettings.h>
#include <QMutexLocker>
#include <icobject.h>
#include <QPluginLoader>
#include <QDir>
#include <icplugininterface.h>

QHash<QString,QSharedPointer<ICObjectSettings> > ICObjectSettings::g_settingGroups;
QMutex ICObjectSettings::g_mutex;
QMutex ICObjectSettings::g_clientLoadMutex;
QThread ICObjectSettings::g_clientThread;

ICObjectSettings::ICObjectSettings(const QString &prefix,QObject* parent)
    :ICErrorNotifier(parent),
      m_prefix(prefix)
{
}

QVariant ICObjectSettings::value(const QString &name, const QVariant &defValue,bool locally)
{
    return settings(locally)->value(name,defValue);
}

QVariant ICObjectSettings::queryValue(const QString &name)
{
    return settings(false)->queryValue(name);
}

void ICObjectSettings::setValue(const QString &name, const QVariant &value,bool locally)
{
    QMutexLocker lock(&m_mutex);
    Q_UNUSED(lock);
    settings(locally)->setValue(name,value);
}

QSharedPointer<ICObjectSettings> ICObjectSettings::settingsGroup(const QString &prefix)
{
    QMutexLocker lock(&g_mutex);
    Q_UNUSED(lock);
    if(!g_settingGroups.contains(prefix))
        g_settingGroups[prefix] = QSharedPointer<ICObjectSettings>(new ICObjectSettings(prefix));
    return g_settingGroups.value(prefix);
}

void ICObjectSettings::onValueChanged(QString setting, QVariant value)
{
    QMutexLocker lock(&m_watchersMutex);
    Q_UNUSED(lock);
    if(!m_watchers.contains(setting))
        return;
    auto ws = m_watchers.value(setting);
    for(ICObject* w: ws)
    {
        auto p = setting.toUtf8().constData();
        if(w->property(p)==value)
            continue;
        w->setProperty(p,value);
    }
}

void ICObjectSettings::appendWatcher(const QString &propName, ICObject *watcher)
{
    QMutexLocker lock(&m_watchersMutex);
    Q_UNUSED(lock);
    if(m_watchers.contains(propName))
        m_watchers[propName] << watcher;
    else
        m_watchers[propName] = {watcher};
}

void ICObjectSettings::removeWatcher(ICObject *watcher)
{
    QMutexLocker lock(&m_watchersMutex);
    Q_UNUSED(lock);
    for(QList<ICObject*> ws:m_watchers.values())
    {
        if(ws.contains(watcher))
            ws.removeAll(watcher);
    }
}

ICSettings* ICObjectSettings::settings(bool local)
{
    ICSettings *sett = nullptr;
    if(local)
    {
        if(m_localSettings!=nullptr)
            return m_localSettings;
        m_localSettings = sett = new ICLocalSettings();
    }
    else
    {
        int typeId = QMetaType::type("ICGlobalSettings");
        if(typeId == QMetaType::UnknownType)
        {
            //если класс не зарегистрирован
            reportError("Класс доступа к глобальным настройкам не зарегистрирован");
            return settings(true);
        }
        m_globalSettings = sett = reinterpret_cast<ICSettings*>(QMetaType::create(typeId));
    }
    sett->beginGroup(m_prefix);
    connect(sett,&ICSettings::valueChanged,this,&ICObjectSettings::onValueChanged);
    return sett;
}
