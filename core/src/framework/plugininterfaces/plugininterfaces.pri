SOURCES += \
    plugininterfaces/icplatformlayer.cpp \
    plugininterfaces/icplugininterface.cpp

HEADERS +=\
    $$CORE_INCLUDE/icplatformlayer.h \
    $$CORE_INCLUDE/icplugincontext.h \
    $$CORE_INCLUDE/icplugininterface.h \
    $$CORE_INCLUDE/icversion.h \
    $$CORE_INCLUDE/icretcode.h
