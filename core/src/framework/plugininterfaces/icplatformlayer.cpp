#include <icplatformlayer.h>
#include "icrouter.h"
#include <QDateTime>

using icrouting::ICRouter;

ICPlatformLayer::ICPlatformLayer(QObject* parent):ICObject(parent)
{
}

ICPlatformLayer::~ICPlatformLayer()
{

}

QList<quint64> ICPlatformLayer::pluginIDs(const QString &pluginName)const
{
    return m_context->router()->pluginIDs(pluginName, m_context->pluginAddress());
}

QList<quint64> ICPlatformLayer::pluginIDs()const
{
    return m_context->router()->pluginIDs(m_context->pluginAddress());
}

quint64 ICPlatformLayer::thisPluginID()const
{
    return m_context->pluginAddress();
}

QString ICPlatformLayer::nodeName(quint64 pluginID)const
{
    return m_context->router()->nodeName(pluginID);
}


QString ICPlatformLayer::pluginName(quint64 pluginID)const
{
    return m_context->router()->pluginName(pluginID);
}


void ICPlatformLayer::sendMessage(quint64 destPluginID, const QByteArray& data)const
{
    m_context->router()->route(m_context->pluginAddress(),destPluginID, data);
}

//inline uint64_t rdtsc()
//{
//    uint64_t x;
//    __asm__ volatile ("rdtsc\n\tshl $32, %%rdx\n\tor %%rdx, %%rax" : "=a" (x) : : "rdx");
//    return x;
//}

quint64 ICPlatformLayer::generateUID()
{
    return ((quint64)qrand()<<32) | qrand();
}
