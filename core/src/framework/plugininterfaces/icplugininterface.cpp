#include <icplugininterface.h>
#include <QDebug>

ICPluginInterface::ICPluginInterface(QObject* parent):ICPlatformLayer(parent)
{
    connect(this, &ICPluginInterface::message, this, &ICPluginInterface::processMessage, Qt::QueuedConnection);
    connect(this, &ICPluginInterface::pluginsChanged, this, &ICPluginInterface::onPluginsChanged, Qt::QueuedConnection);
    connect(this, &ICPluginInterface::needstart, this, &ICPluginInterface::start, Qt::QueuedConnection);
}

void ICPluginInterface::pushMessage(quint64 srcAddr, const QByteArray& data)
{
    emit message(srcAddr, data);
}

void ICPluginInterface::pushPluginsChanged()
{
    emit pluginsChanged();
}

void ICPluginInterface::pushStart()
{
    emit needstart();
}

ICRETCODE ICPluginInterface::init(const QString& pref)
{
    return ICPlatformLayer::init(pref);
}
