#include "icserverlinkprivate.h"
#include "icprimarylinkprivate.h"
#include "icsimplelinkprivate.h"
#include "utilities.h"
#include "icconnection.h"
#include "icgridmanager.h"

namespace icrouting {


ICServerLinkPrivate::ICServerLinkPrivate(ICBaseLink *s): ICDiscoveryLinkPrivate(s)
{

}

ICBaseLinkState* ICServerLinkPrivate::firstState()
{
    return new ICWaitingState(this);
}

void ICServerLinkPrivate::resolvedAddressNotify(const ICNetworkDetails &serverAddr)
{
    emit addressResolved(serverAddr, toICNetworkAddress(connection()->peerPortAddress()));
}

void ICWaitingState::begin()
{
    qDebug()<<"begin Waiting state";
}

void ICWaitingState::processMessage(const ICMessage &message)
{
    Q_ASSERT(message.m_type == ICMessage::PrimaryLinkValidation || message.m_type == ICMessage::SimpleLinkValidation);
    ICLinkVerificationInfo info(message.data());
    ICLinkStatus linkStatus;
    ICBaseLinkState *nextState = nullptr;
    if(ICGridManager::instance()->nodeNameValid(info.name))
    {

        ICNetworkDetails addr;
        addr.ip = toICNetworkAddress(mySession()->connection()->peerPortAddress()).ip;
        addr.port = info.port;
        mySession()->setServerDetails(addr);

        if(message.m_type == ICMessage::PrimaryLinkValidation)
        {
            nextState = new ICDiscoveryStateServer(mySession());
            mySession()->setPriority(ICPrimaryLinkPrivate::typePriority());
        }
        else
        {
            nextState = new ICInfoExchangeStateServer(mySession());
            mySession()->setPriority(ICSimpleLinkPrivate::typePriority());
        }

        if(mySession()->isValid())
        {
            linkStatus.valid = true;
            mySession()->resolvedAddressNotify(addr);
        }
    }
    ICMessage answer;
    answer.m_type = message.m_type;
    answer.setData(linkStatus.toByteArray());
    mySession()->connection()->send(answer.toByteArray());
    if(linkStatus.valid)
        mySession()->changeState(nextState);
    else
    {
        delete nextState;
        mySession()->setServerDetails(ICNetworkDetails());
        mySession()->close();
    }
}


}
