HEADERS += \
    $$CORE_INCLUDE/icrouter.h \
    $$CORE_INCLUDE/icmessages.h \
    $$CORE_INCLUDE/icgridmanager.h \
    $$CORE_INCLUDE/icnodeswitch.h \
    $$CORE_INCLUDE/iclocalnode.h \
    $$CORE_INCLUDE/icremotenode.h \
    $$CORE_INCLUDE/icnetworkdetails.h \
    $$CORE_INCLUDE/icabstractnode.h \
    $$CORE_INCLUDE/iclocker.h \
    ../../include/utilities.h \
    ../../include/icsessionstate.h \
    ../../include/icsessionmap.h \
    ../../include/icbaselinkprivate.h \
    ../../include/icdiscoverylink.h \
    ../../include/icdiscoverylinkprivate.h \
    ../../include/icprimarylink.h \
    ../../include/icprimarylinkprivate.h \
    ../../include/icserverlink.h \
    ../../include/icserverlinkprivate.h \
    ../../include/icsimplelink.h \
    ../../include/icsimplelinkprivate.h \
    ../../include/icbaselink.h



SOURCES += \
    routing/icrouter.cpp \
    routing/icnodeswitch.cpp \
    routing/iclocalnode.cpp \
    routing/icremotenode.cpp \
    routing/icmessages.cpp \
    routing/icnetworkdetails.cpp \
    routing/iclocker.cpp \
    routing/icgridmanager.cpp \
    routing/utilities.cpp \
    routing/icsessionstate.cpp \
    routing/icsessionmap.cpp \
    routing/icbaselink.cpp \
    routing/icbaselinkprivate.cpp \
    routing/icdiscoverylink.cpp \
    routing/icprimarylink.cpp \
    routing/icprimarylinkprivate.cpp \
    routing/icserverlink.cpp \
    routing/icserverlinkprivate.cpp \
    routing/icsimplelink.cpp \
    routing/icsimplelinkprivate.cpp

