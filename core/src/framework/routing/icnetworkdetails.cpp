#include "icnetworkdetails.h"
#include "qhash.h"
#include "QDataStream"

namespace icrouting {

bool operator ==(const ICNetworkDetails& first,const ICNetworkDetails& second)
{
    return (first.ip== second.ip) && (first.port == second.port);
}

QDataStream& operator<<(QDataStream& stream, const ICNetworkDetails& details)
{
    stream<<details.ip;
    stream<<details.port;
    return stream;
}

QDataStream& operator>>(QDataStream& stream, ICNetworkDetails& details)
{
    stream>>details.ip;
    stream>>details.port;
    return stream;
}


}

uint qHash(const icrouting::ICNetworkDetails& d)
{
    return qHash(uint(d.ip))^qHash(uint(d.port));
}

namespace boost {

std::size_t hash_value(const icrouting::ICNetworkDetails &addr)
{
    return qHash(uint(addr.ip))^qHash(uint(addr.port));
}

}


