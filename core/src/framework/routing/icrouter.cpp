#include "icrouter.h"
#include "QSharedPointer"
#include "QCoreApplication"
#include "iclocalsettings.h"
#include "QHostAddress"
#include "QDateTime"
#include <ichash.h>

namespace icrouting {

ICRouter::ICRouter(QObject *parent) : ICObject(parent)
{
    m_switch.reset(new ICNodeSwitch);
    m_localNode.reset(new ICLocalNode);
    ICGridManager::instance()->setLocalNode(m_localNode.data());
}

ICRETCODE ICRouter::init(const QString& pref)
{
    auto ret = ICObject::init(pref);
    m_switch->init(pref);
    m_localNode->init(pref);
    configureLocalNode(m_localNode.data());
    ICGridManager::instance()->init(pref);
    m_switch->addNode(m_localNode.data());
    connect(this, &ICRouter::routeRequested, this, &ICRouter::onRouteRequested, Qt::QueuedConnection);
    connect(ICGridManager::instance(), &ICGridManager::newNode, this, &ICRouter::onNewRemoteNode);
    connect(ICGridManager::instance(), &ICGridManager::nodeLost, this, &ICRouter::onRemoteNodeLost);
    connect(ICGridManager::instance(), &ICGridManager::remotePluginsChanged, this, &ICRouter::pluginsChanged);
    connect(ICGridManager::instance(), &ICGridManager::remoteMessage, this, &ICRouter::onRouteRequested);
    return ret;
}

ICRouter::~ICRouter()
{

}

void ICRouter::connectToGrid()
{
    ICGridManager::instance()->start();
}

void ICRouter::addLocalPLugins(const QList<ICPluginInterface *> &pluginList)
{
    for(ICPluginInterface* plugin : pluginList)
    {
//        plugin->m_context.reset(new ICPluginContext); //это делается раньше
        quint64 plAddress = m_localNode->id();
        plAddress <<= 32;
//        plAddress |= icHash(plugin->pluginInfo().pluginName);
        plAddress |= icHash(plugin->m_context->pluginName()); // потому что даже на одном узле могут быть плагины с одинаковыми именами, переназначенными в конфиге.
        plugin->m_context->setPluginAddress(plAddress);
        plugin->m_context->setRouter(this);
        connect(this , &ICRouter::pluginsChanged, plugin, &ICPluginInterface::pluginsChanged);
    }
    m_localNode->addPlugins(pluginList);
}

void ICRouter::removelocalPlugins(const QList<ICPluginInterface*>& pluginList)
{
    m_localNode->removePlugins(pluginList);

}

bool ICRouter::route(quint64 srcPluginID, quint64 destPluginID, const QByteArray& message)const
{
    if(m_switch->isRoutable(destPluginID))
    {
        emit routeRequested(srcPluginID, destPluginID, message);
        return true;
    }
    return false;
}

void ICRouter::onRouteRequested(quint64 srcPluginID, quint64 destPluginID, const QByteArray &message)const
{
    m_switch->route(srcPluginID, destPluginID, message);
}

QList<quint64> ICRouter::pluginIDs(QString pluginName,quint64 senderID)const
{
    return m_switch->pluginIDs(pluginName, senderID);
}

QList<quint64> ICRouter::pluginIDs(quint64 senderID)const
{
    return m_switch->pluginIDs(senderID);
}

QString ICRouter::nodeName(quint64 pluginID)const
{
    return m_switch->nodeName(pluginID);

}

QString ICRouter::pluginName(quint64 pluginID)const
{
    return m_switch->pluginName(pluginID);
}

void ICRouter::configureLocalNode(ICLocalNode *lnode)
{
    QString name = setting("localnode/name").toString();
    if(name.isEmpty())
        name = createRandomName();
    lnode->setName(name);
    lnode->setID(icHash(name));
}

QString ICRouter::createRandomName()
{

    const QString possibleCharacters("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
    const int randomStringLength = 12;

    QString randomString;
    qsrand(QDateTime::currentMSecsSinceEpoch());
    for(int i=0; i<randomStringLength; ++i)
    {
        int index = qrand() % possibleCharacters.length();
        QChar nextChar = possibleCharacters.at(index);
        randomString.append(nextChar);
    }
    return randomString;
}

void ICRouter::onNewRemoteNode(const ICRemoteNode *node)
{
    connect(node, &ICRemoteNode::message, m_localNode.data(), &ICLocalNode::deliverMessage);
    m_switch->addNode(node);
    emit pluginsChanged();
}

void ICRouter::onRemoteNodeLost(const ICRemoteNode *node)
{
    disconnect(node,0,0,0);
    m_switch->removeNode(node);
    emit pluginsChanged();
}

}
