#include "qglobal.h"
#include "icgridmanager.h"
#include "ictcpstream.h"
#include "iclocalsettings.h"
#include "QCoreApplication"
#include "boost/container/map.hpp"
#include "boost/container/set.hpp"
#include "icprimarylink.h"
#include "icserverlink.h"
#include "icsimplelink.h"
#include "utilities.h"
#include "icdiffiehellmankeygenerator.h"
#include "icsecurestreamencoder.h"

namespace icrouting {

ICGridManager* ICGridManager::m_staticInstance = nullptr;

ICRETCODE ICGridManager::init(const QString& pref)
{
    auto ret = ICObject::init(pref);
    m_listener.init(pref);
    connect(&m_listener, &ICTcpServer::newConnection, this, &ICGridManager::onIncomingConnection);
    return ret;
}

void ICGridManager::start()
{
    m_listener.start();
    int numOfConnections = setting("connectivity/client/number").toInt(); // FIXME: а по-умолчанию чего???
    vector<ICNetworkDetails> serverAddresses = serverAddressesFromConfig(numOfConnections);
    for(int i = 0; i < numOfConnections; i++)
        initPrimarySession(serverAddresses.at(i));
}

void ICGridManager::initPrimarySession(ICNetworkDetails addr)
{

    ICConnection* newConnection = createConnection(addr);
    newConnection->setAutoReconnect(true);
    ICPrimaryLink* pSession = new ICPrimaryLink;
    pSession->setServerDetails(addr);
    connect(pSession, &ICPrimaryLink::discoveryInfoAvailable, this, &ICGridManager::onDiscoveryInfoAvailable);
    m_activeLinks.insert(addr, pSession);
    startSession(pSession, newConnection);
}

void ICGridManager::startSession(ICBaseLink *s, ICConnection *c)
{
    s->setConnection(c);
    connect(s, &ICBaseLink::nodeUp, this, &ICGridManager::onNodeUp);
    connect(s, &ICBaseLink::nodeDown, this, &ICGridManager::onNodeDown);
    connect(s, &ICBaseLink::closed, this, &ICGridManager::onLinkClosed);
    connect(s, &ICBaseLink::aborted, this, &ICGridManager::onLinkAborted);
    connect(s, &ICBaseLink::pluginMessage, this, &ICGridManager::remoteMessage);
    connect(s, &ICBaseLink::pluginsChanged, this, &ICGridManager::remotePluginsChanged);
    s->open();
}

bool ICGridManager::sessionValid(ICBaseLink *s)
{
    ICNetworkDetails addr = s->serverDetails();
    auto sameAddrSession = m_activeLinks.find(addr);
    if(sameAddrSession != m_activeLinks.end())
    {
        if(s->priority() == sameAddrSession->second->priority())
        {
            return isaBetterAddress(*s);
        }
        else if(s->priority() > sameAddrSession->second->priority())
            return true;
        else return false;
    }
    return true;
}

bool ICGridManager::nodeNameValid(QString name)
{
    Q_UNUSED(name);
    return true;
    //TODO: name and hash collision resolution
}

void ICGridManager::onIncomingConnection(ICConnection *incomingConnection)
{
    ICServerLink* sSession = new ICServerLink;
    connect(sSession,&ICServerLink::peerServerAddressResolved, this, &ICGridManager::onPeerServerAddressResolved);
    ICNetworkDetails addr = toICNetworkAddress(incomingConnection->peerPortAddress());
    m_unresolvedLinks.insert(addr, sSession);
    startSession(sSession, incomingConnection);
}

void ICGridManager::onPeerServerAddressResolved(const ICNetworkDetails& serverAddr, const ICNetworkDetails& tempAddr)
{
    ICNetworkDetails serverA = serverAddr;
    ICNetworkDetails tempA = tempAddr;
    auto link = m_activeLinks.find(serverA);
    if(link != m_activeLinks.end())
        link->second->close();

    auto iter = m_unresolvedLinks.find(tempA);
    Q_ASSERT(iter != m_unresolvedLinks.end());
    auto p = m_unresolvedLinks.release(iter);
    ICBaseLink* s = p.release();
    m_activeLinks.insert(serverA,s);
}


void ICGridManager::onNodeUp()
{
    ICBaseLink *s = static_cast<ICBaseLink*>(sender());
    emit newNode(s->node());
}


void ICGridManager::onNodeDown()
{
    ICBaseLink *s = static_cast<ICBaseLink*>(sender());
    emit nodeLost(s->node());
}


void ICGridManager::onLinkClosed()
{
    ICBaseLink *s = static_cast<ICBaseLink*>(sender());
    ICNetworkDetails serverAddr = s->serverDetails();
    m_unresolvedLinks.erase(toICNetworkAddress(s->connection()->peerPortAddress()));
    m_activeLinks.erase(serverAddr);
}

void ICGridManager::onLinkAborted()
{
    ICBaseLink *s = static_cast<ICBaseLink*>(sender());
    ICNetworkDetails serverAddr = s->serverDetails();
    m_unresolvedLinks.erase(toICNetworkAddress(s->connection()->peerPortAddress()));
    m_activeLinks.erase(serverAddr);
    if(m_primaryAddresses.contains(serverAddr))
        initPrimarySession(serverAddr);
}

void ICGridManager::onDiscoveryInfoAvailable(const vector<ICNetworkDetails> &addrList)
{
    for(ICNetworkDetails addr : addrList)
    {
        auto sameAddrLink = m_activeLinks.find(addr);
        if(sameAddrLink == m_activeLinks.end())
        {
            ICSimpleLink* s = new ICSimpleLink;
            ICConnection* c = createConnection(addr);
            s->setServerDetails(addr);
            m_activeLinks.insert(addr,s);
            startSession(s, c);
        }
    }
}

ICConnection* ICGridManager::createConnection(const ICNetworkDetails& addr)const
{
    ICConnection* connection = new ICConnection;
    connection->init(settingsPrefix());
    connection->setConnectionAttribute(toString(addr));
    connection->setDataStream(new ICTcpStream(connection));
    connection->setKeyGenerator(new ICDiffieHellmanKeyGenerator(connection));
    connection->setDataEncoder(new ICSecureStreamEncoder(connection));
    connection->setOutgoing(true);
    return connection;
}


vector<ICNetworkDetails> ICGridManager::serverAddressesFromConfig(int numAddr)
{
// TODO: а нафига делать так, если можно опрерировать чем-то навроде ICLocalSettings::childGroups?
    vector<ICNetworkDetails> addresses;
    for(int i = 0; i < numAddr; ++i)
    {
        QString connection = QString("connectivity/client/connection%1/").arg(i);
        QHostAddress addr(setting(connection + "address").toString());
        quint32 ip = addr.toIPv4Address();
        quint16 port = setting(connection + "port").toInt();
        ICNetworkDetails address;
        address.ip = ip;
        address.port = port;
        addresses.push_back(address);
        m_primaryAddresses.insert(address);
    }
    return addresses;
}

bool ICGridManager::isaBetterAddress(const ICBaseLink& s)
{
    ICNetworkDetails local;
    local.ip= s.connection()->localAddress();
    local.port = m_listener.serverPort();

    if(s.serverDetails().ip > local.ip)
        return true;
    else if(s.serverDetails().ip < local.ip)
        return false;
    else if(s.serverDetails().port > local.port)
        return true;
    else
        return false;
}


ICDiscoveryInfo ICGridManager::discoveryInfo(const ICNetworkDetails& peerServerDetails)const
{
    ICDiscoveryInfo di;
    for(SessionIterC beg = m_activeLinks.begin(), end = m_activeLinks.end(); beg != end; ++beg)
    {
        if(!(beg->first == peerServerDetails))
                di.m_addresses.push_back(beg->first);
    }
    return di;
}

ICLinkVerificationInfo ICGridManager::linkVerificationInfo()const
{
    ICLinkVerificationInfo info;
    info.name = localNodeInfo().m_nodeName;
    info.port = m_listener.serverPort();
    return info;
}

}

