#include "icdiscoverylink.h"
#include "icdiscoverylinkprivate.h"

namespace icrouting {

ICDiscoveryLink::ICDiscoveryLink(ICDiscoveryLinkPrivate *p): ICBaseLink(p)
{
    connect(p, &ICDiscoveryLinkPrivate::discoveryInfoAvailable, this, &ICDiscoveryLink::discoveryInfoAvailable);
}

}
