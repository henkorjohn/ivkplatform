#include "utilities.h"
#include "QHostAddress"
#include "icnetworkdetails.h"

namespace icrouting {

ICNetworkDetails toICNetworkAddress(const QString &details)
{
    QStringList ip_port = details.split(":");
    Q_ASSERT(ip_port.size() == 2);
    ICNetworkDetails d;
    QHostAddress addr(ip_port.at(0));
    d.ip = addr.toIPv4Address();
    d.port = ip_port.at(1).toInt();
    return d;
}

QString toString(const ICNetworkDetails &details)
{
    QString string("%1 : %2");
    QHostAddress addr(details.ip);
    QString ipString = addr.toString();
    return string.arg(ipString).arg(details.port);
}



}
