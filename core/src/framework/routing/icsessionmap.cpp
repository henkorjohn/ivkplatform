#include "icsessionmap.h"

namespace icrouting {

bool ICLinkMap::contains(ICNetworkDetails address)
{
    auto iter = find(address);
    if(iter == end())
        return false;
    return true;
}

}
