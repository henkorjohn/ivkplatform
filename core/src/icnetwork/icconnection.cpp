#include "icconnection.h"
#include <QVariant>
#include "QDebug"

#define IC_DEFAULT_SECRET "default secret"

ICConnection::ICConnection(QVariant connectionAttribute, QObject* parent)
    : ICObject(parent),
      m_state(ConnectionClosedState),
      m_autoReconnect(true),
      m_outgoing(false),
      m_connectionAttribute(connectionAttribute),
      m_dataStream(nullptr),
      m_dataEncoder(nullptr),
      m_keyGenerator(nullptr)
{
    //инициализировать таймер отложенной отправки
    m_resendTimer.setSingleShot(false);
    connect(&m_resendTimer,&QTimer::timeout, this, &ICConnection::onSendingTimerShot);
}

void ICConnection::setConnectionAttribute(QVariant val)
{
    m_connectionAttribute =val;
    if(m_dataStream)
        m_dataStream->setConnectionAttribute(val);
}

void ICConnection::setDataStream(ICDataStream* dataStream)
{
    //Освободить существующий, если он существует
    if(m_dataStream)
        m_dataStream->deleteLater();
    //Назначить новый
    m_dataStream = dataStream;
    if(m_dataStream==0)
        return;
    //инициализировать префикс
    m_dataStream->init(settingsPrefix());
    //Подписаться к его сигналам
    connect(m_dataStream,  SIGNAL(opened()), this, SLOT(onStreamOpened()));
    connect(m_dataStream,&ICDataStream::closed, this, &ICConnection::onStreamClosed);
    connect(m_dataStream, SIGNAL(readyRead()), this,SLOT(onReadyRead()));
    //Инициализировать поток, передав ему атрибут подключения
    m_dataStream->setConnectionAttribute(m_connectionAttribute);
    m_dataStream->initialize();
}

void ICConnection::setDataEncoder(ICDataEncoder *dataEncoder)
{
    //Освободить существующий, если он существует
    if(m_dataEncoder)
        m_dataEncoder->deleteLater();
    //Назначить новый
    m_dataEncoder = dataEncoder;
    //инициализировать
    m_dataEncoder->init(settingsPrefix());
}

void ICConnection::setKeyGenerator(ICKeyGenerator *keyGenerator)
{
    //Освободить существующий, если он существует
    if(m_keyGenerator)
        m_keyGenerator->deleteLater();
    //Назначить новый
    m_keyGenerator = keyGenerator;
    if(m_keyGenerator==0)
        return;
    //Подписаться к его сигналам
    connect(m_keyGenerator, SIGNAL(sendKeyRequest(ICPACKET)),this, SLOT(onKeyRequestSending(ICPACKET)));
    connect(m_keyGenerator,SIGNAL(sendKeyResponse(ICPACKET)), this,SLOT(onKeyResponseSending(ICPACKET)));
    connect(m_keyGenerator, SIGNAL(keyGenerated(QByteArray)), this, SLOT(onKeyGenerated(QByteArray)));
}

void ICConnection::open()
{
    qDebug()<<"opening connection";
    auto currentState = state();
    if(currentState==ConnectionOpenningState || currentState==ConnectionReadyState)
        return;
    setState(ConnectionOpenningState);
    if(!m_dataStream)
        reportError("Не определен интерфейс для работы с каналом связи");
    else
        m_dataStream->open(outgoing());
    // если неопределен генератор ключа и соединение входящее, установить состояние соединение как открытое
    if(!(m_keyGenerator || outgoing()))
        setState(ConnectionReadyState);
}

void ICConnection::close()
{
    if(!m_dataStream)
        reportError("Не определен интерфейс для работы с каналом связи");
    else
        m_dataStream->close();
}

void ICConnection::send(QByteArray message,int ttl)
{
    ICPACKET packet = {{RequestPacket,ttl}, message};
    // отправить сообщение или поставить в очередь
    sendOrDelay(packet);
}

void ICConnection::onStreamOpened()
{
    // если определен интерфейс генерации и соединение исходящее, начать процедуру обмена ключами
    if(m_keyGenerator && outgoing())
    {
        m_keyGenerator->generateKey();
        setState(ConnectionAuthState);
    }
    // иначе установить состояние соединения как открытое
    else
        setState(ConnectionReadyState);
}

void ICConnection::onStreamClosed()
{
    // установить состояние соединения в закрытое
    setState(ConnectionClosedState);
    // сбросить ключ кодирования, если интерфейс кодирования потока определен
    if(m_dataEncoder)
        m_dataEncoder->resetKey();
    // если установлен флаг автоматической переустановки соединения и соединение исходящее,
    if(autoReconnect())
    {
        int reconnectDelay = setting("connectionPolicy/reconnectDelay",g_defaultReconnectDelay).toInt();
        // попытаться отложенно установить соединение вновь
        QTimer::singleShot(reconnectDelay,this,SLOT(open()));
    }
}

void ICConnection::onReadyRead()
{
    if(!m_dataStream)
    {
        reportError("Не определен интерфейс для работы с каналом связи");
        return;
    }
    // прочитать данные из потока в буффер
    auto readData = m_dataStream->read();
    if(readData.isEmpty())
    {
        reportError("Не удалось прочитать данные из потока");
        return;
    }
    m_readBuffer.append(readData);

    while(!m_readBuffer.isEmpty())
    {

        // попытаться выделить из буффера пакет, проверив его целостность
        quint32 packSize = 0;
        auto dataSize = m_readBuffer.size();
        if(dataSize<0)
            return;
        if((uint)dataSize<sizeof(packSize))
            return;
        auto sizeBuf = m_readBuffer.left(sizeof(packSize));
        packSize = *(decltype(&packSize)(sizeBuf.data()));
        // если пакет пришел не весь, выйти из функции
        if(packSize > m_readBuffer.size() - sizeof(packSize))
            return;
        //иначе:
        QByteArray checkBuf = m_readBuffer.mid(sizeof(packSize),packSize);
        //отрезать вычитанные из буффера данные
        m_readBuffer.remove(0,sizeof(packSize)+packSize);
        // выделить CRC
        quint16 crc = 0;
        auto crcBuf = checkBuf.right(sizeof(crc));
        crc = *(decltype(&crc)(crcBuf.data()));
        checkBuf.chop(sizeof(crc));
        // проверить CRC
        if(crc != qChecksum(checkBuf.constData(),checkBuf.size()))
        {
            reportError("Контрольная сумма полученного пакета некорректна");
            return;
        }
        ICPACKET packet;
        packet.header = *(decltype(&packet.header)(checkBuf.constData()));
        packet.body = checkBuf.remove(0,sizeof(packet.header));
        //----------------
        // если интерфейс кодирования определен и сообщение закодировано, декодировать данные
        if(m_dataEncoder && (packet.header.packetFlags & EncryptedPacket))
        {
            bool ok = false;
            packet.body = m_dataEncoder->decode(packet.body,&ok);
            // если декодирование не удалось, выйти из функции, сообщив об ошибке
            if(!ok)
            {
                reportError("Не удалось декодировать сообщение");
                return;
            }
        }
        //----------------
        if(packet.header.packetFlags & KeyExchangePacket)
        {
            // служебный пакет, содержащий публичный ключ
            if(!m_keyGenerator)
            {
                // если генератор сеансового ключа неопределен, закрыть соединение, сообщив об ошибке
                close();
                reportError("Удаленный узел запрашивает установку защищенного соединения,"
                            "но интерфейс для генерации сеансового ключа не определен");
            }
            if(packet.header.packetFlags & ReplyPacket)
                m_keyGenerator->handleKeyResponse(packet);
            else
            {
                setState(ConnectionAuthState);
                m_keyGenerator->handleKeyRequest(packet);
            }
        }
        else
        {
            // послать сигнал о получении нового сообщения
            qDebug()<<"About to handle packet";
            emit received(packet.body);
        }
    }
    qDebug()<<"Packet handled";
}

void ICConnection::onKeyRequestSending(ICPACKET request)
{
    // попытаться отправить сообщение
    if(!trySend(request))
        // установить состояние в закрытое
        setState(ConnectionClosedState);
}

void ICConnection::onKeyResponseSending(ICPACKET response)
{
    // попытаться отправить сообщение
    if(!trySend(response))
        // установить состояние в закрытое
        setState(ConnectionClosedState);
}

void ICConnection::sendOrDelay(ICPACKET &packet)
{
    // попытаться отправить сообщение
    if(trySend(packet))
        // если сообщение отправлено, выйти из функции
        return;
    // декриментить ttl пакета
    packet.header.ttl--;
    // если ttl<=0, выйти из функции
    if(packet.header.ttl<=0)
    {
        reportError("Не удалось доставить пакет получателю");
        return;
    }
    // поставить сообщение в очередь отправки
    enqueuePacket(packet);
}

bool ICConnection::trySend(ICPACKET &packet)
{
    if(!m_dataStream)
    {
        reportError("Не определен интерфейс для работы с каналом связи");
        return false;
    }
    // если соединение не открыто, сообщить об ошибке и вернуть false
    if(state()!=ConnectionReadyState)
    {
        if(packet.header.packetFlags & KeyExchangePacket)
        {
            // если пакет относится к процедуре генерации ключа
            if(state()==ConnectionClosedState)
            {
                reportError("Не удалось отправить публичный ключ: подключение не установлено");
                return false;
            }
        }
        else
            return false;
    }
    if(m_dataEncoder && !(packet.header.packetFlags & KeyExchangePacket))
    {
        // кодировать сообщение, если интерфейс кодирования потока определен и
        // пакет не относится к процедуре генерации ключа

        packet.body = m_dataEncoder->encode(packet.body);

        // установить флаг "закодировано", если кодирование успешно
        packet.header.packetFlags |= EncryptedPacket;
    }
    // упаковать структуру
    QByteArray writeData;
    writeData.append((const char*)&packet.header,sizeof(packet.header));
    writeData.append(packet.body);
    // вычислить CRC и добавить в конец
    quint16 crc = qChecksum(writeData.constData(),writeData.length());
    writeData.append((const char*)&crc,sizeof(crc));
    // добавить размер массива в байтах в начало
    quint32 packSize = writeData.size();
    writeData.prepend((const char*)&packSize,sizeof(packSize));
    // отправить сообщение
    m_dataStream->write(writeData);
    return true;
}

void ICConnection::enqueuePacket(const ICPACKET &packet)
{
    m_sendQueue.enqueue(packet);
    // если таймер не запущен, запустить
    bool ok = false;
    int resendInterval = setting("connectionPolicy/resendInterval",g_defaultResendInterval).toInt(&ok);
    if(!ok)
        resendInterval = g_defaultResendInterval;
    m_resendTimer.setInterval(resendInterval);
    if(!m_resendTimer.isActive())
        m_resendTimer.start();
}

void ICConnection::onKeyGenerated(QByteArray key)
{
    if(!m_dataEncoder)
        return;
    // прочитать из настроек дополнительный приватный ключ
    QByteArray secretKey = setting("security/secretKey",IC_DEFAULT_SECRET).toByteArray();
    // дополнить сеансовый ключ дополнительным приватным ключом
    QByteArray resultKey = key + secretKey;
    // если интерфейс кодирования потока определен, передать ему полученный ключ
    m_dataEncoder->setKey(resultKey);
    setState(ConnectionReadyState);
}

void ICConnection::onSendingTimerShot()
{
    int cnt = m_sendQueue.size();
    for(int i=0;i<cnt;i++)
    {
        // достать сообщение из очереди
        auto packet = m_sendQueue.takeFirst();
        // отправить сообщение или поставить в очередь
        sendOrDelay(packet);
    }
    // если очередь пуста, остановить таймер
    if(m_sendQueue.isEmpty())
        m_resendTimer.stop();
}
