#include "icdiffiehellmankeygenerator.h"

QCA::Initializer* g_qcaInit = nullptr; // initializer of QCA

ICDiffieHellmanKeyGenerator::ICDiffieHellmanKeyGenerator(QObject *parent): ICKeyGenerator(parent)
{
    if(!g_qcaInit)
        g_qcaInit = new QCA::Initializer;
}

void ICDiffieHellmanKeyGenerator::generateKey()
{
    // Клиент генерирует псевдослучайное число a
    QCA::KeyGenerator qcaKeyGen;
    QCA::DLGroup dlGroup = qcaKeyGen.createDLGroup(QCA::IETF_1024);
    m_privateKey = qcaKeyGen.createDH(dlGroup).toDH();
    // Вычисляет значение Ka = g ^ a mod p
    QCA::DHPublicKey pubKey = m_privateKey.toPublicKey().toDH();
    // Формирует сообщение серверу
    ICPACKET keyRequest = {{RequestPacket|KeyExchangePacket,0},QByteArray()};
    QDataStream reqDataOutput(&keyRequest.body,QIODevice::WriteOnly);
    reqDataOutput << dlGroup.g() << dlGroup.p() << pubKey.y();
    // посылает сигнал sendKeyRequest()
    emit sendKeyRequest(keyRequest);
}

void ICDiffieHellmanKeyGenerator::handleKeyRequest(ICPACKET request)
{
    // Сервер разбирает сообщение, выделив g,p,Ka
    QDataStream reqDataInput(&request.body,QIODevice::ReadOnly);
    QCA::BigInteger g,p,Ka;
    reqDataInput >> g >> p >> Ka;
    QCA::DLGroup dlGroup(p,g);
    QCA::DHPublicKey clientPubKey(dlGroup,Ka);
    // генерирует псевдослучайное число b
    QCA::KeyGenerator qcaKeyGen;
    m_privateKey = qcaKeyGen.createDH(dlGroup).toDH();
    // вычисляет значение Kb = g ^ b mod p
    auto Kb = m_privateKey.toPublicKey().toDH().y();
    // формирует ответ клиенту
    ICPACKET keyResponse = {{KeyExchangePacket|ReplyPacket,0},QByteArray()};
    QDataStream responseDataOutput(&keyResponse.body,QIODevice::WriteOnly);
    responseDataOutput << Kb;
    // посылает сигнал sendKeyResponse()
    emit sendKeyResponse(keyResponse);
    // вычисляет сеансовый ключ Kba = Ka ^ b mod p
    QCA::SymmetricKey sesKey = m_privateKey.deriveKey(clientPubKey);
    // посылает сигнал keyGenerated()
    emit keyGenerated(sesKey.toByteArray());
}

void ICDiffieHellmanKeyGenerator::handleKeyResponse(ICPACKET response)
{
    // Клиент разбирает ответ сервера, выделив Kb
    QDataStream respDataInput(&response.body,QIODevice::ReadOnly);
    QCA::BigInteger Kb;
    respDataInput >> Kb;
    QCA::DHPublicKey serverPubKey(m_privateKey.toDH().domain(),Kb);
    // вычисляет Kab = (Kb ^ a mod p)
    QCA::SymmetricKey sesKey = m_privateKey.deriveKey(serverPubKey);
    // посылает сигнал keyGenerated()
    emit keyGenerated(sesKey.toByteArray());
}

QDataStream & operator <<(QDataStream& output,const QCA::BigInteger& val)
{
    output << val.toString();
    return output;
}

QDataStream & operator >> (QDataStream& input,QCA::BigInteger& res)
{
    QString str;
    input >> str;
    bool ok = res.fromString(str);
    Q_ASSERT(ok);
    return input;
}
