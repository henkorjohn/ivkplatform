#ifndef ICNODESWITCHTEST_H
#define ICNODESWITCHTEST_H

#include "QObject"
#include "icnodeswitch.h"
#include "icmocknode.h"


namespace icrouting {

const quint32 TEST_NODE_ID = 5;

class ICNodeSwitchTest: public QObject
{
    Q_OBJECT
public:

private slots:

    //    void initTestCase();

    //    void cleanupTestCase();

    //    void init();

    void cleanup();

    void testPluginIDs();

    void testPluginIDsByName();

    void testNodeName();

    void testPluginName();

    void testRoute();
private:
    void testExistingNodeName();
    void testNonExistingNodeName();

    void testExistingPluginName();
    void testNonExistingPluginName();

    void testExistingRoute();

    void testNonExistingRoute();
    void testNonExistingNodeRoute();
    void testNonExistingPluginRoute();

    static const quint64 numNodes = 10;
    ICNodeSwitch nodeSwitch;
    QList<ICMockNode*> nodeList;
};

}

#endif // ICNODESWITCHTEST_H
