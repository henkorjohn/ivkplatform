#ifndef ICNODETEST_H
#define ICNODETEST_H
#include "QObject"
#include "iclocalnode.h"
#include "icremotenode.h"


namespace icrouting {

class ICAbstractNodeTest: public QObject
{
    Q_OBJECT
public:
    ICAbstractNodeTest();


private slots:

    void testPluginIDs();

    void testPluginIDsByName();

    void testPluginName();

private:
    
//ICLocalNode m_localNode;
//ICRemoteNode m_remoteNode;

};


}



#endif // ICNODETEST_H
