#include <QCoreApplication>
#include <stdio.h>
#include <QThread>
#include <icplatformloader.h>
#include "myplatform.h"
#include <QDebug>
#include <icpluginloader.h>
#include <QFile>
#include <QTextStream>

MyPlatform::MyPlatform()
{
    connect(this, &MyPlatform::init, this, &MyPlatform::platformInit);
    emit init();
}

void MyPlatform::platformInit()
{
    m_timer = new QTimer();
    ICPlatformLoader loader;
    QStringList args = QCoreApplication::arguments();
    if(args.count()>1)
        m_prefix = args.at(1);
    connect(&loader, &ICPlatformLoader::platformLoaded, this, &MyPlatform::onPlatformLoaded);
    loader.loadPlatform(m_prefix);
}

void MyPlatform::onPlatformLoaded(ICPluginLoader* loader)
{
    qDebug()<<"onPlatformLoaded";
    plugin = loader->loadedPlugins().at(0);
    connect(m_timer, &QTimer::timeout, this, &MyPlatform::onTimer);
    m_timer->setSingleShot(false);
    m_timer->start(1000);
}

void MyPlatform::onTimer()
{
    /*    QFile f(m_prefix+".log");
    f.open(QIODevice::Append);
    QTextStream fout(&f);
    fout<<"onTimer\n";*/
    plugin->pushMessage(0, QByteArray());
}

int main(int argc, char** argv)
{
    QCoreApplication app(argc, argv);
    printf("DummyPlatform\n");
    MyPlatform p;
    app.exec();
}
