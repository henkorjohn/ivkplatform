QT += testlib network


SOURCES += \
    ../../src/framework/plugininterfaces/icplugininterface.cpp \
    ../../src/framework/plugininterfaces/icplatformlayer.cpp \
    ../../src/framework/misc/icplatformloader.cpp \
    ../../src/framework/misc/icpluginloader.cpp \
    ../../src/icsettings/iclocalsettings.cpp \
    ../../src/framework/routing/icrouter.cpp \
    ../../src/framework/routing/icclienthandshake.cpp \
    ../../src/framework/routing/ichandshake.cpp \
    ../../src/framework/routing/iclocalnode.cpp \
    ../../src/framework/routing/iclocker.cpp \
    ../../src/framework/routing/icmessages.cpp \
    ../../src/framework/routing/icnetworkdetails.cpp \
    ../../src/framework/routing/icnodeswitch.cpp \
    ../../src/framework/routing/icremotenode.cpp \
    ../../src/framework/routing/icremotenodesmanager.cpp \
    ../../src/framework/routing/icserverhandshake.cpp \
    ../../src/framework/network/icconnection.cpp \
    ../../src/framework/network/icdataencoder.cpp \
    ../../src/framework/network/icdatastream.cpp \
    ../../src/framework/network/ictcpstream.cpp \
    ../../src/framework/network/ictcpserver.cpp \
    ../../src/framework/network/iciostream.cpp \
    ../../src/framework/network/icnetworkstream.cpp \
    ../../src/framework/network/icdiffiehellmankeygenerator.cpp \
    ../../src/framework/network/ickeygenerator.cpp \
    ../../src/framework/network/icsecurestreamencoder.cpp \
    ../../src/framework/network/icchipherencoder.cpp \
    ../../src/framework/network/sosemanuk.cpp \
    ../../src/framework/network/iccompressencoder.cpp \
    pluginloader.cpp

#SUBDIRS += ./dummyplugin \
#    ../routing/routing.pro

HEADERS += \
    ../../include/icsettings.h \
    ../../include/iclocalsettings.h \
    ../../include/icplugininterface.h \
    ../../include/icpluginloader.h \
    ../../include/icplatformloader.h \
    ../../include/icplatformlayer.h \
    ../../include/icrouter.h \
    ../../include/icplugincontext.h \
    ../../include/icabstractnode.h \
    ../../include/iclocker.h \
    ../../include/ichandshake.h \
    ../../include/iclocalnode.h \
    ../../include/icnetwork.h \
    ../../include/icnetworkdetails.h \
    ../../include/icnodeswitch.h \
    ../../include/icremotenode.h \
    ../../include/icremotenodesmanager.h \
    ../../include/icserverhandshake.h \
    ../../include/icconnection.h \
    ../../include/icconnectionfactory.h \
    ../../include/icdatastream.h \
    ../../include/icdataencoder.h \
    ../../include/iciostream.h \
    ../../include/icnetworkstream.h \
    ../../include/ictcpserver.h \
    ../../include/ictcpstream.h \
    ../../include/icclienthandshake.h \
    ../../include/ickeygenerator.h \
    ../../include/iccompressencoder.h \
    ../../include/icchipherencoder.h \
    ../../include/icsecurestreamencoder.h \
    pluginloader.h

LIBS += -lqca -L$$PWD/../../../bin -licsettings -licobject
