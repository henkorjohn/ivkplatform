#ifndef PLUGINLOADER_H
#define PLUGINLOADER_H

#include <QtTest/QtTest>
#include <icpluginloader.h>

class PluginLoaderTest : public QObject
{
    Q_OBJECT
private slots:
    void initTestCase();
    void cleanupTestCase();
//    void init(){}
//    void cleanup(){}
// tests
    void pluginsForLoading();
    void loadCallUnloadPlugin();
private:
    ICPluginLoader* m_pl;
};

#endif // PLUGINLOADER_H
