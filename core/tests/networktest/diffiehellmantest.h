#ifndef DIFFIEHELLMANTEST_H
#define DIFFIEHELLMANTEST_H

#include <QObject>
#include <icdiffiehellmankeygenerator.h>

/**
 * @brief Тест класса, реализующего алгоритм Диффи-Хельмана для генерации сеансового ключа.
 */
class DiffieHellmanTest : public QObject
{
    Q_OBJECT
public:
    explicit DiffieHellmanTest(QObject *parent = 0);
public slots:
    void onKeyGenerated(QByteArray sessionKey);
private slots:
    /**
     * @brief Интеграционный тест, проверяющий всю процедуру генерации ключа для обеих сторон.
     */
    void testKeyGeneration();
private:
    QList<QByteArray> m_keys;
    ICDiffieHellmanKeyGenerator m_clientKeyGenerator;
    ICDiffieHellmanKeyGenerator m_serverKeyGenerator;
};

#endif // DIFFIEHELLMANTEST_H
