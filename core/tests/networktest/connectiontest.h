#ifndef CONNECTIONTEST_H
#define CONNECTIONTEST_H

#include <ictcpserver.h>

/**
 * @brief Группа тестов для класса ICConnection
 */
class ConnectionTest : public QObject
{
    Q_OBJECT

    enum ConnectionTests
    {
        UnknownTest,
        ErrorTest,          ///< Тест обработки ошибок
        OpenTest,           ///< Тест установки незащищенного соединения
        SendRecieveTest,    ///< Тест передачи-приема пакетов через незащищенное соединение
        ClientCloseTest,    ///< Тест закрытия соединения клиентом
        KeyExchangeTest,    ///< Тест установки защищенного соединения
        EncodeDecodeTest,   ///< Тест передачи-приема пакетов через защищенное соединение
        ServerCloseTest,    ///< Тест закрытия соединения сервером
        AutoReconnectTest,  ///< Тест автоматической переустановки соединения
        LostAndRecoveryTest ///< Тест потери и восстановления подключения
    };

public:
    explicit ConnectionTest(QObject *parent = 0);
public slots:
    void onError(ICError error);
    void onServerAcceptConnection(ICConnection* servConnection);
    void onClientStateChanged(ICConnection::ICConnectionState state);
    void onServerStateChanged(ICConnection::ICConnectionState state);
    void onServerReceived(QByteArray message);
private slots:
    /**
     * @brief Тест №1. Обработка ошибок
     */
    void testError();
    /**
     * @brief Тест №2.Тест установки незащищенного соединения
     */
    void testOpen();
    /**
     * @brief Тест №3.Тест передачи-приема пакетов через незащищенное соединение
     */
    void testSendRecieve();
    /**
     * @brief Тест №4.Тест закрытия соединения клиентом
     */
    void testClientClose();
    /**
     * @brief Тест №5.Тест установки защищенного соединения
     */
    void testKeyExchange();
    /**
     * @brief Тест №6.Тест передачи-приема пакетов через защищенное соединение
     */
    void testEncodeDecode();
    /**
     * @brief Тест №7.Тест закрытия соединения сервером
     */
    void testServerClose();
    /**
     * @brief Тест №8.Тест автоматической переустановки соединения
     */
    void testAutoReconnect();
    /**
     * @brief Тест №9.Тест потери и восстановления подключения
     */
    void testLostAndRecovery();
private:
    ICConnection m_clientConnection;
    ICConnection *m_serverConnection;
    bool m_success;
    ConnectionTests m_currentTest;
    ICTcpServer m_server;
};

#endif // CONNECTIONTEST_H
