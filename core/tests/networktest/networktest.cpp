#include <QCoreApplication>
#include <QtTest/QTest>
#include <tcpstreamtest.h>
#include <diffiehellmantest.h>
#include <encodertest.h>
#include <connectiontest.h>
#include <clientloadertest.h>

int main(int argc,char* argv[])
{
    QCoreApplication app(argc,argv);
    Q_UNUSED(app);
    TcpStreamTest tcpStreamTest;
    QTest::qExec(&tcpStreamTest);
    DiffieHellmanTest dhTest;
    QTest::qExec(&dhTest);
    EncoderTest encoderTest;
    QTest::qExec(&encoderTest);
    ConnectionTest connectionTest;
    QTest::qExec(&connectionTest);
    ClientLoaderTest  clientLoaderTest;
    QTest::qExec(&clientLoaderTest);
}
