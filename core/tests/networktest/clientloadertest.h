#ifndef CLIENTLOADERTEST_H
#define CLIENTLOADERTEST_H

#include <ictcpserver.h>
#include <ictcpclientsloader.h>

/**
 * @brief Тесты для класса ICTcpClientsLoader
 */
class ClientLoaderTest : public QObject
{
    Q_OBJECT
public:
    explicit ClientLoaderTest(QObject *parent = 0);
public slots:
    void onNewClientConnection(ICConnection* connection);
    void onNewServerConnection(ICConnection* connection);
    void onMessageRecieved(QByteArray message);
private slots:
    /**
     * @brief Формирует тестовые конфигурации для теста №1.
     */
    void testLoading_data();
    /**
     * @brief Тест №1. Загрузка тестовой конфигурации с созданием клиента с требуемым протоколом обмена.
     * Установка соединения с тестовым TCP-сервером, передача ему тестового сообщения.
     */
    void testLoading();
private:
    ICTcpClientsLoader m_clientsLoader;
    ICTcpServer m_server;
    ICConnection* m_clientConnection;
    ICConnection* m_serverConnection;
    bool m_success;
};

#endif // CLIENTLOADERTEST_H
