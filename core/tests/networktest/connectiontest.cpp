#include "connectiontest.h"
#include <QtTest/QTest>
#include <icerrorhandler.h>
#include <QTcpSocket>
#include <icdiffiehellmankeygenerator.h>
#include <ictcpstream.h>
#include <icsecurestreamencoder.h>

#define IC_BEGIN_TEST(x){m_success = false; m_currentTest = x;}
#define IC_END_TEST     {QTRY_VERIFY_WITH_TIMEOUT(m_success,3000);}
#define IC_TEST_STRING  "Test"
#define IC_TEST_IP      "127.0.0.1"
#define IC_TEST_PORT    IC_DEFAULT_TCP_PORT

ConnectionTest::ConnectionTest(QObject *parent) :
    QObject(parent)
{
    m_success = false;
    m_currentTest = UnknownTest;
    m_serverConnection = 0;
    connect(ICErrorHandler::instance(),SIGNAL(error(ICError)),SLOT(onError(ICError)));
    m_clientConnection.init("");
    m_clientConnection.setConnectionAttribute(QString("%1:%2").arg(IC_TEST_IP).arg(IC_TEST_PORT));
    // Создать сервер, слушающий тестовый TCP-порт
    m_server.init("");
    m_server.setSecure(false);
    connect(&m_server,SIGNAL(newConnection(ICConnection*)),SLOT(onServerAcceptConnection(ICConnection*)));
    m_server.start();

    m_clientConnection.setDataStream(new ICTcpStream(&m_clientConnection));
    m_clientConnection.setOutgoing(true);
    m_clientConnection.setAutoReconnect(false);
    //назначить обработчики сигналов для объектов-подключений
    connect(&m_clientConnection,SIGNAL(stateChanged(ICConnection::ICConnectionState)),SLOT(onClientStateChanged(ICConnection::ICConnectionState)));
}

void ConnectionTest::onError(ICError error)
{
    if(m_currentTest==ErrorTest)
        m_success = (error.message() == QString(IC_TEST_STRING));
}

void ConnectionTest::onServerAcceptConnection(ICConnection *connection)
{
    if(m_serverConnection)
        m_serverConnection->deleteLater();
    m_serverConnection = connection;
    m_serverConnection->init("");
    connect(m_serverConnection,SIGNAL(stateChanged(ICConnection::ICConnectionState)),SLOT(onServerStateChanged(ICConnection::ICConnectionState)));
    connect(m_serverConnection,&ICConnection::received,this,&ConnectionTest::onServerReceived);
}

void ConnectionTest::onClientStateChanged(ICConnection::ICConnectionState state)
{
    switch(state)
    {
    case ICConnection::ConnectionReadyState:
        if(m_currentTest==OpenTest || m_currentTest == KeyExchangeTest
                || m_currentTest==AutoReconnectTest)
            m_success = (m_serverConnection->state()==ICConnection::ConnectionReadyState);
        break;
    case ICConnection::ConnectionClosedState:
        if(m_currentTest==ServerCloseTest)
            m_success = (m_serverConnection->state()==ICConnection::ConnectionClosedState);
        break;
    default:
        break;
    }
}

void ConnectionTest::onServerStateChanged(ICConnection::ICConnectionState state)
{
    switch(state)
    {
    case ICConnection::ConnectionClosedState:
        if(m_currentTest==ClientCloseTest)
            m_success = (m_clientConnection.state()==ICConnection::ConnectionClosedState);
        break;
    default:
        break;
    }
}

void ConnectionTest::onServerReceived(QByteArray message)
{
    m_success = (message == IC_TEST_STRING);
}

///////////////////// Tests /////////////////////

void ConnectionTest::testError()
{
    IC_BEGIN_TEST(ErrorTest);
    m_clientConnection.reportError(IC_TEST_STRING);
    IC_END_TEST;
}

void ConnectionTest::testOpen()
{
    IC_BEGIN_TEST(OpenTest);
    m_clientConnection.open();
    QCOMPARE(m_clientConnection.state(),ICConnection::ConnectionOpenningState);
    IC_END_TEST;
}

void ConnectionTest::testSendRecieve()
{
    IC_BEGIN_TEST(SendRecieveTest);
    m_clientConnection.send(IC_TEST_STRING,0);
    IC_END_TEST;
}

void ConnectionTest::testClientClose()
{
    IC_BEGIN_TEST(ClientCloseTest);
    m_clientConnection.close();
    IC_END_TEST;
}

void ConnectionTest::testKeyExchange()
{
    IC_BEGIN_TEST(KeyExchangeTest);
    m_server.setSecure(true);
    m_clientConnection.setKeyGenerator(new ICDiffieHellmanKeyGenerator(&m_clientConnection));
    m_clientConnection.setDataEncoder(new ICSecureStreamEncoder(&m_clientConnection));
    m_clientConnection.open();
    QCOMPARE(m_clientConnection.state(),ICConnection::ConnectionOpenningState);
    IC_END_TEST;
}

void ConnectionTest::testEncodeDecode()
{
    IC_BEGIN_TEST(EncodeDecodeTest);
    m_clientConnection.send(IC_TEST_STRING,0);
    IC_END_TEST;
}

void ConnectionTest::testServerClose()
{
    IC_BEGIN_TEST(ServerCloseTest);
    m_serverConnection->close();
    IC_END_TEST;
}

void ConnectionTest::testAutoReconnect()
{
    IC_BEGIN_TEST(AutoReconnectTest);
    m_server.stop();
    m_clientConnection.setAutoReconnect(true);
    QTRY_VERIFY_WITH_TIMEOUT(m_clientConnection.state()==ICConnection::ConnectionClosedState,1000);
    m_clientConnection.open();
    QTRY_VERIFY_WITH_TIMEOUT(m_clientConnection.state()==ICConnection::ConnectionOpenningState,1000);
    QTRY_VERIFY_WITH_TIMEOUT(m_clientConnection.state()==ICConnection::ConnectionClosedState,2000);
    m_server.start();
    IC_END_TEST;
}

void ConnectionTest::testLostAndRecovery()
{
    IC_BEGIN_TEST(LostAndRecoveryTest);
    m_serverConnection->close();
    m_server.stop();
    QTRY_VERIFY_WITH_TIMEOUT(m_clientConnection.state()==ICConnection::ConnectionClosedState,1000);
    m_clientConnection.send(IC_TEST_STRING);
    m_server.start();
    QTRY_VERIFY_WITH_TIMEOUT(m_clientConnection.state()==ICConnection::ConnectionReadyState,1000);
    IC_END_TEST;
}

#include <moc_connectiontest.cpp>
