#include "encodertest.h"
#include <icsecurestreamencoder.h>
#include <QtTest/QTest>
#include <QMetaType>

EncoderTest::EncoderTest(QObject *parent) :
    QObject(parent)
{
    qRegisterMetaType<ICCompressEncoder>("ICCompressEncoder");
    qRegisterMetaType<ICChipherEncoder>("ICChipherEncoder");
    qRegisterMetaType<ICSecureStreamEncoder>("ICSecureStreamEncoder");
}

void EncoderTest::testEncoder_data()
{
    QTest::addColumn<QString>("encoderType");
    QTest::newRow("ICCompressEncoder") << "ICCompressEncoder";
    QTest::newRow("ICChipherEncoder") << "ICChipherEncoder";
    QTest::newRow("ICSecureStreamEncoder") << "ICSecureStreamEncoder";
}

void EncoderTest::testEncoder()
{
    QFETCH(QString,encoderType);
    int typeId = QMetaType::type(encoderType.toUtf8().constData());
    QVERIFY(typeId!=QMetaType::UnknownType);
    ICDataEncoder* encoder = static_cast<ICDataEncoder*>(QMetaType::create(typeId));
    QVERIFY(encoder);
    encoder->init("");
    QByteArray originalData = QByteArray("Just a text for testing encoders!");
    QByteArray decodedData;
    QBENCHMARK
    {
        QByteArray encodedData = encoder->encode(originalData);
        decodedData = encoder->decode(encodedData);
    }
    QCOMPARE(decodedData,originalData);
    delete encoder;
}

void EncoderTest::testEncoderWithKey_data()
{
    QTest::addColumn<QString>("encoderType");
    QTest::newRow("ICChipherEncoder") << "ICChipherEncoder";
    QTest::newRow("ICSecureStreamEncoder") << "ICSecureStreamEncoder";
}

void EncoderTest::testEncoderWithKey()
{
    QFETCH(QString,encoderType);
    int typeId = QMetaType::type(encoderType.toUtf8().constData());
    QVERIFY(typeId!=QMetaType::UnknownType);
    ICDataEncoder* encoder = static_cast<ICDataEncoder*>(QMetaType::create(typeId));
    QVERIFY(encoder);
    encoder->init("");
    // установить ключ кодирования
    encoder->setKey(QByteArray("Just a key for encoding test"));
    QByteArray originalData = QByteArray("Just a text for testing encoders!");
    QByteArray decodedData;
    QBENCHMARK
    {
        QByteArray encodedData = encoder->encode(originalData);
        decodedData = encoder->decode(encodedData);
    }
    QCOMPARE(decodedData,originalData);
    delete encoder;
}

#include "moc_encodertest.cpp"
