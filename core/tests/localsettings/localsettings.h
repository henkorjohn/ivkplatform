#ifndef LOCALSETTINGS_H
#define LOCALSETTINGS_H

#include <QtTest/QtTest>
#include <iclocalsettings.h>
#include <QMap>

class LocalSettings : public QObject
{
    Q_OBJECT
private slots:
    void initTestCase();     ///< записывает данные для тестов в конфиг
    void cleanupTestCase();  ///< удаляет тестовый конфиг
// tests
    void testCreatingFile(); ///< тестирует создание файла
    void testAllKeys();      ///< тестирует получение всех ключей 
    void testChildGroups();  ///< тестирует получение всех дочерних групп
    void testChildKeys();    ///< тестирует получение ключей корня
    void testChildKeysInGroup(); ///< тестирует получение ключей группы
    void testAddRemoveKey();  ///< тестирует удаление/добавление ключа
    void testReadOnlyKey();  ///< тестирует чтение и запись ключей в файлах "только для чтения"

private:
    void writeTestSettings(); ///< записывает тестовые настройки в файл
    QMap<QString, int> m_etalons;
};

#endif // LOCALSETTINGS_H
