#ifndef SETTINGSWRITTER_H
#define SETTINGSWRITTER_H

#include <icobject.h>

class SettingsWritter : public ICObject
{
    Q_OBJECT
public:
    explicit SettingsWritter(QObject *parent = 0);
public slots:
    void updateStatus();
};

#endif // SETTINGSWRITTER_H
