#ifndef SETTINGSWATCHER_H
#define SETTINGSWATCHER_H

#include <QObject>
#include <icobject.h>
#include <testconst.h>

class SettingsWatcher : public ICObject
{
    Q_OBJECT
#ifdef TEST_LOCAL
    IC_LOCAL_SETTINGS(status)
#else
    IC_GLOBAL_SETTINGS(status)
#endif

public:
    explicit SettingsWatcher(QObject *parent = 0);

    Q_PROPERTY(QString status READ status WRITE setStatus NOTIFY statusChanged)
    inline QString status() const{return m_status;}
    inline void setStatus(const QString& val){if(m_status==val) return; m_status=val;emit statusChanged();}
signals:
    void statusChanged();
private slots:
    void onStatusChanged();
private:
    QString m_status = INIT_STATUS;
};

#endif // SETTINGSWATCHER_H
