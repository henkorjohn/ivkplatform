#include "icobjecttest.h"
#include <QtTest/QTest>
#include <settingswatcher.h>
#include <testconst.h>
#include <QThread>
#include <QDebug>

ICObjectTest::ICObjectTest()
{
    m_writeThread = new QThread();
    m_writter = new SettingsWritter();
    m_writter->moveToThread(m_writeThread);
    connect(this,&ICObjectTest::update,m_writter,&SettingsWritter::updateStatus/*,Qt::QueuedConnection*/);
}

ICObjectTest::~ICObjectTest()
{
    if(m_writter!=nullptr)
        m_writter->deleteLater();
}

void ICObjectTest::initTestCase()
{
    m_writter->init(SETTINGS_GROUP);
    m_writeThread->start();
    QTRY_VERIFY_WITH_TIMEOUT(m_writeThread->isRunning(),1000);
}

void ICObjectTest::cleanupTestCase()
{
    if(m_writeThread!=nullptr)
    {
        m_writeThread->exit();
        QTRY_VERIFY_WITH_TIMEOUT(m_writeThread->isFinished(),1000);
        m_writeThread->deleteLater();
    }
}

void ICObjectTest::testSettings()
{
    QList<SettingsWatcher*> wts;
    for(int i=0;i<WATCHER_CNT;i++)
    {
        auto w = new SettingsWatcher(this);
        w->init(SETTINGS_GROUP);
        wts << w;
    }
    emit update();
    QTRY_COMPARE_WITH_TIMEOUT(m_succeeded,WATCHER_CNT,1000);
}

void ICObjectTest::testReading()
{
    QCOMPARE(m_writter->setting(SETTING_NAME,INIT_STATUS
#ifndef TEST_LOCAL
             ,false
#endif
             ).toString(),QString(READY_STATUS));
}

#include <moc_icobjecttest.cpp>
