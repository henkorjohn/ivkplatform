#include <QCoreApplication>
#include <icobjecttest.h>
#include <QtTest/QTest>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    a.setOrganizationName("ivk-center");
    ICObjectTest objTest;
    return QTest::qExec(&objTest);
}
