#ifndef INCOMINGLINK_H
#define INCOMINGLINK_H

#include "icsessionstate.h"
#include "icdiscoverylink.h"
#include "icserverlinkprivate.h"
#include "QDebug"

namespace icrouting {

class ICServerLink: public ICDiscoveryLink
{
    Q_OBJECT
public:
    ICServerLink();
signals:
    void peerServerAddressResolved(const ICNetworkDetails& serverAddr, const ICNetworkDetails& tempAddr);
private:
    ICServerLinkPrivate* myImple()const{return static_cast<ICServerLinkPrivate*>(impl());}
};




}







#endif // INCOMINGLINK_H
