#ifndef ICDISCOVERY_H
#define ICDISCOVERY_H
#include "icnetworkdetails.h"
#include "vector"
#include "icbaselink.h"


namespace icrouting {

class ICDiscoveryLinkPrivate;

class ICDiscoveryLink: public ICBaseLink
{
    Q_OBJECT
signals:
    void discoveryInfoAvailable(const std::vector<ICNetworkDetails>);

protected:
    ICDiscoveryLink(ICDiscoveryLinkPrivate*p);
private:
    ICDiscoveryLink();
};


}

#endif // ICDISCOVERY_H
