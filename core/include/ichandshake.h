#ifndef ICHANDSHAKE_H
#define ICHANDSHAKE_H
#include "iclocalnode.h"
#include "icremotenode.h"
#include "icconnection.h"
#include "memory"

using std::unique_ptr;

namespace icrouting {

class ICHandshake: public QObject
{

    Q_OBJECT
public:

    ICHandshake(const ICLocalNode*node, ICConnection* connection):m_localNode(node),m_connection(connection){}
    virtual ~ICHandshake(){/*m_connection->close();*/}
    void stop(){/*m_connection->close();*/}
    virtual void establishLink() = 0;
    virtual void startNegotiation() = 0;
    virtual void finishNegotiation() = 0;

signals:
    void nodeEstablished(ICAbstractNode*);
    void linkLost(QVariant linkParameters);
    void linkUp();

protected:
    const ICLocalNode* m_localNode;
    ICConnection* m_connection;
};
typedef unique_ptr<ICHandshake> ICHandshakeU;
}


#endif // ICHANDSHAKE_H
