#ifndef ICNETWORK_H
#define ICNETWORK_H

/** Включает заголовочные файлы слоя Network, доступные другим слоям платформы. */

#include <icconnection.h>
#include <ictcpserver.h>
#include <ictcpclientsloader.h>

#endif // ICNETWORK_H
