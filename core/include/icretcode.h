#ifndef ICRETCODE_H
#define ICRETCODE_H

/**
 * @brief Коды, возвращаемые функциями системы. 
 * @ingroup core
 */
typedef enum _ICRETCODE {
    ICOK = 1,
    ICGENERROR = 0
} ICRETCODE;

#endif // ICRETCODE_H
