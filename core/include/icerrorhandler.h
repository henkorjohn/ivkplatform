#ifndef ICERRORHANDLER_H
#define ICERRORHANDLER_H

#include <icerror.h>
#include <icobject_global.h>

/**
 * @brief Класс-обработчик ошибок
 * @ingroup core
 */
class ICOBJECTSHARED_EXPORT ICErrorHandler : public QObject
{
    Q_OBJECT
public:
    ICErrorHandler(QObject* parent = 0)
        : QObject(parent) {}
    /**
     * @brief Централизованный обработчик ошибок.
     * @return ICErrorHandler*
     */
    static ICErrorHandler* instance(){return &m_instance;}
public slots:
    /**
     * @brief Слот-обработчик ошибки
     * @param error информация об ошибке
     */
    void onError(ICError error);
signals:
    /**
     * @brief Сигнал об ошибке для дополнительных обработчиков
     * @param error информация об ошибке
     */
    void error(ICError error);
private:
    static ICErrorHandler m_instance;
};


#endif // ICERRORHANDLER_H
