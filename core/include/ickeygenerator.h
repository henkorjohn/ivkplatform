#ifndef ICKEYGENERATOR_H
#define ICKEYGENERATOR_H

#include <icpacket.h>
#include <QObject>

/**
  * class ICKeyGenerator
  * @brief Класс - генератор сеансового ключа.
  * 
  * Реализует методы генерации сеансового ключа.
  * @ingroup core
  */
class ICKeyGenerator : public QObject
{
    Q_OBJECT
public:
    ICKeyGenerator(QObject* parent=0) : QObject(parent){}
    virtual ~ICKeyGenerator(){}
    /**
      * @brief Начать процедуру генерации сеансового ключа
      */
    virtual void generateKey ( );

public slots:
    /**
     * @brief Слот-обработчик запроса на аутентификацию.
     * @param request запрос
     */
    virtual void handleKeyRequest (ICPACKET request) =0;
    /**
      * @brief Обработчик ответа аутентифицирующего узла
      * @param  response Ответ аутентифицирующего узла
      */
    virtual void handleKeyResponse (ICPACKET response ) =0;

signals:  
    /**
      * Сигнал о необходимости отправки запроса на аутентификацию
      * @param  request Запрос на генерацию сеансового ключа
      */
    void sendKeyRequest (ICPACKET request);
    /**
     * @brief Сигнал о необходимости отправки ответа на запрос аутентификации
     * @param response Ответ аутентифицирующего узла
     */
    void sendKeyResponse(ICPACKET response);
    /**
      * Сигнал о готовности сеансового ключа
      * @param  key Сеансовый ключ
      */
    void keyGenerated (const QByteArray& key );
};

#endif // ICKEYGENERATOR_H
