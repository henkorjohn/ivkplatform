#ifndef ICDIFFIEHELLMANKEYGENERATOR_H
#define ICDIFFIEHELLMANKEYGENERATOR_H

#include "ickeygenerator.h"
#include <QtCrypto/QtCrypto>
#include <QDataStream>
#include "icnetwork_global.h"
/**
 * @brief Класс, реализующий протокол генерации сеансового ключа на основании алгоритма Диффи-Хельмана.
 *
 * Протокол:
 * 1. Клиент генерирует псевдослучайное число a (приватный ключ).
 * 2. Вычисляет значение Ka = g ^ a mod p (публичный ключ),
 * где p-безопасное простое число, g-первообразный корень по модулю p.
 * 3. Клиент отправляет g, p, Ka серверу.
 * 4. Сервер получив запрос, генерирует псевдослучайное число b (приватный ключ).
 * 5. Сервер вычисляет значение Kb = g ^ b mod p (публичный ключ) и отправляет клиенту.
 * 6. Сервер вычисляет сеансовый ключ Kba = Ka ^ b mod p.
 * 7. Клиент получив ответ сервера, вычисляет Kab = (Kb ^ a mod p) = (g ^ b ^ a mod p) = (g ^ a ^ b mod p) = (Ka ^ b mod p) = Kba .
 * @ingroup core
 */
class ICNETWORKSHARED_EXPORT ICDiffieHellmanKeyGenerator : public ICKeyGenerator
{
public:
    ICDiffieHellmanKeyGenerator(QObject* parent=0);
    void generateKey() override;
public slots:
    void handleKeyRequest(ICPACKET request) override;
    void handleKeyResponse(ICPACKET response) override;
private:
    QCA::DHPrivateKey m_privateKey;
};

QDataStream & operator << (QDataStream& output,const QCA::BigInteger& val);
QDataStream & operator >> (QDataStream& input,QCA::BigInteger& res);

#endif // ICDIFFIEHELLMANKEYGENERATOR_H
