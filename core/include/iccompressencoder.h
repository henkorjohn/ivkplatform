#ifndef ICZIPENCODER_H
#define ICZIPENCODER_H
#include "icdataencoder.h"
#include <QMetaType>

/**
 * @brief Класс, реализующий сжатие данных.
 *
 * Стандартная реализация сжатия использует функции библиотеки QtCore.
 * @ingroup core 
 */
class ICCompressEncoder : public ICDataEncoder
{
    Q_OBJECT
public:
    explicit ICCompressEncoder(QObject* parent=0);
    ICCompressEncoder(const ICCompressEncoder& copy) : ICDataEncoder(copy){Q_UNUSED(copy);}
    virtual ~ICCompressEncoder(){}
    QByteArray encode(const QByteArray &data);
    QByteArray decode(const QByteArray &data,bool *ok=nullptr);
};

#endif // ICZIPENCODER_H
