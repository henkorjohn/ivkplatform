#ifndef ICNODE_H
#define ICNODE_H

#include "QString"
#include "icobject.h"
#include "memory"

namespace icrouting {

using std::unique_ptr;

class ICAbstractNode: public ICObject
{
    Q_OBJECT
public:
    ICAbstractNode(QObject* parent=nullptr):ICObject(parent){}

    virtual ~ICAbstractNode(){}

    virtual QString name()const = 0;

    virtual void setName(QString name) = 0;

    virtual quint32 id()const = 0;

    virtual void setID(quint32 id) = 0;

    virtual QList<quint32> pluginTypeIDs()const = 0;

    virtual QList<quint32> pluginTypeID(QString pluginName)const = 0;

    virtual QString pluginName(quint32 pluginTypeID)const = 0;

    virtual bool hasPlugin(quint32 pluginTypeID)const = 0;

public slots:
    virtual void deliverMessage(quint64 srcPluginID, quint64 destPluginID, const QByteArray& message)const = 0;

signals:
    void message(quint64 srcPluginID, quint64 destPluginID, const QByteArray& message);
    void pluginsChanged();
private:

//private:
//    QString m_name;
//    quint32 m_id;
};

typedef QSharedPointer<ICAbstractNode> ICAbstractNodeSh;
typedef unique_ptr<ICAbstractNode> ICAbstractNodeU;
}
#endif // ICNODE_H
