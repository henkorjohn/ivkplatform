#ifndef ICDATAENCODER_H
#define ICDATAENCODER_H

#include <QByteArray>
#include <icobject.h>

#define IC_DEFAULT_KEY "default"    ///< Ключ кодирования по-умолчанию

/**
  * @brief Интерфейс классов, преобразующих потоки данных.
  *
  * Интерфейс описывает методы кодирования/декодирования информационных потоков
  * между узлами платформы.
  * @ingroup core
  */
class ICDataEncoder : public ICObject
{
    Q_OBJECT
public:
    explicit ICDataEncoder(QObject* parent=0) : ICObject(parent){}
    ICDataEncoder(const ICDataEncoder& copy);
    virtual ~ICDataEncoder(){}
    /**
      * @brief Кодирование потока данных
      * @param  data Данные.
      * @return кодированный массив данных
      */
    virtual QByteArray encode (const QByteArray& data ) = 0;
    /**
      * @brief Декодирование потока данных
      * @param  data Входные данные
      * @param  ok указатель на переменную, в которую будет записан статус операции (если =nullptr, то статус не обрабатывается)
      * @return декодированный массив данных
      */
    virtual QByteArray decode (const QByteArray& data,bool* ok = nullptr) = 0;
    /**
      * @brief Установить ключ кодирования
      * @param  key Ключ кодирования
      */
    virtual void setKey (const QByteArray& key ){Q_UNUSED(key);}
    /**
     * @brief сбросить ключ кодирования
     */
    void resetKey(){setKey(IC_DEFAULT_KEY);}
};

#endif // ICDATAENCODER_H
