#ifndef ICREMOTENODE_H
#define ICREMOTENODE_H
#include "icabstractnode.h"

class ICConnection;

namespace icrouting {

class ICRemoteNode: public ICAbstractNode
{
    Q_OBJECT
public:
    ICRemoteNode(QObject* parent=nullptr): ICAbstractNode(parent){}

    void setPlugins(const QStringList& list);

    void setConnection(ICConnection*c){Q_CHECK_PTR(c); m_connection = c;}

    void removePlugins(const QStringList &list);
    //virtuals

    quint32 id() const{return m_id;}

    void setID(quint32 id){m_id = id;}

    QString name() const{return m_name;}

    void setName(QString name){m_name = name;}

    QList<quint32> pluginTypeIDs()const;

    QList<quint32> pluginTypeID(QString pluginName)const;

    QString pluginName(quint32 pluginTypeID)const;

    bool hasPlugin(quint32 pluginTypeID) const;
public slots:
    void deliverMessage(quint64 srcPluginID, quint64 destPluginID, const QByteArray& message)const;

private:
    QHash<quint32,QString> m_pluginNameByID;
    ICConnection* m_connection;
    quint32 m_id;
    QString m_name;
};


typedef QSharedPointer<ICRemoteNode> ICRemoteNodeSh;
typedef std::unique_ptr<ICRemoteNode> ICRemoteNodeU;

}

#endif // ICREMOTENODE_H
