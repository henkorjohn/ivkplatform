#ifndef ICOBJECTSETTINGS_H
#define ICOBJECTSETTINGS_H

#include <icerrornotifier.h>
#include <icsettings.h>
#include <QHash>
#include <QSharedPointer>
#include <QMutex>

class ICObject;

/**
 * @brief Класс, реализующий фабрику интерфейсов доступа к настройкам для наследников ICObject.
 * @ingroup core settings
 */
class ICObjectSettings : public ICErrorNotifier
{
    Q_OBJECT
public:
    //methods:
    /**
     * @brief Запросить значение настройки
     * @param name имя настройки
     * @param defValue значение по-умолчанию
     * @param locally true - локально, false - глобально
     * @return значение
     */
    QVariant value(const QString& name,const QVariant& defValue,bool locally=true);
    /**
     * @brief Асинхронный запрос глобальной настройки
     * @param name имя настройки
     * @return значение, если настройка с данным именем найдена в кэше, иначе - Invalid
     */
    QVariant queryValue(const QString& name);
    /**
     * @brief Записать новое значение для настройки
     * @param name имя настройки
     * @param value значение
     * @param locally true - локально, false - глобально
     */
    void setValue(const QString& name,const QVariant& value,bool locally=true);
    /**
     * @brief Предоставить интерфейс доступа к настройкам
     * @param prefix группа настроек
     */
    static QSharedPointer<ICObjectSettings> settingsGroup(const QString& prefix);
    /**
     * @brief Добавить объект в список наблюдателей для настройки с указанным именем
     * @param propName имя настройки
     * @param watcher объект-наблюдатель
     */
    void appendWatcher(const QString& propName,ICObject* watcher);
    /**
     * @brief Удалить объект из списка наблюдателей всех настроек
     * @param watcher объект-наблюдатель
     */
    void removeWatcher(ICObject* watcher);
private slots:
    void onValueChanged(QString setting,QVariant value);
private:
    ICObjectSettings(const QString& prefix,QObject* parent = nullptr);
    ICSettings* settings(bool local);

    //members:
    QString m_prefix;
    static QHash<QString,QSharedPointer<ICObjectSettings> > g_settingGroups;
    static QMutex g_mutex;
    static QMutex g_clientLoadMutex;
    static QThread g_clientThread;
    ICSettings *m_localSettings = nullptr;
    ICSettings *m_globalSettings = nullptr;
    QMutex m_mutex;
    QHash<QString,QList<ICObject*> > m_watchers;
    QMutex m_watchersMutex;
};

#endif // ICOBJECTSETTINGS_H
