#ifndef ICTYPEREGISTRATOR_H
#define ICTYPEREGISTRATOR_H

#include <QObject>
#include <QDebug>

namespace ivk
{
    /** 
     * @brief Класс, регистрирующий мета-тип нужного класса в конструкторе.
     * @ingroup core
     */
    template<typename T> class ICTypeRegistrator
    {
        public:
            ICTypeRegistrator(const char* tname){qRegisterMetaType<T>(tname);}
    };

    /**
     * @brief Класс, регистрирующий мета-тип указателя на класс в конструкторе.
     * @ingroup core
     */
    template<typename T> class ICTypePtrRegistrator
    {
        public:
            ICTypePtrRegistrator(const char* tname){
                qRegisterMetaType<T*>(QString(tname).append("*").toUtf8().constData());
            }
    };

    /**
     * @brief Класс, регистрирующий операторы сериализации в поток нужного класса в конструкторе.
     * @ingroup core
     */
    template<typename T> class ICTypeStreamingRegistrator
    {
        public:
            ICTypeStreamingRegistrator(const char* tname){qRegisterMetaTypeStreamOperators<T>(tname);}
    };
}

#endif // ICTYPEREGISTRATOR_H
