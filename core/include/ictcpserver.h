#ifndef ICTCPSERVER_H
#define ICTCPSERVER_H

#include "icconnectionfactory.h"
#include <QTcpServer>
#include "icnetwork_global.h"

#define IC_DEFAULT_TCP_PORT 23231   ///< TCP-порт по-умолчанию

/**
 * @brief Класс-обертка QTcpServer, для обработки входящих подключений.
 *
 *  Класс используется исключительно как вспомогательный для ICTcpServer
 * с целью получения дескриптора входящего подключения.
 * @ingroup core
 */
class ICNETWORKSHARED_EXPORT ICTcpListener : public QTcpServer
{
    Q_OBJECT
public:
    ICTcpListener(QObject* parent=0) : QTcpServer(parent){}
signals:
    void accepted(qulonglong socketDescriptor);
private:
    void incomingConnection(qintptr socketDescriptor);
};


/**
  * @brief Сервер входящих TCP-подключений.
  * 
  * Открывает TCP-сокет для приема входящих подключений, обрабатывает их, создавая
  * экземпляры наследников ICConnection, настроенные в соответствии с конфигом.
  * @ingroup core
  */
class ICNETWORKSHARED_EXPORT ICTcpServer : public ICConnectionFactory
{
    IC_LOCAL_SETTINGS(secure)
    Q_OBJECT


public:
    ICTcpServer(QObject* parent =0);
    ~ICTcpServer(){}
    ICRETCODE init(const QString &prefix);

    Q_PROPERTY(bool secure READ secure WRITE setSecure)
    /**
     * @brief использовать безопасный протокол обмена?
     * @return true - да/ false - нет
     */
    inline bool secure() const {return m_secure;}
    /**
     * @brief установить режим безопасности
     * @param val если true - устанавливается безопасный протокол обмена, иначе - открытый
     */
    inline void setSecure(bool val){m_secure =val;}

    quint16 serverPort()const{return m_serverPort;}

public slots:
    void start();
    void stop();
private slots:
    void createConnection(qulonglong socketDescriptor);
private:
    ICTcpListener m_listener;
    bool m_secure = true;
    quint16 m_serverPort = 0;
};

#endif // ICTCPSERVER_H
