#ifndef ICSERVERNODESESSIONPRIVATE_H
#define ICSERVERNODESESSIONPRIVATE_H
#include "icdiscoverylinkprivate.h"
#include "QDebug"
namespace icrouting {

class ICServerLinkPrivate: public ICDiscoveryLinkPrivate
{
    Q_OBJECT
public:
    ICServerLinkPrivate(ICBaseLink* s);
    ICBaseLinkState* firstState();
    void resolvedAddressNotify(const ICNetworkDetails& addr);

signals:
    void addressResolved(const ICNetworkDetails& serverAddr, const ICNetworkDetails& tempAddr);

private slots:
    void doOnLinkLost(){emit aborted();}

};

class ICWaitingState: public ICBaseLinkState
{
public:
    ICWaitingState(ICBaseLinkPrivate*s): ICBaseLinkState(s){}
    void begin();
    void end(){qDebug()<<"end Waiting state";}
    void processMessage(const ICMessage& message);
private:
    ICServerLinkPrivate* mySession(){return static_cast<ICServerLinkPrivate*>(m_session);}
};

}

#endif // ICSERVERNODESESSIONPRIVATE_H
