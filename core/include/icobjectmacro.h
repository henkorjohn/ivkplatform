#ifndef ICOBJECTMACRO_H
#define ICOBJECTMACRO_H

#include <ictyperegistrator.h>
#include <icerrorreporter.h>

/*      Макросы, используемые в определениях классов-наследников ICObject
        Все макросы следует использовать в приватной секции хедера класса-наследника.
*/
//////////////////////////////////////////////////////////////////////////////////
/**
 * @brief Макрос, определяющий свойства, которые будут использоваться для доступа к локальным настройкам.
 * 
 * Для корректной работы необходимо, чтобы свойства с указанными именами были зарегистрированы макросом Q_PROPERTY(...)
 * @ingroup core
 * @example IC_LOCAL_SETTINGS(setting1, setting2)
 */
#define IC_LOCAL_SETTINGS(...)   _IC_DECL_SETTINGS(local,__VA_ARGS__)

/**
 * @brief Макрос, определяющий свойства, которые будут использоваться для доступа к глобальным настройкам.
 * 
 * Для корректной работы необходимо, чтобы свойства с указанными именами были зарегистрированы макросом Q_PROPERTY(...)
 * @ingroup core
 * @example IC_GLOBAL_SETTINGS(setting1,setting2)
 */
#define IC_GLOBAL_SETTINGS(...)  _IC_DECL_SETTINGS(global,__VA_ARGS__)

/**
 * @brief Макрос, регистрирующий мета-тип для класса.
 * 
 * Должен располагаться в cpp-файле регистрируемого класса после #include<...>.
 * @ingroup core
 */
#define IC_REGISTER_METATYPE(typeName) ivk::ICTypeRegistrator<typeName> typeName##_reg(#typeName);

/**
 * @brief Макрос, регистрирующий мета-тип для указателя на класс.
 *
 * Должен располагаться в cpp-файле регистрируемого класса после #include<...>.
 * @ingroup core
 */
#define IC_REGISTER_METATYPE_PTR(typeName) ivk::ICTypePtrRegistrator<typeName> typeName##_regPtr(#typeName);

/**
 * @brief Макрос, регистрирующий операторы сериализации в поток класса.
 * 
 * Должен располагаться в cpp-файле регистрируемого класса после #include<...>.
 * @ingroup core
 */
#define IC_REGISTER_METATYPE_STREAM_OPERATORS(typeName) ivk::ICTypeStreamingRegistrator<typeName> typeName##_regso(#typeName);

// --   internal macros  ---
#define _IC_DECL_SETTINGS(scope,...)  \
    QStringList m_##scope##Settings={QString(#__VA_ARGS__).remove(' ').split(',')}; \
    QStringList scope##Settings() const {return m_##scope##Settings;}

/**
 * @brief Макрос, сообщающий об ошибке
 */
#define IC_ERROR(message)   {ivk::ICErrorReporter report(message); Q_UNUSED(report);}

#endif // ICOBJECTMACRO_H
