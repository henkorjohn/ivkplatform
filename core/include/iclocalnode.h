#ifndef ICLOCALNODE_H
#define ICLOCALNODE_H
#include "icabstractnode.h"
#include "icmessages.h"
#include "icplugininterface.h"
#include "memory"
#include "icnetworkdetails.h"

using std::unique_ptr;
namespace icrouting {


class ICLocalNode: public ICAbstractNode
{
    Q_OBJECT
public:

    ICLocalNode(QObject* parent=nullptr);

    struct CollisionStatus
    {
        QStringList collidingNames;
        QList<QPair<QString, QString> > collidingHashes;
    };

    CollisionStatus checkNodeNameOrHashCollisions(QString nodeName)const;
    ICNodeInfo info()const;


    void addPlugins(const QList<ICPluginInterface*>&);

    void removePlugins(const QList<ICPluginInterface*>&);

    int size()const{return m_localPlugins.size();}


    //virtuals
    quint32 id() const{return m_id;}

    void setID(quint32 id){m_id = id;}

    QString name() const{return m_name;}

    void setName(QString name){m_name = name;}

    QList<quint32> pluginTypeIDs()const;

    QList<quint32> pluginTypeID(QString pluginName)const;

    QString pluginName(quint32 pluginTypeID)const;

    bool hasPlugin(quint32 pluginTypeID) const;


public slots:
    void deliverMessage(quint64 srcPluginID, quint64 destPluginID, const QByteArray& message)const;
signals:
    void broadcastMessage(const QByteArray& message);

private:
    quint32 m_id;
    QString m_name;
    QHash<quint32, ICPluginInterface*> m_localPlugins;
};

typedef unique_ptr<ICLocalNode> ICLocalNodeU;
}


#endif // ICLOCALNODE_H
