#ifndef ICERROR_H
#define ICERROR_H

#include <QString>
#include <QObject>
#include "icobject_global.h"

/**
 * @brief Класс, содержащий информацию об ошибке.
 * @ingroup core
 */
class ICOBJECTSHARED_EXPORT ICError
{
public:
    ICError(){}
    /**
     * @brief конструктор
     * @param errorMessage описание ошибки
     */
    ICError(const QString& message)
        :m_message(message) {}
    virtual ~ICError(){}
    /**
     * @brief сообщение об ошибке
     * @return возвращает текст сообщения об ошибке
     */
    virtual QString message() const{return m_message;}

protected:
    QString m_message;
};

#endif // ICERROR_H
