#ifndef ICHASH_H
#define ICHASH_H

#include <QtCore/qglobal.h>

#if defined(ICHASH_LIBRARY)
#  define ICHASHSHARED_EXPORT Q_DECL_EXPORT
#else
#  define ICHASHSHARED_EXPORT Q_DECL_IMPORT
#endif

#include <QString>

uint ICHASHSHARED_EXPORT icHash(const QString& str);

#endif /* ICHASH_H */
