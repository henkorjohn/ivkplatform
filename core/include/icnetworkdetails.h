#ifndef ICNETWORKDETAILS_H
#define ICNETWORKDETAILS_H
#include "qglobal.h"

namespace icrouting {
class ICNetworkDetails
{
public:
    ICNetworkDetails(): ip(0), port(0){}
public:
    bool valid()const{return ip != 0;}
    quint32 ip = 0;
    quint16 port = 0;
};

bool operator ==(const ICNetworkDetails& first,const ICNetworkDetails& second);

QDataStream& operator<<(QDataStream& stream, const ICNetworkDetails& details);
QDataStream& operator>>(QDataStream& stream, ICNetworkDetails& details);

}

uint qHash(const icrouting::ICNetworkDetails& d);

namespace boost {

std::size_t hash_value(const icrouting::ICNetworkDetails &addr);

}



#endif // ICNETWORKDETAILS_H
