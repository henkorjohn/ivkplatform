#ifndef ICNETWORKSTREAM_H
#define ICNETWORKSTREAM_H

#include <iciostream.h>
#include <QAbstractSocket>
#include "QHostAddress"

/**
 * @brief Класс, реализующий интерфейс обмена данными между узлами через Ethernet
 * @ingroup core
 */
class ICNetworkStream : public ICIOStream
{
    Q_OBJECT
public:
    ICNetworkStream(QObject* parent=0) : ICIOStream(parent) { m_port = 0;}
    ~ICNetworkStream(){}
    /**
     * @brief Возвращает указатель на класс, через который реализуется доступ к сокету.
     * 
     * Должен быть переопределен в потомках.
     * @return возвращает экземпляр класса-наследника QAbstractSocket
     */
    virtual QAbstractSocket* socket()const =0;
    void setConnectionAttribute(QVariant connectionAttribute);
    bool open(bool outgoing);
    void close();
    /**
     * @brief Доменное имя или IP удаленного узла
     */
    inline QString hostname() const {return m_hostname;}
    /**
     * @brief TCP-порт удаленного узла
     */
    inline quint16 port() const{return m_port;}

    QString peerPortAddress()const;

    quint32 localAddress()const{return socket()->localAddress().toIPv4Address();}

private:
    QIODevice* device(){return socket();}

    QString m_hostname;
    quint16 m_port;
};

#endif // ICNETWORKSTREAM_H
