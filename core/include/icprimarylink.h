#ifndef ICPRIMARYLINK_H
#define ICPRIMARYLINK_H
#include "icdiscoverylink.h"
#include "icprimarylinkprivate.h"

namespace icrouting {

class ICPrimaryLink: public ICDiscoveryLink
{
    Q_OBJECT
public:
    ICPrimaryLink();
};


}
#endif // ICPRIMARYLINK_H
