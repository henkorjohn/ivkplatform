#ifndef ICLINKSTATE_H
#define ICLINKSTATE_H
#include "icobject.h"
#include "icmessages.h"


namespace icrouting {

class ICDiscoveryLinkPrivate;
class ICServerLinkPrivate;
class ICBaseLinkPrivate;

class ICBaseLinkState
{
public:
    ICBaseLinkState(ICBaseLinkPrivate*s){Q_CHECK_PTR(s);m_session = s;}
    virtual ~ICBaseLinkState(){}
    virtual void begin() = 0;
    virtual void end() = 0;
    virtual void processMessage(const ICMessage& message) = 0;
    void initialize();
    void teardown();
protected:
    ICBaseLinkPrivate*m_session= nullptr;

};


class ICLinkValidationState: public ICBaseLinkState
{
public:
    ICLinkValidationState(ICBaseLinkPrivate*s): ICBaseLinkState(s){}
    void begin();
    void end();
    void processMessage(const ICMessage& message);
    virtual ICMessage createMessage() = 0;
    virtual ICBaseLinkState*nextState() = 0;
};

class ICPrimaryClientLinkVerificationState: public ICLinkValidationState
{
public:
    ICPrimaryClientLinkVerificationState(ICBaseLinkPrivate*s): ICLinkValidationState(s){}
    ICMessage createMessage();
    ICBaseLinkState* nextState();
};

class ICSimpleClientLinkVerificationState: public ICLinkValidationState
{
public:
    ICSimpleClientLinkVerificationState(ICBaseLinkPrivate*s): ICLinkValidationState(s){}
    ICMessage createMessage();
    ICBaseLinkState* nextState();
};


class ICDiscoveryState: public ICBaseLinkState
{
public:
    ICDiscoveryState(ICBaseLinkPrivate*s): ICBaseLinkState(s){}
protected:
    void sendDiscoveryInfo();
protected:
    ICDiscoveryLinkPrivate* mySession();
};

class ICDiscoveryStateServer: public ICDiscoveryState
{
public:
    ICDiscoveryStateServer(ICBaseLinkPrivate*s): ICDiscoveryState(s){}
    void begin();
    void end();
    void processMessage(const ICMessage &message);
};


class ICDiscoveryStateClient: public ICDiscoveryState
{
public:
    ICDiscoveryStateClient(ICBaseLinkPrivate*s): ICDiscoveryState(s){}
    void begin();
    void end();
    void processMessage(const ICMessage& message);
};


class ICBaseInfoExchangeState: public ICBaseLinkState
{
public:
    ICBaseInfoExchangeState(ICBaseLinkPrivate*s): ICBaseLinkState(s){}
protected:
    void parseNodeInfo(const ICMessage& message);
    void sendNodeInfo();
};

class ICInfoExchangeStateClient: public ICBaseInfoExchangeState
{
public:
    ICInfoExchangeStateClient(ICBaseLinkPrivate*s): ICBaseInfoExchangeState(s){}
    void begin();
    void end();
    void processMessage(const ICMessage& message);
};

class ICInfoExchangeStateServer: public ICBaseInfoExchangeState
{
public:
    ICInfoExchangeStateServer(ICBaseLinkPrivate*s): ICBaseInfoExchangeState(s){}
    void begin();
    void end();
    void processMessage(const ICMessage& message);
};

class ICNormalState: public ICBaseLinkState
{
public:
    ICNormalState(ICBaseLinkPrivate*s): ICBaseLinkState(s){}
    void begin();
    void end();
    void processMessage(const ICMessage& message);
private:
    void parseServiceMessage(const ICMessage &message);
    QStringList parsePluginList(const ICMessage &message);
    void parseAddedPlugins(const ICMessage &message);
    void parseRemovedPlugins(const ICMessage &message);
};

}


#endif // ICLINKSTATE_H
