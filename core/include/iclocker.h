#ifndef ICLOCKER_H
#define ICLOCKER_H
#include "QReadWriteLock"
#include "QScopedPointer"
namespace icrouting {

class ICLocker
{
   public: ICLocker();
   static QReadWriteLock* routeTableLock();
private:
   static QScopedPointer<QReadWriteLock> m_routeTableLock;
};

}
#endif // ICLOCKER_H
