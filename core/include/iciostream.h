#ifndef ICIOSTREAM_H
#define ICIOSTREAM_H

#include <icdatastream.h>
#include <QIODevice>
#include <QTimer>

/**
 * @brief Реализация интерфейса обмена данными с использованием класса-наследника QIODevice.
 * @ingroup core
 */
class ICIOStream : public ICDataStream
{
    Q_OBJECT
public:
    explicit ICIOStream(QObject *parent=0);
    virtual ~ICIOStream(){}
    void initialize();
    quint64 write(const QByteArray &data);
    QByteArray read(quint64 bytesToRead = 0);
    bool open(bool outgoing);
protected:
    /**
     * @brief Возвращает указатель на класс, через который реализуется доступ к каналу передачи данных.
     * 
     * Должен быть переопределен в потомках.
     * @return возвращает экземпляр класса-наследника QIODevice
     */
    virtual QIODevice* device() = 0;
private slots:
    void onConnectTimeout();
private:
    QTimer m_connectTimer;
    static const int g_defaultConnectTimeout = 1000;
};

#endif // ICIOSTREAM_H
