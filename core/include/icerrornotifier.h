#ifndef ICERRORPROVIDER_H
#define ICERRORPROVIDER_H

#include <icerror.h>
#include <icobject_global.h>
/**
 * @brief Класс, описывающий интерфейс классов, поддерживающих сообщения об ошибке.
 *
 * Позволяет классам-наследникам сообщать об ошибках, которые обрабатываются централизованно.
 * @ingroup core
 */
class ICOBJECTSHARED_EXPORT ICErrorNotifier : public QObject
{
    Q_OBJECT
public:
    ICErrorNotifier(QObject *parent = 0);
    virtual ~ICErrorNotifier(){}
    inline QString lastError() const {return m_lastErrorText;}

protected:
    /**
     * @brief Сообщить об ошибке
     * @param message текст сообщения
     */
    void reportError(const QString& message){m_lastErrorText=message; emit error(message);}
    void reportError(const char* msg){reportError(trUtf8(msg));}
#define REPORT_ERROR_IN_LINE(msg) reportError(QString("%1 (%2:%3)").arg(msg).arg(__FILE__).arg(__LINE__))
    void issueWarning(const QString& message){reportError(message);}   //TODO
signals:
    /**
     * @brief Сигнал, сообщающий об ошибке
     * @param err информация об ошибке
     */
    void error(ICError err);
private:
    QString m_lastErrorText;
};

#endif // ICERRORPROVIDER_H
