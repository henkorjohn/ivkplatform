#include <QtGlobal>
/**
 * @file icversion.h
 * @brief структура версии программного модуля
 * @copyright ivk-center.ru
 */

/**
 * @brief Структура версии программного модуля.
 *
 * Принимается, что программный модуль (платформа или плагин) не должен ломать совместимости в рамках минорной версии. 
 * @ingroup core
 */
typedef struct _ICVersion
{
    quint16 major;
    quint16 minor;
    quint32 build;
} ICVersion;
