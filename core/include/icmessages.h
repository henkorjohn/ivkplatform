#ifndef ICMESSAGES_H
#define ICMESSAGES_H
#include "QStringList"
#include "QString"
#include "QObject"
#include "icnetworkdetails.h"
#include "vector"

namespace icrouting {

using std::vector;

/**
 * @brief Структура верхнего уровня, содержащая различные виды сообщений, которые пакуются в payload
 * @ingroup core
 */
class ICMessage
{
 public:
    ICMessage();
    ICMessage(const QByteArray& data);

    static ICMessage primaryLinkVerificationMessage();
    static ICMessage simpleLinkVerificationMessage();
    static ICMessage discoveryMessage();
    static ICMessage infoExchangeMessage();
    static ICMessage pluginsInMessage();
    static ICMessage pluginsOutMessage();
    static ICMessage normalMessage();

    void setData(const QByteArray& data);
    QByteArray data()const{return m_payload;}
    enum Type
    {
        PrimaryLinkValidation,
        SimpleLinkValidation,
        Discovery,///< сообщение для обнаружения узлов сети
        InfoExchange,///< startup info exchange message
        PluginsIn,///< подключены новые плагины
        PluginsOut,///< отключены плагины
        Normal///< обычное сообщение плагина
    };
//    operator QByteArray();
    QByteArray toByteArray()const;
    Type m_type;
    QByteArray m_payload;
};


/**
 * @brief Служебная структура, содержащая описание узла
 * @ingroup core
 */
class ICNodeInfo
{
public:
    ICNodeInfo();
    ICNodeInfo(const QByteArray& info);
    QByteArray toByteArray()const;
    QString m_nodeName;
    QStringList m_pluginNames;
};

class ICLinkVerificationInfo
{
public:
    ICLinkVerificationInfo();
    ICLinkVerificationInfo(const QByteArray& data);
    QByteArray toByteArray()const;
    QString name;
    quint16 port;
};

class ICLinkStatus
{
   public:
    ICLinkStatus(){}
    ICLinkStatus(const QByteArray& data);
    QByteArray toByteArray()const;
    bool valid = false;
};

QDataStream& operator<<(QDataStream& stream, const ICNodeInfo& info);
QDataStream& operator>>(QDataStream& stream, ICNodeInfo& info);

class ICDiscoveryInfo
{
public:
    ICDiscoveryInfo();
    ICDiscoveryInfo(const QByteArray& info);
    QByteArray toByteArray()const;
    vector<ICNetworkDetails> m_addresses;
};
 /**
  * @brief Структура сообщений, курсирующих между плагинами.
  * @ingroup core
  */
class ICPluginMessage
{
public:
    ICPluginMessage(){}
    ICPluginMessage(const QByteArray& data);
    QByteArray toByteArray()const;
    quint64 source;
    quint64 target;
    QByteArray payload;
};

QDataStream& operator<<(QDataStream& stream, const ICMessage& message);
QDataStream& operator>>(QDataStream& stream, ICMessage& message);

}



#endif // ICMESSAGES_H
