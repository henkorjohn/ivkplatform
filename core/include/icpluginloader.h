/**
 * @file icpluginloader.h
 * @brief Описание загрузчика плагинов.
 * Загрузчик плагинов работает как фабрика. Он получает имя плагина для загрузки, читает его настройки из файла, загружает его и вызывает конструктор с нужными настройками. 
 * Кроме того, он же устанавливает контекст плагина.
 * @copyright ivk-center.ru
 */

#ifndef ICPLUGINLOADER_H
#define ICPLUGINLOADER_H

#include <QObject>
#include <QString>
#include <QStringList>
#include "icplugininterface.h"
#include <QPluginLoader>
#include <QMap>

/**
 * @brief Класс-загрузчик плагинов.
 *
 * Читает настройки из файла конфигурации, устанавливает контекст плагина, загружает плагин и создает объект.
 * @ingroup core
 */
class ICPluginLoader : public QObject
{
    Q_OBJECT
public:
    ICPluginLoader(){}
    ~ICPluginLoader();
    ICPluginInterface* loadPlugin(const QString& plname);            ///< Загружает плагин с определенным именем.
    bool unloadPlugin(ICPluginInterface* pl);                        ///< Выгружает плагин из памяти.
    static QStringList pluginPathsForLoading(const QString& group);  ///< Возвращает список путей плагинов, которые нужно загрузить.
    static QStringList pluginsForLoading(const QString& group);      ///< Возвращает список плагинов, которые нужно загрузить.
    QList<ICPluginInterface*> loadedPlugins();                       ///< Возвращает список загруженных плагинов
private:
    QMap<ICPluginInterface*, QPluginLoader*> m_plugins;
};

#endif //ICPLUGINLOADER_H
