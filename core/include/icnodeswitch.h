#ifndef ICROUTINGCENTER_H
#define ICROUTINGCENTER_H

#include "icobject.h"
#include "icmessages.h"
#include "icplugintype.h"
#include "QSemaphore"
#include "icabstractnode.h"


namespace icrouting {

class ICNodeSwitch: public ICObject
{
    Q_OBJECT
public:

    ICNodeSwitch(QObject* parent=nullptr);
    //функции обслуживания локальных запросов плагинов


    QList<quint64> pluginIDs(QString pluginName,quint64 senderID)const; ///< Получает идентификаторы плагинов с определенным именем со всех нодов.
    QList<quint64> pluginIDs(quint64 senderID)const;                   ///< Получает идентификаторы всех плагинов со всех нодов.
    QString nodeName(quint64 pluginID)const;           ///< Получает имя нода по идентификатору плагина.
    QString pluginName(quint64 pluginID)const;         ///< Получает имя плагина по идентификатору плагина.

    bool isRoutable(quint64 dest)const;

public slots:

    void addNode(const ICAbstractNode* node);

    void removeNode(const ICAbstractNode* node);

    void route(quint64 src, quint64 dest, const QByteArray& message)const;


private:

    QHash<quint32, const ICAbstractNode*> m_routeTable;
};

}

#endif // ICROUTINGCENTER_H
