/**
 * @file icsettings.h
 * @brief Чисто виртуальный класс для хранения настроек.
 * @copyright ivk-center.ru
 */

#ifndef ICSETTINGS_H
#define ICSETTINGS_H

/**
 * @defgroup settings Доступ к настройкам
 */

#include <QVariant>
#include <QStringList>
#include <icsettings_global.h>
#include <QObject>

/**
 * @brief Базовый класс настроек.
 * @ingroup settings core
 */
class ICSETTINGSSHARED_EXPORT ICSettings : public QObject
{
    Q_OBJECT
public:
    ICSettings(){}
    ICSettings(const QString& organization, const QString& application){Q_UNUSED(organization); Q_UNUSED(application)}
    virtual ~ICSettings(){}
    /**
     * @brief Асинхронный запрос настройки
     * @param name имя настройки
     * @return значение настройки
     */
    virtual QVariant queryValue(const QString& name){return value(name,QVariant());}
    /**
     * @brief Синхронный запрос настройки
     * @param name имя настройки
     * @param defaultValue значение по-умолчанию
     * @return текущее значение
     */
    virtual QVariant value(const QString& name, const QVariant& defaultValue = QVariant()) const =0;
    /**
     * @brief Запись настройки
     * @param key имя
     * @param value значение
     */
    virtual void setValue(const QString& key, const QVariant& value)=0;
    virtual void beginGroup(const QString prefix){Q_UNUSED(prefix);}
    virtual void endGroup(){}
    virtual QStringList childGroups() const{return QStringList();}
    virtual QStringList childKeys() const{return allKeys();}
    virtual QStringList allKeys() const=0;
    virtual void remove(const QString& key)=0;
    virtual void clear()=0;
signals:
    void valueChanged(QString settingName,QVariant value);
};

#endif
