#ifndef ICQMLTYPEREGISTRATOR_H
#define ICQMLTYPEREGISTRATOR_H

#include <QObject>
#include <QtQml>

/** @brief Класс, регистрирующий QML-тип нужного класса в конструкторе.
 * @ingroup core gui
 */
template<typename T> class ICQmlTypeRegistrator
{
public:
    ICQmlTypeRegistrator(const char* uri,int verMajor,int verMinor,const char* tname){qmlRegisterType<T>(uri,verMajor,verMinor,tname);}
};

#endif // ICQMLTYPEREGISTRATOR_H
