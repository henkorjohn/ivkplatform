#ifndef ICGUIMACRO_H
#define ICGUIMACRO_H

#include <icqmltyperegistrator.h>

/*      Макросы, используемые в классах GUI-подсистемы.
*/
//////////////////////////////////////////////////////////////////////////////////
/**
 * @brief Макрос, регистрирующий мета-тип для класса.
 * Должен распологаться в cpp-файле регистрируемого класса после #include<...>.
 * @ingroup core
 */
#define IC_QML_REGISTER_TYPE(uri,ver,subver,typeName) ICQmlTypeRegistrator<typeName> typeName##_qmlreg(uri,ver,subver,#typeName);

#endif // ICGUIMACRO_H
