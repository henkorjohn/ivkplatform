#ifndef ICSETTINGSFILTERMODEL_H
#define ICSETTINGSFILTERMODEL_H

#include <icsettingsmodel_global.h>
#include <QSortFilterProxyModel>
#include <icsettingsmodel.h>

/**
 * @brief Модель фильтрации настроек.
 * @ingroup dataaccess settings
 */
class ICSETTINGSMODELSHARED_EXPORT ICSettingsFilterModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    ICSettingsFilterModel();
    virtual ~ICSettingsFilterModel();

    //properties
    Q_PROPERTY(bool advanced READ advanced WRITE setAdvanced NOTIFY advancedChanged) ///< Показывать дополнительные насройки
    /**
    * @brief Возвращает значение свойства "Показывать дополнительные насройки"
    */
    inline bool advanced() const{return m_advanced;}
    /**
    * @brief Устанавливает значение свойства "Показывать дополнительные насройки"
    * @param val новое значение
    */
    inline void setAdvanced(bool val){if(m_advanced==val) return; m_advanced=val; emit advancedChanged();}

    Q_PROPERTY(QString filter READ filter WRITE setFilter NOTIFY filterChanged) ///< Фильтр по имени настройки или категории
    /**
    * @brief Возвращает значение свойства "Фильтр по имени настройки или категории"
    */
    inline QString filter() const{return m_filter.pattern();}
    /**
    * @brief Устанавливает значение свойства "Фильтр по имени настройки или категории"
    * @param val новое значение
    */
    inline void setFilter(QString val){if(m_filter.pattern()==val) return; m_filter.setPattern(val); emit filterChanged();}


    //methods:
    /**
     * @brief Загрузить модель-источник
     */
    void load();
    /**
     * @brief Применить настройки
     */
    inline void apply(){if(m_model!=nullptr) m_model->apply();}
    /**
     * @brief Сбросить настройки
     */
    inline void reset(){if(m_model!=nullptr) m_model->reset();}
protected:
    //overriden methods
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;

signals:
    void filterChanged();   ///< Сообщает об изменении значения свойства "Фильтр по имени настройки или категории"
    void advancedChanged();   ///< Сообщает об изменении значения свойства "Показывать дополнительные насройки"
private slots:
    void applyFilter();
    //data members:
private:
    QRegExp m_filter;
    bool m_advanced;
    ICSettingsModel* m_model=nullptr;
};


#endif // ICSETTINGSFILTERMODEL_H
