#include <icsettingsfiltermodel.h>
#include <iclocalsettings.h>
#include <icobjectmacro.h>
#include <icsettingsnode.h>

ICSettingsFilterModel::ICSettingsFilterModel()
{
    m_filter.setCaseSensitivity(Qt::CaseInsensitive);
    // подписать к нотифаерам атрибутов фильтрации слот обновления модели
    connect(this,&ICSettingsFilterModel::filterChanged,this,&ICSettingsFilterModel::applyFilter);
    connect(this,&ICSettingsFilterModel::advancedChanged,this,&ICSettingsFilterModel::applyFilter);
}

ICSettingsFilterModel::~ICSettingsFilterModel()
{
    if(m_model!=nullptr)
        delete m_model;
}

void ICSettingsFilterModel::load()
{
    // прочитать из настроек имя класса модели-источника
    ICLocalSettings sett;
    auto className = sett.value("ui/settingsModel","DefaultSettingsModel").toString();
    // если модель источник была ранее определена
    if(m_model==nullptr || m_model->metaObject()->className()!=className)  //если имя изменилось
    {
        auto typeId = QMetaType::type(className.toUtf8().constData());
        Q_ASSERT(typeId!=QMetaType::UnknownType);
        if(typeId==QMetaType::UnknownType)
        {
//            IC_REPORT_ERROR(trUtf8("Класс модели настроек с именем %1 не существует").arg(className));
            return;
        }
        //убить старую модель
        if(m_model!=nullptr)
            m_model->deleteLater();
        //создать новую модель
        m_model = static_cast<ICSettingsModel*>(QMetaType::create(typeId));
        setSourceModel(m_model);
    }
    //вызвать метод загрузки модели-источника
    m_model->load();
}

bool ICSettingsFilterModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    Q_ASSERT(m_model!=nullptr);
    auto index = m_model->index(source_row,0,source_parent);
    if(!index.isValid())
        return false;
    auto nd = (ICTreeNode*)index.internalPointer();
    auto setnode = dynamic_cast<ICSettingsNode*>(nd);
    if(setnode==nullptr)
        return false;
    // проверить условия фильтров
    return setnode->match(m_filter,m_advanced);
}

void ICSettingsFilterModel::applyFilter()
{
    invalidateFilter();
}
