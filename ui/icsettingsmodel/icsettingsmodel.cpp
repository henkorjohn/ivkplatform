#include "icsettingsmodel.h"
#include <icsettingsnode.h>

ICSettingsModel::ICSettingsModel(QObject* parent)
    :ICTreeModel(parent)
{
}

QVariant ICSettingsModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();
    auto nd = (ICTreeNode*)index.internalPointer();
    auto node = dynamic_cast<ICSettingsNode*>(nd);
    if(node!=nullptr)
    {
        switch (role)
        {
        case TypeRole:
            return node->valueType();
        case ValueRole:
            return node->value();
        default:
            break;
        }
    }
    return ICTreeModel::data(index,role);
}

bool ICSettingsModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!index.isValid() || role!=ValueRole)
        return false;
    auto node = (ICTreeNode*)index.internalPointer();
    auto setNode = dynamic_cast<ICSettingsNode*>(node);
    if(setNode==nullptr)
        return false;
    setNode->setValue(value);
    return true;
}

QHash<int, QByteArray> ICSettingsModel::roleNames() const
{
    auto res = ICTreeModel::roleNames();
    res[TypeRole] = "valueType";
    res[ValueRole] = "value";
    return res;
}

Qt::ItemFlags ICSettingsModel::flags(const QModelIndex &index) const
{
    auto res = QAbstractItemModel::flags(index);
    if(!index.isValid())
        return res;
    auto node = (ICSettingsNode*)index.internalPointer();
    if(node->valueType()==ICSettingsNode::NoType)
        return res;
    return res | Qt::ItemIsEditable;
}

void ICSettingsModel::apply()
{
    for(auto nd: m_nodes)
    {
        auto set = dynamic_cast<ICSettingsNode*>(nd);
        if(set!=nullptr)
            set->apply();
    }
}

void ICSettingsModel::reset()
{
    for(auto nd: m_nodes)
    {
        auto set = dynamic_cast<ICSettingsNode*>(nd);
        if(set!=nullptr)
            set->reset();
    }
}

void ICSettingsModel::updateValue()
{
    auto nd = dynamic_cast<ICSettingsNode*>(sender());
    if(nd==nullptr)
        return;
    QModelIndex index = createIndex(nd->row(),0,nd);
    emit dataChanged(index,index,{ValueRole,DataRole});
}
