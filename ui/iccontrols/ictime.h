#ifndef ICTIME_H
#define ICTIME_H

#include <icobject.h>
#include <QDateTime>

/**
 * @brief Класс, реалищуюший QML-интерфейс для форматирования даты и времени
 */
class ICTime : public ICObject
{
    Q_OBJECT
public:
    explicit ICTime(QObject *parent = 0);

    Q_PROPERTY(qint64 time READ time WRITE setTime NOTIFY timeChanged) ///< время
    /**
    * @brief Возвращает время
    */
    inline qint64 time() const{return m_time;}
    /**
    * @brief Устанавливает время
    * @param val новое значение
    */
    inline void setTime(qint64 val){if(m_time==val) return; m_time=val; emit timeChanged();}

    Q_PROPERTY(QString format READ format WRITE setFormat NOTIFY formatChanged) ///< формат
    /**
    * @brief Возвращает формат
    */
    inline QString format() const{return m_format;}
    /**
    * @brief Устанавливает формат
    * @param val новое значение
    */
    inline void setFormat(QString val){if(m_format==val) return; m_format=val; emit formatChanged();}

    Q_PROPERTY(QString text READ text NOTIFY textChanged) ///< строковое представление даты/времени
    /**
    * @brief Возвращает строковое представление даты/времени
    */
    inline QString text() const{return QDateTime::fromMSecsSinceEpoch(time()).toString(format());}
signals:
    void timeChanged();   ///< Сообщает об изменении значения свойства "время"
    void formatChanged();   ///< Сообщает об изменении значения свойства "формат"
    void textChanged();   ///< Сообщает об изменении значения свойства "строковое представление даты/времени"
private:
    qint64 m_time;
    QString m_format="HH:mm:ss";
    QString m_text;
};

#endif // ICTIME_H
