#include "screenkeyboardattachedtype.h"

ScreenKeyboardAttachedType::ScreenKeyboardAttachedType(QObject *parent)
    : QObject(parent)
{
    m_input = 0;
    m_output = 0;
}


void ScreenKeyboardAttachedType::setInput(ScreenKbInput *input)
{
    if(m_input==input)
        return;
    if(!input && m_output)
        m_input->disconnect(m_output);
    m_input = input;
    bind();
}

void ScreenKeyboardAttachedType::setOutput(ScreenKbOutput *output)
{
    if(m_output==output)
        return;
    if(m_output!=output && m_output && m_input)
        m_input->disconnect(m_output);
    m_output = output;
    bind();
}

void ScreenKeyboardAttachedType::bind()
{
    m_input->setVisible(m_output);
    if(!(m_input && m_output))
        return;
    connect(m_input,SIGNAL(charAppended(QString)),m_output,SIGNAL(charAppended(QString)),Qt::UniqueConnection);
    connect(m_input,SIGNAL(charRemoved()),m_output,SIGNAL(charRemoved()),Qt::UniqueConnection);
    connect(m_output,SIGNAL(defaultLayoutChanged(int)),m_input,SLOT(setDefaultLayout(int)),Qt::UniqueConnection);
    connect(m_input,SIGNAL(accepted()),m_output,SIGNAL(accepted()),Qt::UniqueConnection);
    m_input->setDefaultLayout(m_output->defaultLayout());
}
