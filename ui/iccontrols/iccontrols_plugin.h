#ifndef ICCONTROLS_PLUGIN_H
#define ICCONTROLS_PLUGIN_H

#include <QQmlExtensionPlugin>

class ICControlsPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char *uri);
};

#endif // ICCONTROLS_PLUGIN_H

