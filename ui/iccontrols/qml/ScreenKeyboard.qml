import QtQuick 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0
import ivk.controls 1.0

Rectangle {
    id: kb
    property int buttonSize: 60
    property int layout : 0
    signal keyPressed(string key)
    signal removed()
    signal accepted()

    color: "lightgrey"
    border.width: 1
    border.color: "#404040"
    implicitHeight: kbRow.implicitHeight+20
    implicitWidth: kbRow.implicitWidth+20
    ScreenKeyboardModel
    {
        id: kbModel
    }
    RowLayout
    {
        id: kbRow
        anchors.fill: parent
        anchors.margins: 10
//        anchors.bottomMargin: 40
//        anchors.leftMargin: 40
        spacing: 10
        Column {
            Layout.preferredWidth: 2*kb.buttonSize
            Layout.fillHeight: true
            spacing: 90
            UnscaledButton {
                id: btnLoc
                property int locale : 0
                property bool alternate : false
                text: "EN"
                iconSource: "../images/en.png"
                height: kb.buttonSize
                width: 2*kb.buttonSize

                states: State{
                    name: "ru"
                    PropertyChanges {
                        target: btnLoc
                        text: "RU"
                        iconSource: "../images/ru.png"
                        locale: 2
                    }
                    when: btnLoc.alternate
                }
                onClicked: alternate = !alternate
                onLocaleChanged: {kb.layout &= ~2;kb.layout|=locale}
            }
            UnscaledButton{
                id: btnCase
                checkable: true
                height: kb.buttonSize
                width: 2*kb.buttonSize
                iconSource: "../images/up.png"
                text: checked ? "A" : "a"
                onCheckedChanged: {if(checked) kb.layout |= 1; else kb.layout &= ~1}
            }
        }
        Column {
            id: chars
//            Layout.preferredWidth: 80
            Layout.fillHeight: true
            property int charset : 0
            signal charsetSelected(int chs)
            spacing: 15
            Layout.alignment: Qt.AlignHCenter|Qt.AlignVCenter
            Repeater{
                model: ["letters","numbers","symbols"]
                Item {
                    id: csBtn
                    property bool isSelected : chars.charset == index
                    width: buttonSize
                    height: buttonSize
                    anchors.horizontalCenter: parent.horizontalCenter
                    scale: isSelected ? 1.0 : 0.6
                    Rectangle{
                        id: csRect
                        anchors.fill: parent
                        radius: width/2
                        property color baseColor: csBtn.isSelected ? "#f0f0f0": "transparent"
                        gradient: Gradient {
                            GradientStop{position: 0;color:csRect.baseColor}
                            GradientStop{position: 1.0;color:Qt.darker(csRect.baseColor,1.2)}
                        }
                    }
                    Image{
                        id: bsLabel
                        anchors.centerIn: parent
                        source: "../images/"+modelData+".png"
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: { chars.charset = index; chars.charsetSelected(index)}
                    }
                }
            }
            onCharsetSelected: {
                switch(chs)
                {
                case 0:
                    kb.layout = btnLoc.locale|(btnCase.checked ? 1 : 0)
                    break;
                case 1:
                    kb.layout = 4
                    break;
                case 2:
                    kb.layout = 5
                    break;
                default:
                    break;
                }
            }
        }
        Grid {
            spacing: 10
            columns: 11
            Layout.fillHeight: true
            Layout.fillWidth: true
//            Layout.alignment: Qt.AlignVCenter|Qt.AlignHCenter
            Repeater {
                model: kbModel.layout
                Button {
                    width: kb.buttonSize
                    height: kb.buttonSize
                    text: modelData
                    onClicked: keyPressed(text)
                }
            }
        }
        Column {
            Layout.preferredWidth: 2*kb.buttonSize
            Layout.fillHeight: true
            spacing: 10
            UnscaledButton{
                height: kb.buttonSize
                width: 2*kb.buttonSize
                iconSource: "../images/backspace.png"
                onClicked: kb.removed()
            }
            UnscaledButton{
                height: kb.buttonSize
                width: 2*kb.buttonSize
                iconSource: "../images/space.png"
                onClicked: kb.keyPressed(" ")
            }
            UnscaledButton{
                height: kb.buttonSize
                width: 2*kb.buttonSize
                iconSource: "../images/enter.png"
                onClicked: {kb.accepted()}
            }
        }
    }
    onLayoutChanged: {
        kbModel.switchLayout(kb.layout)
        if(kb.layout<4)
            chars.charset = 0
        else if(kb.layout==4)
            chars.charset=1
        else
            chars.charset=2
    }
}
