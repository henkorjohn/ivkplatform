#include "iccontrols_plugin.h"
#include <qqml.h>
#include "icplot.h"
#include "screenkbinput.h"
#include "screenkboutput.h"
#include "screenkeyboardmodel.h"
#include "icmodelstatistics.h"
#include "ictime.h"

void ICControlsPlugin::registerTypes(const char *uri)
{
    // @uri ivk.controls
    qmlRegisterType<ICPlot>(uri, 1, 0, "ICPlot");
    qmlRegisterType<ScreenKbInput>(uri, 1, 0, "ScreenKbInput");
    qmlRegisterType<ScreenKbOutput>(uri, 1, 0, "ScreenKbOutput");
    qmlRegisterType<ScreenKeyboardModel>(uri,1,0,"ScreenKeyboardModel");
    qmlRegisterType<ICModelStatistics>(uri,1,0,"ICModelStatistics");
    qmlRegisterType<ICTime>(uri,1,0,"ICTime");
}


