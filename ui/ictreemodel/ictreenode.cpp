#include "ictreenode.h"
#include "ictreemodel.h"

ICTreeNode::ICTreeNode(QObject *model, const QString &description)
    : m_description(description)
    , m_model(dynamic_cast<ICTreeModel*>(model))
{

}

void ICTreeNode::append(ICTreeNode *node)
{
    m_subnodes<<node;
    node->setParentNode(this);
    //    emit hasChildrenChanged();
}

int ICTreeNode::row()
{
    if (parentNode()==nullptr)
    {
        if (m_model==nullptr)
            return 0;
        else
            return m_model->row(this);
    }
    else
        return parentNode()->subnodes().indexOf(this);
}

void ICTreeNode::setParentNode(ICTreeNode *parent)
{
    if (parent == nullptr)
        setIsRoot(true);
    m_parent = parent;
}
