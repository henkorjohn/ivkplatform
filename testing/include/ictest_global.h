#ifndef ICTEST_GLOBAL_H
#define ICTEST_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(ICTEST_LIBRARY)
#  define ICTESTSHARED_EXPORT Q_DECL_EXPORT
#else
#  define ICTESTSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // ICTEST_GLOBAL_H
