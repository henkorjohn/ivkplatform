#include <icrecordset.h>
#include <icvaluemap.h>
#include <icbinaryrecord.h>
#include <icmetaobjectrecord.h>

ICRecordSet::~ICRecordSet()
{
    if(m_needDestroyRecords)
        qDeleteAll(records);
}

void ICRecordSet::serialize(QDataStream &output) const
{
    int cnt = records.count();
    output << cnt;
    if(cnt==0)
        return;
    for(ICRecord* r: records)
	ICRecord::serialize(output, r);
}

void ICRecordSet::deserialize(QDataStream &input)
{
    int cnt = 0;
    input >> cnt;
    if(cnt==0)
        return;
    for(int i=0; i<cnt; i++)
    {
        Q_ASSERT(!input.atEnd());
        records.append(ICRecord::deserialize(input));
    }
}

ICRecordSet& ICRecordSet::operator=(const ICRecordSet& rs)
{
    if(m_needDestroyRecords)
        qDeleteAll(records);
    m_needDestroyRecords = rs.m_needDestroyRecords;
    records = rs.records;
    return *this;
}

ICRecordSet& ICRecordSet::deepCopy(const ICRecordSet& rs)
{
    if(m_needDestroyRecords)
        qDeleteAll(records);
    records.clear();
    m_needDestroyRecords = rs.m_needDestroyRecords;
    for(auto rec : rs.records)
    {
        ICRecord* tmp = rec->makeCopy();
        records.append(tmp);
    }
    return *this;
}

QDataStream& operator <<(QDataStream& output,const ICRecordSet& rs)
{
    rs.serialize(output);
    return output;
}

QDataStream& operator >>(QDataStream& input,ICRecordSet& rs)
{
    rs.deserialize(input);
    return input;
}
