#include <icmetaobjectrecord.h>
#include <QMetaProperty>

IC_REGISTER_ICRECORDTYPE(ICMetaObjectRecord)

ICMetaObjectRecord::ICMetaObjectRecord(QObject *target)
    : ICRecord(),
      m_target(target),
      m_toFreeTarget(false)
{
    if(!target)
    {
        m_target = new QObject();
        m_toFreeTarget = true;
    }
}

ICMetaObjectRecord::~ICMetaObjectRecord()
{
    if(m_toFreeTarget)
        delete m_target;
}

QStringList ICMetaObjectRecord::names() const
{
    const QMetaObject *metaObj = m_target->metaObject();
    int propCnt = metaObj->propertyCount();
    QStringList result;
    for(int i=0;i<propCnt;i++)
    {
        auto nm = metaObj->property(i).name();
        if(nm==QString("objectName"))
            continue;
        result << nm;
    }
    auto dynProps = m_target->dynamicPropertyNames();
    for(auto prop:dynProps)
        result<<prop;
    return result;
}

QVariant ICMetaObjectRecord::value(const QString &name) const
{
    return m_target->property(name.toUtf8().constData());
}

void ICMetaObjectRecord::setValue(const QString &name, const QVariant &value)
{
    m_target->setProperty(name.toUtf8().constData(),value);
}

void ICMetaObjectRecord::serializeContent(QDataStream &stream) const
{
    auto propNames = names();
    stream << propNames.count();
    for(auto name: propNames)
    {
        auto val = m_target->property(name.toUtf8().constData());
        stream << name << val;
    }
}

void ICMetaObjectRecord::deserializeContent(QDataStream &stream)
{
    int cnt = 0;
    stream >> cnt;
    for(int i=0;i<cnt;i++)
    {
        QString name;
        QVariant val;
        stream >> name >> val;
        m_target->setProperty(name.toUtf8().constData(),val);
    }
}
