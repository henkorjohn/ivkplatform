#include <icbinaryrecord.h>

IC_REGISTER_ICRECORDTYPE(ICBinaryRecord)

QStringList ICBinaryRecord::g_names=QStringList("content"); // т.к. конструктор QStringList очень медленный, то приходится делать так

ICBinaryRecord::ICBinaryRecord(const QByteArray &content)
    : ICRecord(),
      m_content(content)
{
}

void ICBinaryRecord::serializeContent(QDataStream &stream) const
{
    stream << m_content;
}

void ICBinaryRecord::deserializeContent(QDataStream &stream)
{
    stream >> m_content;
}

void ICBinaryRecord::setValue(const QString &name, const QVariant &value)
{
    if(name!=QString("content"))
        return;
    m_content = value.toByteArray();
}
