#ifndef ICSHAREDRECORD_H
#define ICSHAREDRECORD_H

#include <icrecord.h>
#include <memory>

typedef std::shared_ptr<ICRecord> SharedRecordPointer;
typedef QList<SharedRecordPointer> SharedRecordList;

/**
 * @brief Обертка ICRecord.
 * 
 * Реализует интерфейс ICRecord, используя в качестве источника умный указатель на оригинальную запись.
 * @see ICRecord
 * @ingroup dataaccess
 */
class ICCONTAINERSSHARED_EXPORT ICSharedRecord : public ICRecord
{
public:
    ICSharedRecord(SharedRecordPointer recPtr=SharedRecordPointer());

    QStringList names() const final{return m_recPtr->names();}
    QVariant value(const QString &name) const final{return m_recPtr->value(name);}
    void setValue(const QString &name, const QVariant &value) final{m_recPtr->setValue(name,value);}
    ICRecord *newInstance() const{Q_ASSERT(false);return new ICSharedRecord();} //TODO: debug
    QString className() const{return "ICSharedRecord";}
    inline ICRecord* get() const{return m_recPtr.get();}

private:
    void serializeContent(QDataStream& ds) const{ m_recPtr->serializeContent(ds);}
    void deserializeContent(QDataStream& ds){m_recPtr->deserializeContent(ds);}

    SharedRecordPointer m_recPtr;
};

#endif // ICSHAREDRECORD_H
