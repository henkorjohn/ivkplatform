#ifndef ICRECORDTYPESCOLLECTOR_H
#define ICRECORDTYPESCOLLECTOR_H

#include <QHash>
#include <QDebug>
#include "iccontainers_global.h"

class ICRecord;

#ifdef Q_OS_WIN
#define FUNCEXPORT ICCONTAINERSSHARED_EXPORT
#else
#define FUNCEXPORT
#endif

namespace ivk
{

/**
 * @brief Класс, служащий для сбора информации о наследниках ICRecord, через который создаются новые объекты нужного типа.
 * @ingroup core database
 */
class ICCONTAINERSSHARED_EXPORT ICRecordTypesCollector 
{
public:
    ICRecordTypesCollector(){}
    virtual ~ICRecordTypesCollector(){ qDeleteAll(m_typeInstances); }
    static ICRecordTypesCollector* instance() {if(g_collectorInstance==nullptr) g_collectorInstance=new ICRecordTypesCollector(); return g_collectorInstance;}
    static ICRecordTypesCollector* g_collectorInstance;
    QHash<QString, int> m_typeHash;    ///< хэш соответствий имени типа и индекса соответствующего типа в списке
    QList<ICRecord*> m_typeInstances;  ///< массив объектов, через которые создаются объекты нужного типа 
};

//extern ICRecordTypesCollector* ICRecordTypesCollector::g_collectorInstance;

/**
 * @brief Служит для регистрации типа, наследуемого от ICRecord.
 * @ingroup core database
 */
template<typename T>
int icRegisterType(const QString& name)
{
    ICRecordTypesCollector* inst = ICRecordTypesCollector::instance();
    if(inst->m_typeHash.contains(name))
    {
	qDebug()<<QString("Тип с именем %1 уже зарегистрирован!").arg(name);
	return inst->m_typeHash[name];
    }
    inst->m_typeInstances.append(new T());
    inst->m_typeHash[name] = inst->m_typeInstances.size()-1;
    return inst->m_typeInstances.size()-1;
}

/**
 * @brief Возвращает индекс, под которым зарегистрирован нужный тип.
 * @return Если тип не зарегистрирован, то возвращается -1. 
 * @ingroup core database
 */
ICCONTAINERSSHARED_EXPORT int icTypeIndex(const QString& name);

/** 
 * @brief С оздает новый объект заданного строкой типа.
 *
 * Подразумевается, что такой тип гарантировано зарегистрирован. 
 * @ingroup core database
 */
ICCONTAINERSSHARED_EXPORT ICRecord* icNewInstanceByName(const QString& name);

/** 
 * @brief Создает новый объект по его идентификатору
 *
 * Подразумевается, что такой тип гарантировано зарегистрирован. 
 * @ingroup core database
 */ 
ICCONTAINERSSHARED_EXPORT ICRecord* icNewInstance(int typeId);
    
/** @brief Класс, регистрирующий операторы сериализации в поток нужного класса в конструкторе.
 * @ingroup core database
 */
template<typename T> class ICRecordTypeRegistrator 
{
public:
    ICRecordTypeRegistrator(const char* tname){ icRegisterType<T>(tname);}
};

}

#endif /* ICRECORDTYPESCOLLECTOR_H */
