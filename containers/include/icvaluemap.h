#ifndef ICVALUEMAP_H
#define ICVALUEMAP_H

#include <icrecord.h>
#include <QHash>

/**
 * @brief Реализация класса-контейнера на основе QMap<QString,QVariant>
 * @ingroup core database
 */
class ICCONTAINERSSHARED_EXPORT ICValueMap : public ICRecord
{
public:
    QStringList names() const Q_DECL_FINAL {return m_values.keys();}
    QVariant value(const QString &name) const Q_DECL_FINAL {return m_values[name];}
    void setValue(const QString& name,const QVariant& value) Q_DECL_FINAL {m_values[name] = value;}
    void serializeContent (QDataStream &stream) const Q_DECL_FINAL;
    void deserializeContent(QDataStream &stream) Q_DECL_FINAL;
    QString className() const override {return "ICValueMap";}
    ICRecord* newInstance() const override {return new ICValueMap;}

private:
    QHash<QString, QVariant> m_values;
};

#endif // ICVALUEMAP_H
