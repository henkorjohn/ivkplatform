#ifndef ICOBJECTRECORD_H
#define ICOBJECTRECORD_H

#include "icobjectrecord_global.h"
#include "icrecord.h"
#include <icobject.h>
#include <QStringList>

/**
 * @brief Базовый класс для создания классов, которые могут определять разные политики передачи своих данных через QDataStream и свое представление в БД.
 * 
 * По-умолчанию содержит все необходимое, наследникам остается только определить нужные для сохранения поля через Q_PROPERTY. 
 * @ingroup database core
 */
class ICOBJECTRECORDSHARED_EXPORT ICObjectRecord : public ICObject, public ICRecord 
{
    Q_OBJECT
public:
    Q_PROPERTY ( qulonglong rowid READ rowid WRITE setRowid NOTIFY rowidChanged) ///< идентификатор строки
    inline qulonglong rowid() const {return m_rowid;}
    void setRowid(qulonglong val){if(m_rowid==val) return; m_rowid=val;emit rowidChanged();}
    Q_PROPERTY ( qulonglong updCounter READ updCounter WRITE setUpdCounter ) ///< счетчик обновления поля
    inline qulonglong updCounter() const {return m_updCounter;}
    void setUpdCounter(qulonglong val){if(m_updCounter==val) return; m_updCounter=val;}
    Q_PROPERTY ( qulonglong refCounter READ refCounter WRITE setRefCounter ) ///< счетчик ссылок на объект из других объектов
    inline qulonglong refCounter() const {return m_refCounter;}
    void setRefCounter(qulonglong val){m_refCounter=val;}

    explicit ICObjectRecord(QObject* parent=nullptr);
    ICObjectRecord(const ICObjectRecord& c); ///< конструктор копирования 
    virtual ~ICObjectRecord();
    QStringList names() const override;
    bool contains(const QString& nm) const override;
    Q_INVOKABLE QVariant value(const QString& name) const override;
    Q_INVOKABLE void setValue(const QString& name,const QVariant& value) override;
    void serializeContent(QDataStream& stream) const override;
    void deserializeContent(QDataStream& stream) override;
    ICRecord* newInstance() const override;
    QString className() const override;
    /**
     * @brief подтверить изменения
     */
    void commit() final;
    /**
     * @brief откатить изменения
     */
    void rollback() final;
    /**
     * @brief восстановить статус измененных свойств
     */
    void restoreCommitState();
    QStringList changes() const final{return m_changes;}
private slots:
    inline void setChanges(const QStringList& val){m_changes=val;}
signals:
    void rowidChanged();
    void inserted(); ///< сигнал, сообщающий о добавлении объекта
    void removed(); ///< сигнал, сообщающий об удалении объекта
    void selected(); ///< сигнал, сообщающий о в выборке атрибутов объекта
protected:

    /**
     * Шаблоны для простого преобразования списков типов в QVariantList и обратно
     */
    template<typename T>
    QVariantList toVl(const QList<T>& lst) const
    {
        QVariantList vl;
        for(auto i:lst)
            vl.append(QVariant::fromValue(i));
        return vl;
    }

    template<typename T>
    void setFromVl(QVariantList vl, QList<T>* lst, bool deleteOld=true)
    {
        if(deleteOld)
        {
            for(auto v:vl)
            {
                auto o = v.value<T>();
                if(lst->indexOf(o)!=-1)
                    lst->removeOne(o);
            }
            qDeleteAll(*lst);
        }
        lst->clear();
        for(auto i:vl)
            lst->append(i.value<T>());
    }
    /**
     * @brief Копирует свойства объекта.
     *
     * Метод предназначен для быстрой реализации конструктора копирования классов наследников.
     * @param cpy копируемый объект
     */
    void copy(const ICObjectRecord* cpy);
private:
    bool isPropSerializable(const QString &pname) const final;
    QHash<QString, int> m_properties; ///< соответствие имя-свойства->его-индекс-в-metaobject
    qulonglong m_rowid = 0;
    qulonglong m_updCounter = 0;
    qulonglong m_refCounter = 0;
    QHash<QString,QVariant> m_commitedValues;
    QStringList m_changes;
};

#endif /* ICOBJECTRECORD_H */
