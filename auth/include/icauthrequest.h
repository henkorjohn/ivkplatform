#ifndef ICAUTHREQUEST_H
#define ICAUTHREQUEST_H

#include "icauth_global.h"
#include "icpermission.h"

/**
 * @brief Класс служит для запросов на доступ к какому-то объекту из определенной сессии.
 * @ingroup auth
 */
class ICAUTHSHARED_EXPORT ICAuthRequest
{
public:
    quint64 sessionId=0; ///< идентификатор текущей сессии
    quint64 objectId=0;  ///< идентификатор объекта, на который запрашиваются права
    ICPermission::Privileges priv = ICPermission::None; ///< права 
};

QDataStream& operator<<(QDataStream& out, const ICAuthRequest& req) 
{
    out<<req.sessionId<<req.objectId<<req.priv;
    return out;
}

QDataStream& operator>>(QDataStream& in, ICAuthRequest& req) 
{
    int priv=0;
    in>>req.sessionId>>req.objectId>>priv;
    req.priv=(ICPermission::Privileges)priv;
    return in;
}

#endif // ICAUTHREQUEST_H
