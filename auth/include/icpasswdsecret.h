#ifndef ICPASSWDSECRET_H
#define ICPASSWDSECRET_H

#include "icauth_global.h"
#include <QString>
#include <icsecret.h>

/**
 * @brief Секрет-пароль.
 * @ingroup auth
 */
class ICAUTHSHARED_EXPORT ICPasswdSecret : public ICSecret
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit ICPasswdSecret(QObject* parent=nullptr):ICSecret(parent){m_container.setDataType(QString(metaObject()->className()));}
    explicit ICPasswdSecret(const ICPasswdSecret& s):ICSecret(s){setContainer(s.container());}
    ICPasswdSecret(const QString& passwd);
//    ICSecret* make_copy() const;
    bool operator==(const ICPasswdSecret& other);
    Q_INVOKABLE void setPassword(const QString& p); ///< задает строку-пароль 
};

//QDataStream &operator<<(QDataStream &out, const ICPasswdSecret &myObj);
//QDataStream &operator>>(QDataStream &in, ICPasswdSecret &myObj);

Q_DECLARE_METATYPE(ICPasswdSecret)

#endif // ICPASSWDSECRET_H
