#ifndef ICCREDENTIALS_H
#define ICCREDENTIALS_H

#include "icauth_global.h"
#include <QString>
#include <icsecretcontainer.h>
#include <icobjectrecord.h>

/**
 * @brief Аутентификационные данные пользователя.
 * @ingroup auth
 */
class ICAUTHSHARED_EXPORT ICCredentials : public ICObjectRecord
{
    Q_OBJECT
public:
    Q_PROPERTY(QString name READ name WRITE setName)
    Q_PROPERTY(ICSecretContainer* secret READ secretMutable WRITE setSecret)

    ICSecretContainer* secretMutable(){ return &m_secretData;}
    Q_INVOKABLE explicit ICCredentials(QObject* parent=nullptr);
    ICCredentials(const ICCredentials& c);
    ~ICCredentials();
    ICCredentials(const QString& n, const ICSecretContainer &s);
    const ICSecretContainer* secret() const {return &m_secretData;} ///< Бинарный контейнер с секретом пользователя
    void setSecret(const ICSecretContainer* s);                     ///< устанавливает готовый секрет пользователя
    QString name() const {return m_name;}                           ///< имя пользователя 
    void setName(const QString& n) {m_name=n;}                      ///< устанавливает имя пользователя
    ICCredentials& operator=(const ICCredentials& c);               ///< оператор присваивания
private:
    QString m_name;
    ICSecretContainer m_secretData;
};

Q_DECLARE_METATYPE(ICCredentials)

#endif // ICCREDENTIALS_H
