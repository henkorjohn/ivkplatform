#ifndef ICAUTH_GLOBAL_H
#define ICAUTH_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(ICAUTH_LIBRARY)
#  define ICAUTHSHARED_EXPORT Q_DECL_EXPORT
#else
#  define ICAUTHSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // ICAUTH_GLOBAL_H
