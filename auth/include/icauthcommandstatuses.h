#ifndef ICAUTHCOMMANDSTATUSES_H
#define ICAUTHCOMMANDSTATUSES_H

#include <iccommand.h>

#define AUTH_STATUS_AUTHFAIL (COMMAND_STATUS_USER + 1) ///< Ошибка аутентификации пользователя @ingroup auth
#define AUTH_STATUS_DENY     (COMMAND_STATUS_USER + 2) ///< доступ запрещен @ingroup auth
#define AUTH_STATUS_NOAUTH   (COMMAND_STATUS_USER + 3) ///< пользователь не авторизован @ingroup auth

#endif // ICAUTHCOMMANDSTATUSES_H
