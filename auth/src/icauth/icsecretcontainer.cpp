#include "icsecretcontainer.h"

IC_REGISTER_METATYPE(ICSecretContainer)
IC_REGISTER_ICRECORDTYPE(ICSecretContainer)

ICSecretContainer::ICSecretContainer(QObject* parent) : ICObjectRecord(parent)
{
}

ICSecretContainer::ICSecretContainer(const ICSecretContainer& c) : ICObjectRecord()
{
    *this = c;
}

ICSecretContainer::~ICSecretContainer()
{
    wipeData();
}

void ICSecretContainer::wipeData()
{
    for(int i=0; i<m_data.size();i++)
        m_data[i]=qrand();
}

ICSecretContainer& ICSecretContainer::operator=(const ICSecretContainer& s)
{
    copy(&s);
//    setData(s.data());
//    m_dataType=s.dataType();
    return *this;
}

/*QDataStream &operator<<(QDataStream &out, const ICSecretContainer &myObj)
{
    out<<myObj.dataType();
    out<<myObj.data();
    return out;
}

QDataStream &operator>>(QDataStream &in, ICSecretContainer &myObj)
{
    QByteArray d;
    QString dt;
    in>>dt;
    in>>d;
    myObj.setData(d);
    myObj.setDataType(dt);
    return in;
}
*/
