#include "icuserphoto.h"
#include <QUrl>
#include <QFile>

IC_REGISTER_METATYPE(ICUserPhoto)
IC_REGISTER_METATYPE_PTR(ICUserPhoto)
IC_REGISTER_ICRECORDTYPE(ICUserPhoto)

ICUserPhoto::ICUserPhoto(QObject *parent):ICObjectRecord(parent){}

ICUserPhoto::ICUserPhoto(const ICUserPhoto &c):ICObjectRecord(){*this=c;}

bool ICUserPhoto::load(const QString &urlString)
{
    // load from url as a string
    QUrl url(urlString);
    QFile file(url.toLocalFile());
    if (!file.open(QIODevice::ReadOnly))
        return false;
    setImageData(file.readAll());
    file.close();
    return true;
}

void ICUserPhoto::setImageData(QByteArray val){
    if(m_imageData==val)
        return;
    m_imageData=val;
    emit imageDataChanged();
}

ICUserPhoto& ICUserPhoto::operator=(const ICUserPhoto& p)
{
    copy(&p);
    return *this;
}
