#include <icsecret.h>
#include <QDebug>
#include <icobjectmacro.h>

IC_REGISTER_METATYPE(ICSecret)
//IC_REGISTER_METATYPE_STREAM_OPERATORS(ICSecret)

ICSecret::ICSecret(const ICSecret& s) : ICObjectRecord()
{
    setContainer(s.container());
}

//ICSecret* ICSecret::make_copy() const
//{
//    ICSecret* ret = new ICSecret();
//    ret->setData(m_data);
//    return ret;
//}

//QDataStream &operator<<(QDataStream &out, const ICSecret &myObj)
//{
//    out<<myObj.pack();
//    return out;
//}

//QDataStream &operator>>(QDataStream &in, ICSecret &myObj)
//{
//    QByteArray arr;
//    in>>arr;
//    ICSecret* obj = qobject_cast<ICSecret*>(ICObject::unpack(arr));
//    myObj = *obj;
//    delete obj;
//    return in;
//}
