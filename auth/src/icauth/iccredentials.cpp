#include <iccredentials.h>

IC_REGISTER_METATYPE(ICCredentials)

ICCredentials::ICCredentials(QObject* parent) : ICObjectRecord(parent)
{
}

ICCredentials::ICCredentials(const ICCredentials& c) : ICObjectRecord()
{
    m_name = c.name();
    setSecret(c.secret());
}

ICCredentials::ICCredentials(const QString& n, const ICSecretContainer& s) : ICObjectRecord()
{
    m_name=n;
    m_secretData = s;
}

ICCredentials& ICCredentials::operator=(const ICCredentials& c)
{
    m_name = c.name();
    setSecret(c.secret());
    return *this;
}

ICCredentials::~ICCredentials()
{
}

void ICCredentials::setSecret(const ICSecretContainer* s)
{
    m_secretData = *s;
}
