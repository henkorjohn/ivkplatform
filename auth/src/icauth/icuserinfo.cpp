#include <icuserinfo.h>
#include <icpasswdsecret.h>

IC_REGISTER_METATYPE(ICUserInfo)
IC_REGISTER_ICRECORDTYPE(ICUserInfo)

ICUserInfo::ICUserInfo(QObject *parent):ICObjectRecord(parent)
{
    connect(&m_photo, SIGNAL(imageDataChanged()), this, SIGNAL(userPhotoChanged()));
}

ICUserInfo::ICUserInfo(const ICUserInfo &c):ICObjectRecord()
{
    operator=(c);
    connect(&m_photo, SIGNAL(imageDataChanged()), this, SIGNAL(userPhotoChanged()));
}

ICUserInfo& ICUserInfo::operator=(const ICUserInfo& i)
{
        //    this->setName(i.name());
        //    setSecret(i.secret());
        //    setRoles(i.roles());
    for(QString name:names())
    {
        auto nm = name.toUtf8().constData();
        setProperty(nm,i.property(nm));
    }
    return *this;
}

QVariantList ICUserInfo::vlRoles() const
{
    QVariantList vl;
    for(auto i:m_roles)
        vl.append(QVariant::fromValue(i));
    return vl;
}

void ICUserInfo::setVlRoles(const QVariantList& rs)
{
    qDeleteAll(m_roles);
    m_roles.clear();
    for(auto i:rs)
        m_roles.append(i.value<ICRole*>());
}

void ICUserInfo::setTextSecret(const QString & textSecret)
{
    ICPasswdSecret newSecret(textSecret);
    auto curSc = secretMutable();
    auto newSc = newSecret.container();
    curSc->setDataType(newSc->dataType());
    curSc->setData(newSc->data());
    //    setSecret(secret.container());
}

void ICUserInfo::setPhoto(const ICUserPhoto *photo)
{
    m_photo = *photo;
    emit userPhotoChanged();
}

bool ICUserInfo::loadPhoto(const QString &url)
{
    return m_photo.load(url);
}
