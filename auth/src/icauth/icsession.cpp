#include <icsession.h>
#include <QDateTime>
//ICSession* ICSession::m_current = nullptr;

ICSession::ICSession(const ICUserInfo& user, qint64 expTime, ICRole::Policy policy)
{
    m_sessionId = ((quint64)qrand()<<32) | qrand();
    m_user = user;
    m_policy = policy;
    m_expirationTime = expTime;
    ICRole userRole;
    for(auto r : user.constRoles())
        userRole<<*(dynamic_cast<ICRole*>(r));
    auto perm = userRole.superposition(policy, policy);
    ICPermission* pms;
    for(auto p : perm)
    {
        pms=dynamic_cast<ICPermission*>(p);
        m_priveleges[pms->object()] = pms->privileges();
    }
    qDeleteAll(perm);
}

bool ICSession::canAccess(quint64 obj, ICPermission::Privileges priv, QString* denyReason) const
{
    if(!isActive())
    {
        if(denyReason != nullptr)
            *denyReason = QString("Время сессии закончилось");
        return false;
    }
    if(!m_priveleges.contains(obj))
        goto DENY;
    if((m_priveleges[obj] & priv) == ICPermission::None)
        goto DENY;
    return true;

DENY:
    if(denyReason != nullptr)
        *denyReason = QString("Нет права доступа к объекту");
    return false;
}

bool ICSession::isActive() const
{
    return m_expirationTime > QDateTime::currentMSecsSinceEpoch();
}
