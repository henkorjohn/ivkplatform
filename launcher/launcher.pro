#-------------------------------------------------
#
# Project created by QtCreator 2014-03-19T13:45:33
#
#-------------------------------------------------
include(../platform_common.pri)

QT       += core network
QT       -= gui

TARGET = launcher
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += main.cpp

LIBS += -L$$DESTDIR -livkplatform -licobject -licsettings -licnetwork

unix:!mac{
QMAKE_LFLAGS += -Wl,--rpath=\\\$\$ORIGIN
}
