#include <QCoreApplication>
#include <icplatform.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    a.setOrganizationName("ivk-center");
    a.setApplicationName("launcher");
    ICPlatform pl;
    Q_UNUSED(pl);
    return a.exec();
}
