#ifndef ICDBREMOVEREQUEST_H
#define ICDBREMOVEREQUEST_H

#include <icdbconditionrequest.h>

/**
 * @brief Класс-контейнер запроса на удаление
 * @ingroup database
 */
class ICDBREQUESTSHARED_EXPORT ICDbRemoveRequest : public ICDbConditionRequest
{
public:
    ICDbRemoveRequest(const QString& dbname="") : ICDbConditionRequest(dbname){}
    DbRequestType type() const Q_DECL_FINAL {return RemoveType;}
};

#endif // ICDBREMOVEREQUEST_H
