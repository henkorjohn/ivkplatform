#ifndef DBSELECT_H
#define DBSELECT_H

#include <icdbselectrequest.h>
#include <iccommand.h>

ICDbSelectionFields& operator+(ICDbSelectionFields& first, const ICDbSelectionFields& second);

ICDbSelectionFields makeSelectionFields(const QString& name, ICAggregation::AggregationFunc aggr=ICAggregation::None);


#define what(n) makeSelectionFields(##n)

namespace aggr
{
#define min(n) makeSelectionFields(##n, ICAggregation::Min)
#define max(n) makeSelectionFields(##n, ICAggregation::Max)
#define avg(n) makeSelectionFields(##n, ICAggregation::Avg)
#define count(n) makeSelectionFields(##n, ICAggregation::Count)
}

//namespace cond
//{
#define less ICCondition::Less
#define greater ICCondition::Greater
#define notequals ICCondition::NotEquals
#define equals ICCondition::Equals
#define lessorequals ICCondition::LessOrEquals
#define greaterorequals ICCondition::GreaterOrEquals
#define like ICCondition::Like
#define where(n,o,v) ICConditionSet(ICCondition(QString(#n),v,o))
//}

#define orderby(n,a) ICDbSelectRequest::ICSortingPair(#n,ICDbSelectRequest::##a)

ICCommand& select(ICCommand& cmd, QString retType, const QStringList& fromTypes, const ICDbSelectionFields& fields, const ICConditionSet& conds, int count, int countFrom, ICDbSelectRequest::ICSortingPair sort, bool cascade);

#define selectCascade(a,b,c,d,e,f,g) select(this->newCommand(), QString(##a), QString(##b).remove(QRegExp("[\{,\}, ]")).split(','), c, d, e, f, g, true)

/**
  * @sample selectSimple(PSTag, {PSTag}, what(all), cond::where(time,cond::less,QDateTime::currentDateTime(), 0, 0, orderby(updateTime,ASC),void[](){return;});
  */
#define selectSimple(a,b,c,d,e,f,g,h) this->request(select(this->newCommand(), QString(#a), QString(#b).remove(QRegExp("[\{,\}, ]")).split(','), c, d, e, f, g, false), h)

#endif /* DBSELECT_H */
