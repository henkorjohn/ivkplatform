#ifndef ICDBCONNPOOLMANAGER_H
#define ICDBCONNPOOLMANAGER_H

#include <icdbconnectionpool.h>

/**
 * @brief Менеджер пула подключений к базе данных.
 * @ingroup database
 */
class DBCONNECTIONSHARED_EXPORT ICDbConnPoolManager : public ICObject
{
public:
    ICDbConnPoolManager(QObject* parent = nullptr):ICObject(parent){}
    ~ICDbConnPoolManager();
    /**
     * @brief Получить экземпляр пула подключений к БД с указанным именем
     * @param dbname имя БД
     * @return экземпляр пула подключений
     */
    ICDbConnectionPool* pool(const QString& dbname);
    /**
     * @brief Предоставляет хэндл подключения к БД для определенного запроса
     * @param request запрос
     * @return ICDbConnection*
     */
    ICDbConnection* connection(const QString& dbName="", QString* errStr=nullptr);
    void releaseConnection(ICDbConnection* con);
    void setDefaultDbName(const QString& dbn){m_defaultDbName = dbn;}

private:
    QMutex m_poolMutex;
    QHash<QString, ICDbConnectionPool*> m_pools;
    QString m_defaultDbName;
};

#endif // ICDBCONNPOOLMANAGER_H
