#ifndef ICDBALLREQUESTS_H
#define ICDBALLREQUESTS_H

#include <icdbselectrequest.h>
#include <icdbinsertrequest.h>
#include <icdbupdaterequest.h>
#include <icdbremoverequest.h>
#include <icaggregation.h>
#include <icdbexecrequest.h>
#include <icdbupdategrouprequest.h>

#endif // ICDBALLREQUESTS_H
