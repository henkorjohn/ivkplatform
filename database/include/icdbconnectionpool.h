#ifndef ICDBCONNECTIONPOOL_H
#define ICDBCONNECTIONPOOL_H

#include <QMutex>
#include <icdbconnection.h>
#include <QSemaphore>
#include <icobject.h>

/**
 * @brief Пул подключений к БД.
 * 
 * Обеспечивает создание ограниченного количетсва подключений к БД с указанным именем, а также предоставляет безопасный доступ к этим подключениям клиентам.
 * Оперирует бэкендами к базе данных через подключи dbconnection:<dbname>/backend=</path/to/backend>
 * @ingroup database
 */
class DBCONNECTIONSHARED_EXPORT ICDbConnectionPool : public ICObject
{
    Q_OBJECT
public:
    ICDbConnectionPool(const QString& dbname, QObject* parent=nullptr);
    ~ICDbConnectionPool();
    /**
     * @brief Получить подключение.
     *
     * Если есть свободное подключение, занимает и возвращает его клиенту.
     * Иначе, если лимит подключений не исчерпан, создает новое, занимает и возвращает клиенту.
     * Иначе ждет освобождения какого-либо подключения в течение заданного таймаута.
     * @return экземпляр подключения
     */
    ICDbConnection* get();
    /**
     * @brief Освободить подключение
     * @param con
     */
    void release(ICDbConnection* con);

    ICRETCODE init(const QString& pref);
private:
    int m_maxSize;
    int m_waitTimeout;
    QString m_backend;
    QString m_dbname;
    QMutex m_mutex;
    QSemaphore m_semaphore;
    QHash<ICDbConnection*,bool> m_connections;
};


#endif // ICDBCONNECTIONPOOL_H
