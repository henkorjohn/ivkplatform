#ifndef ICDBREQUEST_H
#define ICDBREQUEST_H

#include "icdbrequests_global.h"
#include <QDataStream>
#include <QStringList>

/**
 * @brief Интерфейс запроса к серверу БД
 * @ingroup database
 */
class ICDBREQUESTSHARED_EXPORT ICDbRequest
{
public:
    ICDbRequest(const QString& dbname):m_databaseName(dbname){}
    virtual ~ICDbRequest(){}
    /**
     * @brief сериализация в поток
     * @param output поток
     */
    virtual void serialize(QDataStream& output) const;
    /**
     * @brief десериализация из потока
     * @param input поток
     */
    virtual void deserialize(QDataStream& input);

    // getters/setters:

//    inline QString typeName() const {return m_typeName;}      ///< имя типа, в котором нужно вернуть результаты
//    void setTypeName(const QString& val){m_typeName =val;}
    bool genericContainer() const {return m_genericContainer;}  ///< признак того, что результат надо вернуть не в типе запрашиваемой таблицы, а в контейнере ICValueMap
    void setGenericContainer(bool gc) {m_genericContainer = gc;} 

    void setDatabaseName(const QString& name) {m_databaseName=name;}
    QString databaseName() const {return m_databaseName;}
//protected:
    /**
     * @brief Типы запросов
     */
    enum DbRequestType
    {
        CommonType,
        SelectType, ///< выборка
        InsertType, ///< добавление
        UpdateType, ///< обновление
        RemoveType, ///< удаление
        GroupUpdateType ///< групповое обновление
    };
    /**
     * @brief Тип запроса
     * @return
     */
    virtual DbRequestType type() const{return CommonType;}
    QString typeName() const {return m_typeName;}
    void setTypeName(const QString& tn) {m_typeName=tn;}

private:
//    QString m_typeName;
    bool m_genericContainer=false;
    QString m_databaseName;
    QString m_typeName;
};

#endif // ICDBREQUEST_H
