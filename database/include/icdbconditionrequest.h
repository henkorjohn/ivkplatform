#ifndef ICDBPATTERNREQUEST_H
#define ICDBPATTERNREQUEST_H

#include <icdbrequest.h>
#include <icconditionset.h>

/**
 * @brief Класс-контейнер для запросов, применяемых для объектов, удовлетворяющих условиям.
 * @ingroup database
 */
class ICDBREQUESTSHARED_EXPORT ICDbConditionRequest : public ICDbRequest
{
public:
    ICDbConditionRequest(const QString& dbname);
    ICDbConditionRequest(const QString& dbname, const ICConditionSet& val);
    virtual ~ICDbConditionRequest();

    void serialize(QDataStream &output) const Q_DECL_OVERRIDE;
    void deserialize(QDataStream &input) Q_DECL_OVERRIDE;

    //getters/setters:

    inline const ICConditionSet& conditions() const{return m_conditions;}
    inline void setConditions(const ICConditionSet& val){m_conditions = val;}
    inline bool cascade() const {return m_cascade;}           ///< нужно ли производить каскадом действия над вложенными объектами
    inline void setCascade(bool cascade) {m_cascade=cascade;}

private:
    //members:
    ICConditionSet m_conditions;    ///< условие
    bool m_cascade = true;
};

#endif // ICDBPATTERNREQUEST_H


