#ifndef ICGENERICDATADRIVER_H
#define ICGENERICDATADRIVER_H

#include "icgenericdatadriver_global.h"
#include <icdatadriverinterface.h>

/**
 * @brief Реализация простого драйвера данных БД.
 * @see ICDataDriverInterface
 * @ingroup database
 */
class ICGENERICDATADRIVERSHARED_EXPORT ICGenericDataDriver : public ICDataDriverInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "ivk-center icgenericdatadriver")
    Q_INTERFACES(ICDataDriverInterface)

public:
    ICGenericDataDriver(QObject* parent=nullptr);
    ICRETCODE init(const QString& prefix, ICDbConnPoolManager *poolManager);

protected:
    /**
     * @brief Обработчик команды добавления.
     * @param cmd команда
     */
    virtual void handleInsert(ICCommand& cmd);

    /**
     * @brief Обработчик команды обновления.
     * @param cmd команда
     */
    virtual void handleUpdate(ICCommand& cmd);

    /**
     * @brief Обработчик команды удаления.
     * @param cmd команда
     */
    virtual void handleDelete(ICCommand& cmd);

    /**
     * @brief Обработчик команды выборки.
     * @param cmd команда
     */
    virtual void handleSelect(ICCommand& cmd);
    /**
     * @brief Обработчик команды группового обновления.
     * @param cmd команда
     */
    virtual void handleGroupUpdate(ICCommand& cmd);
};

#endif // ICGENERICDATADRIVER_H
