#include <icdbinsertrequest.h>
#include <QDebug>

void ICDbInsertRequest::serialize(QDataStream &output) const
{
    ICDbRequest::serialize(output);
    output << inserts;
}

void ICDbInsertRequest::deserialize(QDataStream &input)
{
    ICDbRequest::deserialize(input);
    input >> inserts;
}

bool ICDbInsertRequest::append(ICDbInsertRequest* req)
{
    if(req->databaseName()!=databaseName())
        return false;
    inserts.records.append(req->inserts.records);
    return true;
}
