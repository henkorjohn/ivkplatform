#include <icdbupdaterequest.h>

ICDbUpdateRequest::~ICDbUpdateRequest()
{
    if(m_changes && m_destroyRecords)
        delete m_changes;
}

void ICDbUpdateRequest::serialize(QDataStream &output) const
{
    ICDbConditionRequest::serialize(output);
    ICRecord::serialize(output,changes());
}

void ICDbUpdateRequest::deserialize(QDataStream &input)
{
    ICDbConditionRequest::deserialize(input);
    m_changes = ICRecord::deserialize(input);
}
