#include <icconditionset.h>
#include <QDebug>

ICConditionSet::ICConditionSet(const ICCondition& c)
{
    m_pairs.append(ICLogicalPair(c));
}

void ICConditionSet::append(const ICConditionSet& s, ICLogicalPair::LogicalOperator op)
{
    if(op==ICLogicalPair::LO_NONE)
        qDebug()<<__FILE__<<":"<<__LINE__<<": Ошибка добавления условия в список!";
    else if(!s.isEmpty())
    {
        if(!m_pairs.isEmpty())
            m_pairs.last().op=op;
        m_pairs.append(s.conditions());
    }
}

void ICConditionSet::operator&=(const ICCondition& c)
{
    append(ICConditionSet(c), ICLogicalPair::LO_AND);
}

void ICConditionSet::operator&=(const ICConditionSet& s)
{
    append(s, ICLogicalPair::LO_AND);
}

void ICConditionSet::operator|=(const ICCondition& c)
{
    append(ICConditionSet(c), ICLogicalPair::LO_OR);
}

void ICConditionSet::operator|=(const ICConditionSet& s)
{
    append(s, ICLogicalPair::LO_OR);
}

QString ICConditionSet::toString() const
{
    if(m_pairs.isEmpty())
        return QString();
    QString result;
    for(auto pair: m_pairs)
    {
        result+=pair.cond.toString();
        if(pair.op)
            result += " "+QString(pair.op == ICLogicalPair::LO_AND ? "&&" : "||")+" ";
    }
    return result;
}

ICConditionSet ICConditionSet::fromString(const QString &expression)
{
    ICConditionSet result;
    QString trueExp = expression;
    trueExp.replace("and","&&",Qt::CaseInsensitive).replace("or","||",Qt::CaseInsensitive);
    auto disExps = trueExp.split("||");
    for(auto disExp:disExps)
    {
        ICConditionSet cset;
        auto conExps = disExp.split("&&");
        for(QString cExp: conExps)
        {
            auto cond = ICCondition::fromString(cExp);
            if(!cond.isEmpty())
                cset &= cond;
        }
        result |= cset;
    }
    return result;
}

QDataStream& operator<<(QDataStream& out, const ICConditionSet& s)
{
    out<<s.conditions().count();
    for(auto p:s.conditions())
    {
        out<<p.cond;
        out<<(int)p.op;
    }
    return out;
}

QDataStream& operator>>(QDataStream& in, ICConditionSet& s)
{
    int cnt = 0;
    in>>cnt;
    QList<ICLogicalPair> pairs;
    for(int i=0; i<cnt; i++)
    {
        ICCondition c;
        int o=0;
        in>>c;
        in>>o;
        pairs.append({c, (ICLogicalPair::LogicalOperator)o});
    }
    s.setConditions(pairs);
    return in;
}

ICConditionSet operator &&(const ICConditionSet& left,const ICConditionSet& right)
{
    ICConditionSet result = left;
    result &= right;
    return result;
}

ICConditionSet operator ||(const ICConditionSet& left,const ICConditionSet& right)
{
    ICConditionSet result = left;
    result |= right;
    return result;
}
