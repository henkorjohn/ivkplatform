#include <iccondition.h>
#include <QRegExp>

QDataStream& operator << (QDataStream& stream,const ICCondition& condition)
{
    return stream << condition.field() << (int)condition.compareOperator() << condition.value();
}

QDataStream& operator >> (QDataStream& stream,ICCondition& condition)
{
    QString field;
    int op = 0;
    QVariant val;
    stream >> field >> op >> val;
    condition.setField(field);
    condition.setCompareOperator((ICCondition::ConditionOperators)op);
    condition.setValue(val);
    return stream;
}


ICCondition ICCondition::fromString(const QString &expression)
{
    ICCondition result;
    QRegExp rx("\\s*(\\S+)\\s*(=|>|<|>=|<=|!=|~)\\s*(.+)\\s*");
    if(rx.indexIn(expression)==-1)
        return ICCondition();
    result.setField(rx.cap(1));
    result.setCompareOperator(result.m_operDict.key(rx.cap(2)));
    auto valstr = rx.cap(3);
    // -> QString?
    QRegExp strPattern("'.+'\\s*");
    if(strPattern.exactMatch(valstr))
        result.setValue(valstr.mid(1,valstr.length()-2));
    // ->bool?
    else if(valstr=="true")
        result.setValue(true);
    else if(valstr=="false")
        result.setValue(false);
    // ->QByteArray?
    else if(valstr.startsWith("#"))
        result.setValue(QByteArray::fromHex(valstr.right(valstr.length()-1).toUtf8()));
    else
    {
        // -> double?
        bool ok=false;
        auto dnum = valstr.toDouble(&ok);
        if(ok)
            result.setValue(dnum);
        else
        {
            // -> qlonglong?
            auto lnum = valstr.toLongLong(&ok);
            if(!ok)
                return ICCondition();
            result.setValue(lnum);
        }
    }
    return result;
}

QString ICCondition::toString() const
{
    return field()+expression();
}

QString ICCondition::expression() const
{
    QString valexp="";
    switch(value().type())
    {
    case QVariant::String:
        valexp=QString("'%1'").arg(value().toString());
        break;
    case QVariant::ByteArray:
        valexp="#"+value().toByteArray().toHex();
        break;
    default:
        valexp = value().toString();
    }
    return m_operDict[compareOperator()]+valexp;
}
