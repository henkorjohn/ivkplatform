#-------------------------------------------------
#
# Project created by QtCreator 2014-01-14T16:29:10
#
#-------------------------------------------------
include(../common.pri)

QT       -= gui

TARGET = icdbconnection
TEMPLATE = lib
CONFIG += plugin

DEFINES += DBCONNECTION_LIBRARY
#           IC_MTDB                  # многопоточная реализация

SOURCES += icdbconnection.cpp \
           icdbinformation.cpp \
           icdbconnectionpool.cpp \
           icdbconnpoolmanager.cpp

HEADERS +=  $$INCLUDEDIR/icdbconnection.h \
            $$INCLUDEDIR/icdbinformation.h \
            $$INCLUDEDIR/icdbconnectionpool.h \
            $$INCLUDEDIR/icdbconnpoolmanager.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

LIBS += -L$$DESTDIR -licsettings -licobject -livkplatform -licdbrequests
