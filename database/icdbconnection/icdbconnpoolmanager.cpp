#include <icdbconnpoolmanager.h>

ICDbConnPoolManager::~ICDbConnPoolManager()
{
    qDeleteAll(m_pools);
}

ICDbConnectionPool* ICDbConnPoolManager::pool(const QString& dbname)
{
    QMutexLocker lock(&m_poolMutex);
    Q_UNUSED(lock);
    if(m_pools.contains(dbname))
        return m_pools.value(dbname);
    auto pool = new ICDbConnectionPool(dbname);
    pool->init(settingsPrefix());
    pool->moveToThread(this->thread());
    m_pools[dbname] = pool;
    return pool;
}

ICDbConnection* ICDbConnPoolManager::connection(const QString& dbName, QString *errStr)
{
    QString dbn = m_defaultDbName;
    if(dbName!="")
        dbn = dbName;
    auto conPool = pool(dbn);
    auto con = conPool->get();
    if(con==nullptr && errStr!=nullptr)
        *errStr = trUtf8("Не удалось получить подключение к БД.")+conPool->lastError();
    return con;
}

void ICDbConnPoolManager::releaseConnection(ICDbConnection *con)
{
    auto conPool = pool(con->databaseName());
    conPool->release(con);
}
