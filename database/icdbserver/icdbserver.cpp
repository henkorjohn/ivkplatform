#include "icdbserver.h"
#include <QPluginLoader>
#include <icsettings.h>
#include <QDebug>

ICDbServer::ICDbServer(QObject *parent) : ICTMPluginInterface(parent), m_conPoolManager(this)
{
}

ICDbServer::~ICDbServer()
{
    qDeleteAll(m_drivers);
}

ICPluginInfo ICDbServer::pluginInfo()
{
    return {"ICDbServer",{0,0,2},{2,0,0}};
}

ICRETCODE ICDbServer::init(const QString &pref)
{
    auto ret = ICTMPluginInterface::init(pref);
    m_conPoolManager.init(settingsPrefix());
    m_conPoolManager.setDefaultDbName(setting("database/name", "dbfile.bin").toString());
    // здесь читаем список драйверов данных для загрузки
    ICLocalSettings sett;
    sett.beginGroup(settingsPrefix());
    sett.beginGroup("drivers");
    QStringList drivers = sett.childGroups();
    // грузим драйверы
    for(const QString& dr : drivers)
    {
        sett.beginGroup(dr);
        QString path = sett.value("path").toString();
        if(path == "")
        {
            sett.endGroup();
            continue;
        }
        QPluginLoader loader(path);
        bool loaded = loader.isLoaded();
        ICDataDriverInterface* di = qobject_cast<ICDataDriverInterface*>(loader.instance());
        if(loaded)
            di = qobject_cast<ICDataDriverInterface*>(di->metaObject()->newInstance());
        if(di == nullptr)
        {
            if(loaded)
                reportError(trUtf8("ICDbServer: Невозможно загрузить драйвер данных %1. Возможно для конструктора не указано Q_INVOKABLE.").arg(path));
            else
                reportError(trUtf8("ICDbServer: Невозможно загрузить драйвер данных %1. (%2)").arg(path).arg(loader.errorString()));
            sett.endGroup();
            continue;
        }
        m_drivers.append(di);
        // подключимся к сигналам драйвера
        connect(di, &ICDataDriverInterface::done, this, &ICDbServer::reply);
        connect(di, &ICDataDriverInterface::send, this, &ICDbServer::onDriverSend);
        // проинициализируем
        di->init(settingsPrefix(), &m_conPoolManager);
        // получаем список обрабатываемых команд
        COMMAND_TYPE_LIST cmds = di->commandTypes();
        for(auto ctype : cmds)
        {
            // добавляем хэндлер для получения сообщений в плагин
            registerCommandHandler(ctype, (pluginFunc)&ICDbServer::incomingCommand, ctype.priority);
            // добавляем драйвер в мультихэш для передачи ему команд
            m_handlerDrivers[ctype] = di;
        }
        // перенесем драйвер в свой собственный поток, если надо
        if(sett.value("separateThread", false).toBool() == true)
        {
            QThread* thr = new QThread(this);
            qDebug()<<"ICDbServer: перенесем драйвер в свой собственный поток:"<< thr;
            di->moveToThread(thr);
            thr->start();
        }
        sett.endGroup();
    }
    return ret;
}

void ICDbServer::incomingCommand(ICCommand& cmd)
{
    // пришла команда. Надо разобраться, какому драйверу ее перенаправить.
    CommandType ct(cmd);
    if(!m_handlerDrivers.contains(ct))
    {
        reportError(trUtf8("ICDbServer: не найден драйвер для команды (%1:%2)").arg(ct.object).arg(ct.type));
        return;
    }
    ICDataDriverInterface* di = m_handlerDrivers.value(ct);
    di->pushCommand(cmd);
}

void ICDbServer::onDriverSend(ICCommand cmd)
{
    ICCommand tmp = newCommand(); //  |
    cmd.id = tmp.id;              //   > это нужно т.к. через интерфейс недоступна функция newCommand()
    cmd.time = tmp.time;          //  |
    cmd.address = tmp.address;    //  |
    request(cmd);
}

void ICDbServer::startWork()
{
    for(auto i : m_drivers)
        i->pushStart();
}
