#ifndef ICDBSERVER_H
#define ICDBSERVER_H

/**
  * @defgroup database Подсистема хранения
  */

#include "icdbserver_global.h"
#include <ictmplugininterface.h>
#include <icdbconnectionpool.h>
#include <icdbconnpoolmanager.h>
#include <icdatadriverinterface.h>

/**
 * @brief Базовый класс сервера БД.
 *
 * Реализует подсистему хранения данных, оперируя пулом подключений. Для доступа к данным используется интерфейс ICDbConnection.
 * Для доступа к данным использует драйверы, которые задаются в конфигурационном файле (в секции плагина) с подключем drivers/<Name>/path=...
 * @see ICDataDriverInterface
 * @see ICGenericDataDriver
 * @see ICDbConnection
 * @ingroup database
 */
class ICDBSERVERSHARED_EXPORT ICDbServer : public ICTMPluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "ivk-center icdbserver")
    Q_INTERFACES(ICPluginInterface)

public:
    ICDbServer(QObject* parent=nullptr);
    ~ICDbServer();

    ICPluginInfo pluginInfo() override;
    ICRETCODE init(const QString &pref) override;

protected:
    ICDbConnPoolManager m_conPoolManager;
    void incomingCommand(ICCommand& cmd);

protected slots:
    void startWork();

private:
    QHash<CommandType, ICDataDriverInterface*> m_handlerDrivers; // соответствие типа обрабатываемой команды драйверу. Может быть только однозначным.
    QList<ICDataDriverInterface*> m_drivers; // нужно только для удобного вызова функций старта и удаления в деструкторе
private slots:
    void onDriverSend(ICCommand cmd);
};

#endif // ICDBSERVER_H
