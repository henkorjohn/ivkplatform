#include "icsqlconnection.h"
#include <iclocalsettings.h>
#include <QSqlDriver>
#include <icvaluemap.h>

#define MYSQLERROR_TABLELOCKED 1213

ICSqlConnection::ICSqlConnection(const QString &dbname, const QString &conString)
    : ICDbConnection(dbname,conString)
{
    m_knownTypes[QVariant::Bool] = "BOOLEAN";
    m_knownTypes[QVariant::Int] = "INT";
    m_knownTypes[QVariant::UInt] = "INT";
    m_knownTypes[QVariant::Double] = "REAL";
    m_knownTypes[QVariant::Date] = "DATE";
    m_knownTypes[QVariant::String] = "VARCHAR";
    m_knownTypes[QVariant::DateTime] = "DATETIME";
    m_knownTypes[QVariant::ULongLong] = "INTEGER";
    m_knownTypes[QVariant::LongLong] = "NUMERIC";
}
ICSqlConnection::~ICSqlConnection()
{
//    m_knownTypes.clear();
}

bool ICSqlConnection::openDB()
{
    //инициализировать подключение
    ICLocalSettings sett;
    //m_recordType = (ICRecord::RecordType)sett.value(m_dbInfo->settingsGroup()+"recordType",ICRecord::ValueMapRecord).toInt();
    auto driver = m_dbInfo->driver();
    auto conName = connectionName();
    if(QSqlDatabase::contains(conName))
    {
        issueWarning(trUtf8("Получено соединение с базой с существующим именем: %1").arg(conName));
        m_db = QSqlDatabase::database(conName,false);
    }
    else
    {
        m_db = QSqlDatabase::addDatabase(driver, conName);
        m_db.setDatabaseName(databaseName());
        m_db.setHostName(m_dbInfo->serverHostname());
        m_db.setPort(m_dbInfo->serverPort());
        m_db.setUserName(m_dbInfo->username());
        m_db.setPassword(m_dbInfo->password());
    }
    // открыть подключение
    if(!m_db.open())
    {
        reportDbError();
        return false;
    }
    m_query = QSqlQuery(m_db);
    //настроить БД
    if(driver == QString("QSQLITE"))
    {
        // для SQLITE настроить прагмы
        QStringList pragmas {
            "foreign_keys = ON",
            "synchronous = 0",
            "read_uncommited = true",
            "count_changes = false",
            "journal_mode = MEMORY",
            "temp_store = MEMORY"};
        for(auto stmt : pragmas)
        {
            if(!execStmt("PRAGMA "+stmt))
                return false;
        }
    }
    return true;
}

void ICSqlConnection::closeDB()
{
    m_query.clear();
    m_query = QSqlQuery();
    m_db.close();
    m_db = QSqlDatabase();
    QSqlDatabase::removeDatabase(connectionName());
}

bool ICSqlConnection::beginDBTransaction()
{
    if(!checkState())
        return false;
    bool ok = m_db.transaction();
    if(!ok)
        reportDbError();
    return ok;
}

bool ICSqlConnection::endDBTransaction(bool toCommit)
{
    if(!checkState())
        return false;
    bool ok = toCommit ? m_db.commit() : m_db.rollback();
    if(!ok)
        reportDbError();
    return ok;
}

bool ICSqlConnection::execQuery()
{
    bool ok = false;
    while(1) // почти не костыль. Связано с тем, что разные базы могут возвращать разные некритичные ошибки, при которых надо просто повторить запрос.
    {
        ok = m_query.exec();
        if(ok)
            break;
        if(needBreakOnSpecificDbError(m_query))
            break;
    }
    if(!ok)
        reportQueryError();
    return ok;
}

bool ICSqlConnection::needBreakOnSpecificDbError(const QSqlQuery& q)
{
    if(m_dbInfo->driver() == "QMYSQL")
    {
        if(q.lastError().number() == MYSQLERROR_TABLELOCKED) // просто кто-то в данный момент пишет в эту таблицу.
            return false;
    }
    return true;
}

bool ICSqlConnection::execStmt(const QString &stmt)
{
    if(!m_query.exec(stmt))
    {
        reportQueryError();
        return false;
    }
    return true;
}

QStringList ICSqlConnection::queryTypes() const
{
    return m_db.tables();
}

QStringList ICSqlConnection::queryFields(const QString &objType) const
{
    QSqlRecord rec =m_db.record(objType);
    QStringList result;
    for(int i=0;i<rec.count();i++)
        result << rec.fieldName(i);
    return result;
}

bool ICSqlConnection::createObjectType(const QString &typeName, const QList<ICDbFieldInfo> &fields)
{
    QStringList colDefs;
    for(ICDbFieldInfo field : fields)
    {
        auto colType = toSqlType(field.valueType);
        auto colDef = QString("%1 %2").arg(field.name).arg(colType);
        if(field.name == "rowid")
            colDef += " PRIMARY KEY AUTOINCREMENT";
        else if(isTableReference(field.name))
        {
            auto fldextras = parseReferenceField(field.name);
            colDef += QString(" REFERENCES %1(rowid) ON DELETE CASCADE").arg(fldextras[0]);
        }
        colDefs << colDef;
    }
    auto stmt = QString("CREATE TABLE %1 (%2);").arg(typeName).arg(colDefs.join(","));
    return execStmt(stmt);
}

QString ICSqlConnection::toSqlType(QVariant::Type type) const
{
    if(!m_knownTypes.contains(type))
        return "BLOB";
    return m_knownTypes.value(type);
}

bool ICSqlConnection::createField(const QString &typeName, const ICDbFieldInfo &field)
{
    auto stmt = QString("ALTER TABLE %1 ADD %2 %3;").arg(typeName).arg(field.name).arg(toSqlType(field.valueType));
    return execStmt(stmt);
}

QString ICSqlConnection::makeSelectFields(const ICDbSelectRequest& request)
{
    auto flds = request.selectionFields();
    QString res;
    if(flds.isEmpty())
        res="t.*";
    else 
        res="t.rowid,t.updCounter";    
    for(const ICDbSelectionField& f : flds)
    {
        if(f.func == ICAggregation::None)
            res+=QString(",%1").arg(f.field);
        else if(f.func == ICAggregation::Max)
            res+=QString(",MAX(%1)").arg(f.field);
        else if(f.func == ICAggregation::Min)
            res+=QString(",MIN(%1)").arg(f.field);
        else if(f.func == ICAggregation::Avg)
            res+=QString(",AVG(%1)").arg(f.field);
        else if(f.func == ICAggregation::Count)
            res+=QString(",COUNT(%1)").arg(f.field);
    }
    return res;
}

bool ICSqlConnection::selectObjects(const ICDbSelectRequest &request, ICDbResponse &response)
{
    auto tName = request.typeName();
    m_recordType = tName; // сохраним имя таблицы
    //сформировать выражение запроса
    // сначала сформируем поля, которые нужно выбрать (вдруг там есть агрегатные функции)
    auto fields = makeSelectFields(request);
    auto stmt = QString("SELECT %1 FROM %2 t %3").arg(fields).arg(tName);
    if(!request.groupByField().isEmpty())
        stmt += QString(" GROUP BY %1").arg(request.groupByField());
    if(!request.sortByField().isEmpty())
        stmt += QString(" ORDER BY %1 %2").arg(request.sortByField()).arg(request.sortingDirection()==ICDbSelectRequest::ASC?"ASC":"DESC");
    auto lim = request.count();
    if(lim!=0)
        stmt+=QString(" LIMIT %1").arg(lim);
    auto offset = request.offset();
    if(offset!=0)
        stmt+=QString(" OFFSET %2").arg(offset);
    if(request.onlyCount())
        stmt = QString("SELECT COUNT(*) AS count from (%1)").arg(stmt);
    m_query.clear();
    if(!prepareQuery(&request,stmt))
        return false;
    //выполнить запрос
    if(!execQuery())
        return false;
    // упаковать записи в контейнер
    packResults(response, request.genericContainer());
    return true;
}

void ICSqlConnection::packResults(ICDbResponse &response, bool genericContainer)
{
    QString t;
    if(genericContainer)
        t="ICValueMap";
    else
        t=m_recordType;
    int ti = ivk::icTypeIndex(t);
    if(ti<0)
    {
        reportError( QString("Тип %1 не зарегистрирован!").arg(m_recordType));
        return;
    }
    while(m_query.next())
    {
        auto sqlRec = m_query.record();
        auto rec = ivk::icNewInstance(ti);
        //пройтись по полученным полям, установив значения атрибутов
        int fieldCnt = sqlRec.count();
        for(int i=0;i<fieldCnt;i++)
        {
            auto sqlFieldName = sqlRec.fieldName(i);
            auto sqlFieldVal = sqlRec.value(sqlFieldName);
            rec->setValue(sqlFieldName,sqlFieldVal);
        }
        response.records.append(rec);
    }
}

bool ICSqlConnection::insertObject(const QString &typeName, const ICRecord *object, ICDbResponse &response)
{
    QList<ICRecord*> rl;
    rl.append((ICRecord*)object);
    return insertBulk(typeName, rl, response);
}

bool ICSqlConnection::insertBulk(const QString &typeName, const QList<ICRecord*>& objects, ICDbResponse &response)
{
    m_recordType = typeName;
    //сформировать список выражений
    auto nms = objects.first()->names();
    if(needsToPrepare())
    {
        QStringList fields;
        QStringList markers;
        QStringList unions;
        for(auto nm:nms)
        {
            if(nm == "rowid")
                continue;
            fields<<nm;
            markers<<"?";
        }
        QString unionMarkers = QString("(%1)").arg(markers.join(","));
        for(int i = 0; i < objects.count(); i++)
            unions<<unionMarkers;
        //сформировать строку запроса
        auto stmt = QString("INSERT INTO %1 (%2) VALUES %3;").arg(typeName).arg(fields.join(",")).arg(unions.join(","));
        if(!m_query.prepare(stmt))
        {
            reportQueryError();
            return false;
        }
    }
    //привязать значения
    int nmsCnt = nms.count();
    int cntr = 0;
    for(auto rec : objects)
        for(int i=0; i<nmsCnt; i++)
        {
            if(nms.value(i)=="rowid")
                continue;
            m_query.bindValue(cntr++, rec->value(nms.value(i)));
        }
    //выполнить запрос
    if(!execQuery())
        return false;
    // если драйвер поддерживает lastInsertId(), добавить в контейнер response запись с присвоенным идентификатором
    // проблема в том, что нам надо идентификаторы всех объектов, а не только последнего
    if(m_db.driver()->hasFeature(QSqlDriver::LastInsertId))
    {
        qulonglong fid = m_query.lastInsertId().toULongLong()-objects.size()+1; // первый rowid среди вставленных объектов
        for(int i=0; i<objects.size(); i++)
        {
            ICRecord* rec = new ICValueMap();
            rec->setValue("rowid", fid++);
            response.records.append(rec);
        }
    }
    return true;
}

bool ICSqlConnection::updateObjects(const ICDbUpdateRequest &request)
{
    //сформировать список выражений field = 'value'
    auto chgRec = request.changes();
    if(!chgRec)
    {
        reportError(trUtf8("Запрос не содержит изменений"));
        return false;
    }
    auto chgNms = chgRec->changes();
    QStringList fieldExprs;
    QVariantList vals;
    auto needsPrepare = needsToPrepare();
    for(auto nm: chgNms)
    {
        if(nm == "rowid")
            continue;
        if(needsPrepare)
            fieldExprs << QString("%1=?").arg(nm);
        vals << chgRec->value(nm);
    }
    if(needsPrepare)
    {
        //сформировать строку шаблона выражения
        auto stmt = QString("UPDATE %1 SET %2").arg(request.typeName()).arg(fieldExprs.join(','));
        auto condSet = request.conditions().conditions();
        if(!condSet.isEmpty())
        {
            QString conds;
            for(auto cond:condSet)
            {
                conds.append(makeCondExpression(cond));
                vals << cond.cond.value();
                switch (cond.op) {
                case ICLogicalPair::LO_AND:
                    conds.append(" AND ");
                    break;
                case ICLogicalPair::LO_OR:
                    conds.append(" OR ");
                    break;
                default:
                    break;
                }
            }
            auto condStmt = QString(" WHERE %1;").arg(conds);
            stmt += condStmt;
        }
        //выполнить запрос
        if(!m_query.prepare(stmt))
        {
            reportQueryError();
            return false;
        }
    }
    for(auto val:vals)
        m_query.addBindValue(val);
    return execQuery();
}

bool ICSqlConnection::removeObjects(const ICDbRemoveRequest &request)
{
    //сформировать строку шаблона выражения
    QString stmt = QString("DELETE FROM %1 %2").arg(request.typeName());
    //выполнить запрос
    return execRequest(&request,stmt);
}

bool ICSqlConnection::execRequest(const ICDbConditionRequest *request, const QString &stmtTemplate)
{
    if(!prepareQuery(request,stmtTemplate))
        return false;
    //выполнить запрос
    return execQuery();
}

bool ICSqlConnection::prepareQuery(const ICDbConditionRequest *request, const QString &stmtTemplate)
{
    //сформировать строку условия
    auto condSet = request->conditions().conditions();
    if(needsToPrepare())
    {
        QString stmt;
        if(!condSet.isEmpty())
        {
            QString conds;
            for(int i=0;i<condSet.length();i++)
            {
                conds.append(makeCondExpression(condSet[i]));
                switch (condSet[i].op) {
                case ICLogicalPair::LO_AND:
                    conds.append(" AND ");
                    break;
                case ICLogicalPair::LO_OR:
                    conds.append(" OR ");
                    break;
                default:
                    break;
                }
            }
            stmt = stmtTemplate.arg("WHERE "+conds);//QString("%1 WHERE %2;").arg(stmtTemplate).arg(cond);
        }
        else
            stmt = stmtTemplate.arg("");
        //подготовить запрос
        if(!m_query.prepare(stmt))
        {
            reportQueryError();
            return false;
        }
    }
    if(condSet.isEmpty())
        return true;
    //привязать значения
    int cnt = condSet.count();
    for(int i=0;i<cnt;i++)
        m_query.bindValue(i,condSet[i].cond.value());
    return true;
}

QString ICSqlConnection::makeCondExpression(ICLogicalPair& pair)
{
    QString result = QString(" %1%2?").arg(pair.cond.field());
    QString opExpr;
    int op = pair.cond.compareOperator();
    if(op & ICCondition::Less)
        opExpr.append("<");
    if(op & ICCondition::Greater)
        opExpr.append(">");
    if(op &  ICCondition::Equals)
        opExpr.append("=");
    else if(op == ICCondition::Like)
    {
        opExpr.append(" LIKE ");
        pair.cond.setValue(QString("%%1%").arg(pair.cond.value().toString()));
    }
    return result.arg(opExpr);
}

bool ICSqlConnection::execute(const ICDbExecRequest &request, ICDbResponse &response)
{
    if(!execStmt(request.stmt()))
        return false;
    packResults(response, false);
    return true;
}
