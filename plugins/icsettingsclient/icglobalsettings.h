#ifndef ICGLOBALSETTINGS_H
#define ICGLOBALSETTINGS_H

#include <icsettings.h>
//#include <icsettingsclient_global.h>

/**
 * @brief Класс глобальных настроек.
 * @ingroup settings
 */
class /*ICSETTINGSCLIENTSHARED_EXPORT*/ ICGlobalSettings : public ICSettings
{
    Q_OBJECT
public:
    ICGlobalSettings();
    ICGlobalSettings(const ICGlobalSettings& cpy) : ICGlobalSettings(){Q_UNUSED(cpy);}
    QVariant value(const QString& key, const QVariant& defaultValue = QVariant()) const final;
    QVariant queryValue(const QString &name) final;
    void setValue(const QString& key, const QVariant& value) final;
    void beginGroup(const QString prefix) final;
    void endGroup() final;
    QStringList childGroups() const final;
    QStringList childKeys() const final;
    QStringList allKeys() const final;
    void remove(const QString& key) final;
    void clear() final;
private slots:
    void onValueChanged(QString name,QVariant value);
private:
    QString m_prefix;
};

#endif // ICGLOBALSETTINGS_H
