#ifndef ICSETTINGSSERVER_H
#define ICSETTINGSSERVER_H

#include "icsettingsserver_global.h"
#include <ictmplugininterface.h>
#include <icsettingscontainer.h>

#define IC_DEFAULT_BACKEND "ICLocalSettings"

/**
 * @brief Класс плагина, реализующего хранение и удаленный доступ к глобальным конфигам.
 * @ingroup settings
 */
class ICSETTINGSSERVERSHARED_EXPORT ICSettingsServer : public ICTMPluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "ivk-center.icsettings-server")
    Q_INTERFACES(ICPluginInterface)
public:
    Q_INVOKABLE ICSettingsServer();

    //overriden methods:
    ICRETCODE init(const QString &prefix) final;
    ICPluginInfo pluginInfo() final {return {"icsettings-server",{0,0,1},{0,0,0}};}

private:
    //command handlers:
    void handleRead(ICCommand& cmd);
    void handleWrite(ICCommand& cmd);

    //internal methods:
    ICSettings* settings();
    void reportAndSendError(ICCommand& cmd,const char* msg);
    void sendSync(const ICSettingsContainer& sc);

    //members:
    ICSettings *m_settings = 0;
};

#endif // ICSETTINGSSERVER_H
