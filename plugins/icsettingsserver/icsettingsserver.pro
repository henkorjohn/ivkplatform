#-------------------------------------------------
#
# Project created by QtCreator 2014-03-26T13:07:19
#
#-------------------------------------------------
include(../plugins_common.pri)
QT       -= gui

TARGET = icsettingsserver
TEMPLATE = lib
CONFIG += plugin
DESTDIR = $$PLATFORM_DIR/bin/plugins

DEFINES += ICSETTINGSSERVER_LIBRARY

SOURCES += icsettingsserver.cpp

HEADERS += icsettingsserver.h

INCLUDEPATH += $$PLATFORM_DIR/ictmplugininterface/include $$PLATFORM_DIR/containers/include

LIBS += -L$$PLATFORM_DIR/bin -licobject -licsettings -livkplatform -lictmplugininterface -liccontainers

unix {
    target.path = /usr/lib
    INSTALLS += target
}
