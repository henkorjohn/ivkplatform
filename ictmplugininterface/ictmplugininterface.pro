include(../platform_common.pri)

TEMPLATE = lib
TARGET = ictmplugininterface
QT += core
QT -= gui
CONFIG += plugin

SOURCES += \
    src/iccommand.cpp \
    src/ictmplugininterface.cpp \

HEADERS +=\
    include/iccommand.h \
    include/ictmplugininterface.h \
    include/ictmplugininterface_global.h \

INCLUDEPATH += ./include

DEFINES += ICTMPLUGININTERFACE_LIBRARY

LIBS += -L$$DESTDIR -livkplatform -licobject -licsettings
