/**
 * @file ictmplugininterface.h
 * @brief Интерфейс плагинов с поддержкой передачи сообщений согласно типам обрабатываемых команд (без указания конкретного получателя).
 * @copyright ivk-center.ru
 */
#ifndef ICTMPLUGININTERFACE_H
#define ICTMPLUGININTERFACE_H

#include <icplugininterface.h>
#include <ictmplugininterface_global.h>
#include <iccommand.h>
#include <QTimer>
#include <QMap>
#include <QDebug>
#include <QSemaphore>
#include <functional>

/**
 * @brief Интерфейс для подключаемых модулей с поддержкой пересылки команды тому модулю, который поддерживает тип операнда и оператор.
 *
 * Модулям-наследникам нужно перегрузить init(), где воспользовавшись функцией registerCommandHandler зарегистрировать свои обработчики команд.
 * При этом надо не забыть вызвать функцию инициализации базового класса. Ну а работа должна начинаться с функции start
 * (в ней тоже надо не забыть вызвать start базового класса).
 * @ingroup core
 */
class ICTMPLUGININTERFACESHARED_EXPORT ICTMPluginInterface : public ICPluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "ivk-center.ictmplugininterface")
    Q_INTERFACES(ICPluginInterface)

public:
    typedef void (ICTMPluginInterface::*pluginFunc) (ICCommand&);
    /**
     * @brief Прототип функции для обработки ответа. Первый аргумент - адрес ответившего. Второй - ответ.
     */
    typedef std::function<void(quint64, ICCommand&)> answerFunc;

    Q_INVOKABLE ICTMPluginInterface(QObject* parent=nullptr);

    ICPluginInfo pluginInfo() {return {"ICTMPluginInterface",{1,0,0},{2,0,0}};}
    /**
     * @brief Создает новую команду.
     * @return Новая команда с уникальным идентификатором.
     */
    Q_INVOKABLE virtual ICCommand newCommand(OPERATION_TYPE operation=COMMAND_TYPE_ERROR,const QString& operandType=QString());
public slots:
    ICRETCODE init(const QString &pref) override;
    void uninit() override {}
    /**
     * @brief Посылает запрос плагину-обработчику.
     * @param cmd Команда
     * @param f Функция-обработчик ответа. Если nullptr, то ответа не ожидается.
     * @param mTimeout Максимальное время ожидания ответа. Если 0, то используется значение по-умолчанию.
     * @return Кол-во модулей, которым отправлена команда.
     */
    virtual int request(const ICCommand &cmd, answerFunc f=nullptr, uint mTimeout=0);

protected slots:
    /**
     * @brief Перегруженная функция. Обрабатывает исчезновение модулей, которые обрабатывают определенные команды.
     */
    void onPluginsChanged() override;

    /**
     * @brief Начало работы модуля.
     */
    virtual void startWork(){}

    /**
     * @brief Опрашивает все существующие модули о поддерживаемых командах.
     */
    void start() final;

    /**
     * @brief Посылает ответ на команду.
     * @param cmd Команда.
     */
    virtual void reply(const ICCommand &cmd);
    void processMessage(quint64 srcAddr, const QByteArray& data) final;
protected:
    /**
     * @brief Регистрирует функцию-обработчик команды заданного типа.
     * @param ct Тип обрабатываемой команды.
     * @param f Функция-обработчик.
     * @param priority приоритет обработчика. Если INVALID_PRIORITY, то читается из конфига.
     */
    void registerCommandHandler(CommandType ct, pluginFunc f, quint8 priority = INVALID_PRIORITY);
    /**
     * @brief Устанавливает идентификатор сессии пользователя для этого модуля.
     * @param sid Идентификатор сессии.
     */
    void setSessionId(quint64 sid) { m_sessionId = sid; }
private:
    quint64 m_sessionId = 0;
    QHash<CommandType, pluginFunc> m_pluginQueryHandlerByType; // Соответствие: тип обрабатываемой команды - плагинный обработчик запроса

    struct TimedFunc // структура для хранения времени, когда нужно сообщить о таймауте и функции-обработчика ответа
    {
        qint64 time;      // время, когда нужно сообщить о том, что ответ не пришел
        answerFunc f;     // функция-обработчик ответа
        quint64 answerer; // от кого ожидается ответ
        ICCommand cmd;    // команда (а вдруг кому-то приспичит получить назад именно оригинальную команду)
        bool operator==(const ICTMPluginInterface::TimedFunc& v) const
        {
            return (time==v.time && answerer==v.answerer);
        }
    };
    struct AddrPriority
    {
        quint64 addr;
        quint8 priority;
        bool operator==(const ICTMPluginInterface::AddrPriority& v) const
        {
            return (addr==v.addr && priority==v.priority);
        }
    };
    QMultiHash<quint64, TimedFunc> m_answerWaiters; // Соответствие: идентификатор команды - функция обработки ответа и время ожидания.
    uint m_defaultWaitTimeout; // время ожидания ответа по-умолчанию.
    QMultiHash<CommandType, AddrPriority> m_remoteHandlers; // Соответствие: тип обрабатываемой команды - адрес модуля
    QTimer m_answerCheckTimer;
    void commandsSelected(quint64 srcAddr, ICCommand& cmd); // обрабатывает селект поддерживаемых команд
    void onSelectCommandTypes(ICCommand& cmd); // обрабатывает запрос поддерживаемых команд
    void requestPluginsForSupportedCommands(QList<quint64> pls);
    quint8 readPriorityFromConfig(const CommandType& ct); // читает приоритеты обработчиков команд из конфига
    int m_answersCount=0; // переменная, показывающая, от скольки модулей нужно ждать ответы
    QMap<int, QString> m_operatorString; //соответсвие оператора соответствующей строке в настроечном файле
private slots:
    void onAnswerCheckTimer(); // обработка проверки ответов по таймеру
signals:
    void startPluginWork();
};

#define make_callback(x) std::bind(x, this, std::placeholders::_1, std::placeholders::_2)

//Q_DECLARE_INTERFACE(ICTMPluginInterface, "ivk-center.ICTMPluginInterface/1.0")

#endif // ICTMPLUGININTERFACE_H
